package com.gnilapon.revision.apres.examen.manque.deserialization;

import com.gnilapon.revision.apres.examen.manque.serialization.PersonSerialize;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationDemo {
    public static void main(String[] args) {
        // Deserialize the object from the file
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("person.ser"))) {
            PersonSerialize person = (PersonSerialize) in.readObject();
            System.out.println("Deserialized object: " + person);
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
