package com.gnilapon.revision.apres.examen.manque;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SortArrayCollections {

    public static void main(String[] args) {

        /**
         * méthode de Comparator permet de créer un Comparator qui compare les éléments en utilisant l'ordre naturel des clés extraites par une fonction de clé.
         */
        List<String> list = Arrays.asList("apple", "banana", "cherry");
        list.sort(Comparator.comparing(String::length));

        /**
         * méthode de Comparator permet de créer un Comparator qui compare les éléments en fonction d'une fonction de clé et en utilisant un Comparator spécifique pour les clés
         */
        List<String> list0 = Arrays.asList("apple", "banana", "cherry");
        list.sort(Comparator.comparing(String::length, Comparator.naturalOrder()));

        /**
         * trier une liste de chaînes de caractères par leur longueur et, en cas d'égalité, par ordre alphabétique inverse
         */
        List<String> list1 = Arrays.asList("apple", "banana", "kiwi");
        list.sort(Comparator.comparingInt(String::length).thenComparing(Comparator.reverseOrder()));

        /**
         * Comment spécifier un ordre de tri naturel inversé en utilisant Comparator
         */
        List<Integer> list2 = Arrays.asList(1, 2, 3);
        list.sort(Comparator.reverseOrder());

        /**
         * méthode de Comparator peut être utilisée pour gérer les valeurs nulls en dernier dans l'ordre de tri
         */
        List<String> list3 = Arrays.asList("apple", null, "banana");
        list.sort(Comparator.nullsLast(Comparator.naturalOrder()));

        /**
         * Comment trier une liste d'objets en utilisant Comparable sans appeler explicitement Collections.sort() ou Arrays.sort()
         */
        List<String> list4 = Arrays.asList("apple", "banana", "cherry");
        list.sort(null);
    }
}


