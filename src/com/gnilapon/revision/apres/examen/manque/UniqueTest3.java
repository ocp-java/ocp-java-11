package com.gnilapon.revision.apres.examen.manque;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.*;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class UniqueTest3 {
    public static void main(String[] args) {

    }
}

interface Instrument{
    default void reset(){
        System.out.println("In Instrument");
    }
}
class Debt implements Instrument{ }
class Bond extends Debt{
    public void reset(){
        super.reset();
        System.out.println("In Bond");
    }
}
class ZCBond extends Bond{
    ZCBond(){
        reset();
    }
    public void reset(){
        super.reset();
        System.out.println("In ZCBond");
    }
    public static void main(String args[]){
        Instrument i = new ZCBond();
        /*System.out.println(null + true); //1
        System.out.println(true + null); //2
        System.out.println(null + null); //3*/
    }
}

class TestStatic{
    int i1;
    static int i2;
    public void method1(){
        int i;
        i = this.i1;

        i = this.i1;
    }
}


/**
 * Q53
 * R : It will print: Exception - Code=404, Msg=NOT FOUND, OrigMsg=FILE UNREADABLE
 */
class MyExceptionEx extends Exception {
    private final int code;
    public MyExceptionEx(int code, Throwable actualEx){
        super(actualEx);     this.code = code;   }
    public MyExceptionEx(int code, String message, Throwable actualEx){
        super(message, actualEx);//1
        this.code = code;   }
    public String getMessage(){
        return String.format("Exception - Code=%d, Msg=%s, OrigMsg=%s", code,   super.getMessage(), this.getCause().getMessage()); //2
    }
}
class TestClassEX {
    public static void main(String[] args) {
        try{
            throw new MyExceptionEx(404, "NOT FOUND",  new IOException("FILE UNREADABLE")); //3
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}


/**
 * Q49
 * R : Compilation error
 *
 * Observez que la méthode printNames ne prend aucun argument.
 * Mais l'argument de la méthode forEach nécessite une méthode qui prend un argument.
 * La méthode forEach invoque la méthode passée et lui donne un élément de la liste comme argument.
 * Voici comment vous pouvez visualiser le fonctionnement du code donné :  Iterator it = list.iterator() ; while(it.hasNext()){ //ceci ne sera pas compilé car la méthode printNames ne prend pas d'argument.
 * Names.printNames(it.next()) ; }  Pour que cela fonctionne, la ligne n.getList().forEach(Names::printNames) ; doit être remplacée par : n.printNames() ;
 *
 */
class Names{
    private List<String> list;
    public List<String> getList() {
        return list;     }
    public void setList(List<String> list) {
        this.list = list;
    }
    public void printNames(){
        System.out.println(getList());
    }
    public static void main(String[] args) {
        List<String> list = Arrays.asList( "Bob Hope","Bob Dole","Bob Brown");
        Names n = new Names();
        n.setList(list.stream().collect(Collectors.toList()));
        //n.getList().forEach(Names::printNames);
    }
}

/**
 * Q42
 *
 * R : dV.add(new Dooby(){ });, bV = bL;, dV = bL;, bV = tL;
 */

class Booby0{ }
class Dooby0 extends Booby0{ }
class Tooby0 extends Dooby0{ }

class Main42{
    public static void main(String[] args) {
        List<? extends Booby0> bV = new ArrayList<Booby0>();//code for initialization
        List<? super Dooby0> dV = Collections.singletonList(new ArrayList<Tooby0>()); //code for initialization
        var bL = new ArrayList<Booby0>();
        var tL = new ArrayList<Tooby0>();

        dV.add(new Dooby0(){ });
        bV = bL;
        dV = Collections.singletonList(tL);


        /**
         * Q41
         * R : Map<String , List<? extends CharSequence>> stateCitiesMap1 = new HashMap<>();, var stateCitiesMap = new HashMap<String, List<? extends CharSequence>>();
         */
        Map<String , List<? extends CharSequence>> stateCitiesMap0 = new HashMap<String, List<? extends CharSequence>>();
        Map<String , List<? extends CharSequence>> stateCitiesMap1 = new HashMap<>();
        var stateCitiesMap = new HashMap<String, List<? extends CharSequence>>();


        /**
         * Q40
         * R : [A, null] [A, null] , It will throw a java.lang.UnsupportedOperationException at run time.
         */
        var strList1 = new ArrayList<String>();
        strList1.add("A");
        var strList2 = Collections.unmodifiableList(strList1);
        strList1.add(null);
        System.out.println(strList1+" "+strList2);
        strList2.add(null);
        System.out.println(strList1+" "+strList2);
    }
}

/**
 * Q39
 * R : In int In char
 * La question semble confuse, mais elle est simple.
 * Rappelez-vous que lorsque deux méthodes sont applicables pour un appel de méthode, c'est celle qui est la plus spécifique à l'argument qui est choisie.
 * Dans le cas de m(a), a est un int, qui ne peut pas être passé comme un char (parce qu'un int ne peut pas entrer dans un char).
 * Par conséquent, seul m(int) est applicable.
 * Dans le cas de m(c), c est un caractère, qui peut être transmis à la fois comme un int et comme un char.
 * Les deux méthodes sont donc applicables.
 * Cependant, m(char) est plus spécifique et est donc préférée à m(int).
 *
 */
class Noobs {
    public void m(int a){
        System.out.println("In int "+ a);
    }
    public void m(char c){
        System.out.println("In char "+ c);
    }
    public static void main(String[] args) throws SQLException {
        Noobs n = new Noobs();
        int a = 'a';
        char c = 6;
        n.m(a);
        n.m(c);


        /**
         * Q38
         * R :  Connection connection = DriverManager.getConnection("","","");
         *         try(Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery("select * from STUDENT"); ) {
         *             while(rs.next()){
         *                 //do something with the row
         *             }
         *         }
         * Les objets Statement et ResultSet sont créés dans la section try-with-resources et il n'est donc pas nécessaire de les fermer explicitement.
         * De plus, la connexion devant être réutilisée pour d'autres requêtes, il n'est pas nécessaire de la fermer non plus.
         * Rappelez-vous que les ressources sont fermées à la fin du bloc try (et avant tout catch ou finally) dans l'ordre inverse de leur création.
         * Ainsi, ici, rs.close() sera appelé en premier, suivi de stmt.close().
         *
         */
        Connection connection = DriverManager.getConnection("","","");
        try(Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery("select * from STUDENT"); ) {
            while(rs.next()){
                //do something with the row
            }
        }
    }
}


/**
 * Q37
 * R :  It will print E1 and Finally.
 * Puisque E2 est une sous-classe de E1, catch(E1 e) sera capable d'attraper les exceptions de la classe E2.
 * Par conséquent, E1 est imprimé.
 * Une fois l'exception capturée, les autres blocs catch au même niveau (c'est-à-dire ceux associés au même bloc try) sont ignorés.
 * E n'est donc pas imprimé.
 * finally est toujours exécuté (sauf lorsque la méthode System.exit est invoquée), donc Finally est également imprimé.
 */
class E1 extends Exception{ }
class E2 extends E1 { }
class TestEx{
    public static void main(String[] args){
        try{
            throw new E2();
        } catch(E1 e){
            System.out.println("E1");
        } catch(Exception e){
            System.out.println("E");
        } finally{
            System.out.println("Finally");
        }
    }
}


/**
 * Q36
 * R : 4 3 3
 */
class TestLength36{
    public static void main(String[] args){
        var i = 4;
        int[][][] ia = new int[i][i = 3][i];
        System.out.println( ia.length + ", " + ia[0].length+", "+ ia[0][0].length);
    }
}


/**
 * Q35
 * R : [100] []
 *
 */
class TestClassStack{

    public static void processStacks(Stack x1, Stack x2){

//assume that the push method adds the passed object to the stack.
        x1.push (new Integer ("100"));
        x2 = x1;
    }
    public static void main(String args[]){
        Stack s1 = new Stack ();
        Stack s2 = new Stack ();
        processStacks (s1, s2);
        System.out.println (s1 + "    "+ s2);




        /**
         * Q34
         * R : int[] array2D[] = new int [2] [2]; array2D[0] [0] = 1; array2D[0] [1] = 2; array2D[1] [0] = 3;
         *     int[] arr = {1, 2}; int[][] arr2 = {arr, {1, 2}, arr}; int[][][] arr3 = {arr2};
         */
        int[] array2D[] = new int [2] [2];
        array2D[0] [0] = 1;
        array2D[0] [1] = 2;
        array2D[1] [0] = 3;


        int[] arr = {1, 2};
        int[][] arr2 = {arr, {1, 2}, arr};
        int[][][] arr3 = {arr2};
    }

}


/**
 * Q31
 * R : It will fail to compile at line marked //3
 * Rappelez-vous que la partie "liste de paramètres" d'une expression lambda déclare de nouvelles variables qui sont utilisées dans la partie "corps" de cette expression lambda.
 * Cependant, une expression lambda ne crée pas de nouvelle portée pour les variables.
 * Par conséquent, vous ne pouvez pas réutiliser les noms de variables locales qui ont déjà été utilisés dans la méthode englobante pour déclarer les variables dans votre expression lambda.
 * Cela reviendrait à déclarer deux fois la même variable.
 * Ici, la méthode principale a déjà déclaré une variable nommée e.
 * Par conséquent, la partie liste de paramètres de l'expression lambda ne doit pas déclarer une autre variable portant le même nom.
 * Vous devez utiliser un autre nom.
 * Par exemple, si vous remplacez //3 par ce qui suit, cela fonctionnera.
 * System.out.println(validateEmployee(e, x->x.age<10000)) ; Cela imprimerait true.
 */

class Employee{
    int age;   //1
}
class TestClassEmployee{
    public static boolean validateEmployee(Employee e, Predicate<Employee> p){
        return p.test(e);
    }
    public static void main(String[] args) {
        Employee e = new Employee(); //2
        System.out.println(validateEmployee(e, k->k.age<10000)); //3
    }
}


/**
 * Q28
 * R : Usage of public arrays should be avoided.
 * Les tableaux Java sont toujours mutables.
 * Bien qu'il ne soit pas possible de modifier la longueur d'un tableau, il est toujours possible de modifier ses éléments.
 * Il est donc préférable d'exposer des collections non modifiables plutôt que des tableaux.
 * Si vous voulez exposer un tableau, exposez-le toujours à travers une méthode après l'avoir cloné.
 * Par exemple : int[] getKey(){ return key.clone() ; }  Ligne directrice 6-12 / MUTABLE-12 : Ne pas exposer de collections modifiables
 *
 */

class FastDataProcessor{
    public static int[] key = {1, 2, 3, 4, 5};
    public native byte[]  transform(byte[] input, int start, int end);


    /**
     * Q22
     * R : Line //1 will cause the compilee to generate a warning.
     *
     * @param la
     */
    static void printElements(List<String>... la) {  //1
       for(List<String> l : la){ //2
           System.out.println(l);
       }
    }
}


/**
 * Q19
 * R : The readObject method will be invoked before a Data object is deserialized.
 * Lorsque vous essayez de lire un objet à partir d'un ObjectInputStream à l'aide de la méthode readObject de l'ObjectInputStream, celui-ci vérifie si la classe qui est lue implémente la méthode readObject.
 * Si c'est le cas, la méthode readObject de cette classe sera invoquée.
 * La classe peut avoir une logique de désérialisation personnalisée dans cette méthode.
 * Dans le code donné, la méthode readObject invoque stream.defaultReadObject(), qui lit l'état présent dans le flux d'entrée dans cet objet Data.
 * Ensuite, elle définit l'objet ld à une date particulière.
 */
class DataReadObject implements Serializable {
    int id;
    transient LocalDate ld;
    private void readObject(ObjectInputStream stream)  throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        ld = LocalDate.of(2020,1,1);
    }
}


/**
 * Q17
 * R : -class created inside the main method at //1 is final.
 *     -Objects of class B cannot be created inside the main method just by doing "new B()"
 *     -new TestClass().new A().m(); can be inserted in main to invoke A's m().
 *     -tc.new A().m(); can be inserted in main to invoke A's m().
 */
class Ai { }
class TestClassNested {
    public class Ai    {
        public void m() { }
    }
    class B extends Ai    {
        B(){ m(); }
    }
    public static void main(String args[]){
        new TestClassNested().new Ai() {   public void m() { } }; //1
        var tc = new TestClassNested();
        //insert call to A's m() here
    }
}

/**
 * Q15
 * R :It will throw an exception at run time.
 *
 * Deux facteurs contradictoires entrent en jeu.
 * 1. Par défaut, la méthode Files.move tente de déplacer le fichier vers le fichier cible, échouant si le fichier cible existe, sauf si la source et la cible sont le même fichier, auquel cas cette méthode n'a aucun effet. Par conséquent, ce code devrait lever une exception parce que a.java existe dans le répertoire cible.
 * 2. Toutefois, lorsque l'argument CopyOption de la méthode move est StandardCopyOption.ATOMIC_MOVE, l'opération dépend de l'implémentation si le fichier cible existe. Le fichier existant peut être remplacé ou une exception IOException peut être levée. Si le fichier sortant à p2 est remplacé, Files.delete(p1) lèvera java.nio.file.NoSuchFileException.
 * Par conséquent, dans ce cas, le code donné dans son ensemble se soldera par une exception.
 * NOTE : Certains candidats ont déclaré avoir été testés avec StandardCopyOption.ATOMIC_MOVE. Malheureusement, il n'est pas clair si l'examen teste l'aspect dépendant de l'implémentation de cette option. Si vous recevez une question sur cette option et que les options ne contiennent aucune option faisant référence à sa nature dépendante de l'implémentation, nous vous suggérons ce qui suit : Supposez que l'opération de déplacement réussisse et que l'opération de suppression génère une exception java.nio.file.NoSuchFileException.
 */

class MoveTest {
    public static void main(String[] args) throws IOException {
        Path p1 = Paths.get("C:\\projects\\cerif-java\\OCP\\java-11\\TP\\ocp-java-11\\src\\com\\gnilapon\\pock\\examen\\UniqueTest3.java");
        Path p2 = Paths.get("C:\\projects\\cerif-java\\OCP\\java-11\\TP\\ocp-java-11\\src\\com\\gnilapon\\pock\\examen\\classes\\AAAAA.java");
        Files.move(p1, p2, StandardCopyOption.ATOMIC_MOVE);
        Files.delete(p1);
        System.out.println(p1.toFile().exists()+" "+p2.toFile().exists());
    }
}


/**
 * Q6
 * R : The approach used for executing code by checking user permissions using user's AccessControlContext is correct but it is implemented incorrectly.
 * Le code donné est correct à l'exception du fait qu'il ne vérifie pas si le contexte est nul. Un contexte nul est interprété comme n'ajoutant aucune restriction supplémentaire. Par conséquent, avant d'utiliser des contextes stockés, vous devez vous assurer qu'ils ne sont pas nuls.
 */
/*
class BackgroundExecutionService {
    public void submitTask(final List<Taskbar> tasks, final AccessControlContext acc){
        new Thread(){
            public void run(){ AccessController.doPrivileged(new PrivilegedAction<Void>() {

                public Void run() {
                    for(Taskbar t : tasks){
                        // process task t
                     }
                    return null;
                }
            }, acc);
            }.start();
           // ...other irrelevant code
        }

    }
}*/


//in file A.java
class ACheckedEx{
    protected void m() throws Exception{

    }
}
//in file B.java public
class BCheckedEx extends ACheckedEx{
    public void m(){ }
}
//in file TestClass.java public
class TestClassCheckedEx{
    public static void main(String[] args){
        //insert code here. //1
    }
}


class TestClassH{
    public static void main(String args[ ] ){
        Ah o1 = new Ch( );
        Bh o2 = (Bh) o1;
        System.out.println(o1.m1( ) );
        System.out.println(o2.i );
    }
}
class Ah {
    int i = 10;
    int m1( ) {
        return i;
    }
}
class Bh extends Ah {
    int i = 20;
    int m1() {
        return i;
    }
}
class Ch extends Bh {
    int i = 30;
    int m1() {
        return i;
    }
}


class Game{
    public void play() throws Exception{
        System.out.println("Playing...");
    }
}

class Soccer extends Game{
    public void play(){
        System.out.println("Playing Soccer...");
    }
    public static void main(String[] args){
        Game g = new Soccer();
        //g.play();
    }
}
