package com.gnilapon.revision.apres.examen.manque;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JavaIOStreams {
    public static void main(String[] args) throws IOException {
        /**
         * écrit correctement un objet sérialisé dans un fichier en Java
         */
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("file.dat"))) {
            oos.writeObject(new Object());
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

        /**
         *  lit correctement un objet sérialisé à partir d'un fichier en Java
         */
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("file.dat"))) {
            Object obj = ois.readObject();
        }
        catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        /**
         * lit correctement des données de la console en utilisant BufferedReader
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        /**
         * lit correctement un fichier texte ligne par ligne
         */
        try (BufferedReader reader1 = new BufferedReader(new FileReader("file.txt"))) {
            String line;
            while ((line = reader1.readLine()) != null) {
                System.out.println(line);
            }
        }

        /**
         * écrit correctement une chaîne de caractères dans un fichier texte
         */
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("file.txt"))) {
            writer.write("Hello, World!");
        }

        /**
         * lit correctement le contenu d'un fichier texte dans une chaîne de caractères
         */
        String content = new String(Files.readAllBytes(Paths.get("file.txt")));

        /**
         * écrit correctement des données binaires dans un fichier
         */
        try (FileOutputStream fos = new FileOutputStream("file.dat")) {
            fos.write(new byte[]{0x0A, 0x0B, 0x0C});
        }

        /**
         * lit correctement des données binaires à partir d'un fichier
         */
        try (FileInputStream fis = new FileInputStream("file.dat")) {
            byte[] data = new byte[3];
            fis.read(data);
        }

        /**
         * écrit correctement un objet sérialisé dans un fichier
         */
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("file.dat"))) {
            oos.writeObject(new Object());
        }

        /**
         * lit correctement un objet sérialisé à partir d'un fichier
         */
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("file.dat"))) {
            Object obj =  ois.readObject();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        /**
         *  lit correctement des primitives de types de données Java à partir d'un flux d'entrée
         */
        try (DataInputStream dis = new DataInputStream(new FileInputStream("file.dat"))) {
            int value = dis.readInt();
        }

        /**
         * écrit correctement des primitives de types de données Java dans un flux de sortie
         */
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream("file.dat"))) {
            dos.writeInt(123);
        }
    }
}
