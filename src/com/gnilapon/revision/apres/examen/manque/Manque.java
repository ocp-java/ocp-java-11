package com.gnilapon.revision.apres.examen.manque;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.logging.Logger;
import java.util.stream.Collectors;

class PrimitivesAndWrappers {
    private static final Logger logger = Logger.getLogger(PrimitivesAndWrappers.class.getName());
    public static void main(String[] args) {
        // Types primitifs
        int a = 5;
        double b = 10.5;
        char c = 'A';

        // Classes wrappers
        Integer aWrapper = Integer.valueOf(a);
        Double bWrapper = Double.valueOf(b);
        Character cWrapper = Character.valueOf(c);

        // Opérations arithmétiques
        int somme = a + 10;
        double produit = a * b;

        // Utilisation des parenthèses pour contrôler l'ordre des opérations
        double resultat = (a + b) * 2 / (a - 3);
        double resultat1 = a + b * 2 / a - 3;

        // Promotion de type
        double resultatPromu = a + b; // a (int) est promu en double

        // Casting
        int resultatCast = (int) b; // double vers int, la partie fractionnaire est perdue

        // Affichage des résultats
        logger.info("Somme (int + int) : " + somme);
        logger.info("Produit (int * double) : " + produit);
        logger.info("Résultat avec parenthèses : " + resultat);
        logger.info("Résultat 1 avec parenthèses : " + resultat1);
        logger.info("Résultat promu (int + double) : " + resultatPromu);
        logger.info("Résultat casté (double vers int) : " + resultatCast);

        // Opérations logiques avec primitifs et wrappers
        boolean estEgal = (aWrapper == a); // vrai, car aWrapper est auto-déréférencé en int
        boolean estEgalWrapper = aWrapper.equals(a); // vrai, comparaison correcte

        logger.info("aWrapper est égal à a : " + estEgal);
        logger.info("aWrapper est égal à a en utilisant equals : " + estEgalWrapper);

        // Opérateurs relationnels
        boolean estPlusGrand = b > a;
        logger.info("b (double) est-il plus grand que a (int) : " + estPlusGrand);

        // Opérateurs logiques
        boolean etLogique = (a > 0) && (b > 0);
        boolean ouLogique = (a < 0) || (b > 0);

        logger.info("ET logique (a > 0 && b > 0) : " + etLogique);
        logger.info("OU logique (a < 0 || b > 0) : " + ouLogique);

        // Promotion de type dans les expressions
        long x = 100;
        float y = 20.5f;
        double z = x + y; // x promu en float, puis le résultat en double

        logger.info("Résultat de long + float promu en double : " + z);

        // Casting dans les expressions
        int resultatInt = (int)(x / y); // le résultat de la division est casté en int
        logger.info("Résultat de long / float casté en int : " + resultatInt);





        // Primitives
        int intPrim = 5;
        double doublePrim = 10.5;
        float floatPrim = 15.5f;
        long longPrim = 15L;

        // Primitives
        Integer intObj = 5;
        Double doubleObj = new Double(10.5);
        Float floatObj = new Float(15.5f);
        Long longObj = new Long(15L);

        double dou = floatObj;
        double dou1 = intObj;
        double dou2 = intObj + floatObj + longObj + doubleObj;
        //Double dou3 = intObj+floatObj;

        float f0 = intObj*longObj+floatObj;

        long l0 = intObj+longObj;
        long l1 = intObj;
        long l2 = intPrim;

        int i0  = intObj;

        // Classes Enveloppantes
        Integer intWrap = Integer.valueOf(10);
        Double doubleWrap = Double.valueOf(20.5);
        Float floatWrap = Float.valueOf(25.5f);

        // Utilisation des opérateurs et des parenthèses
        double result1 = (doublePrim + intPrim) * floatPrim;
        System.out.println("Result1: " + result1);

        // Promotion de type (type promotion)
        double result2 = intPrim + doublePrim;
        System.out.println("Result2: " + result2);

        // Moulage (casting)
        int result3 = (int) (doubleWrap + floatWrap);
        System.out.println("Result3: " + result3);

        // Conversion de primitives à objets (boxing)
        Integer boxedInt = intPrim;
        Double boxedDouble = doublePrim;
        Float boxedFloat = floatPrim;

        // Conversion d'objets à primitives (unboxing)
        int unboxedInt = intWrap;
        double unboxedDouble = doubleWrap;
        float unboxedFloat = floatWrap;

        System.out.println("Boxed and unboxed values: " + boxedInt + ", " + boxedDouble + ", " + boxedFloat);
        System.out.println("Unboxed values: " + unboxedInt + ", " + unboxedDouble + ", " + unboxedFloat);

    }
}

class DateFormatExample {
    public static void main(String[] args) {
        // Création d'une date actuelle
        Date currentDate = new Date();

        // Création de formats de date avec différents styles
        DateFormat shortDateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);
        DateFormat mediumDateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE);
        DateFormat shortDateTimeFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.FRANCE);
        DateFormat mediumDateTimeFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.FRANCE);

        // Formatage de la date actuelle avec les différents styles
        String shortDate = shortDateFormat.format(currentDate);
        String mediumDate = mediumDateFormat.format(currentDate);
        String shortDateTime = shortDateTimeFormat.format(currentDate);
        String mediumDateTime = mediumDateTimeFormat.format(currentDate);

        // Affichage des résultats
        System.out.println("Date format SHORT : " + shortDate);
        System.out.println("Date format MEDIUM : " + mediumDate);
        System.out.println("DateTime format SHORT : " + shortDateTime);
        System.out.println("DateTime format MEDIUM : " + mediumDateTime);

        // Exemple d'analyse (parsing) de date
        try {
            Date parsedDate = shortDateFormat.parse(shortDate);
            System.out.println("Date analysée (SHORT) : " + parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}

class ExpressionExamples {
    public static void main(String[] args) {
        // Déclaration et initialisation des variables primitives
        int a = 5;
        int b = 10;
        double x = 2.5;
        double y = 3.5;
        float p = 1.5f;
        float q = 2.0f;

        // Exemple 1 : Opérations arithmétiques de base avec des parenthèses
        int result1 = (a + b) * (a - b);
        System.out.println("Result1: " + result1);

        // Exemple 2 : Mélange de types avec promotion de type et casting
        double result2 = a * x + b / y;
        System.out.println("Result2: " + result2);

        // Exemple 3 : Utilisation de float et double avec des opérateurs et des parenthèses
        double result3 = (p + q) * (x - y);
        System.out.println("Result3: " + result3);

        // Exemple 4 : Priorité des opérateurs et utilisation des parenthèses pour contrôler l'ordre
        double result4 = p + q * x - y / (a + b);
        double result40 = p + q * x - y % a + b;
        System.out.println("Result40: " + result40);
        System.out.println("Result4: " + result4);



        // Exemple 5 : Moulage (casting) explicite
        int result5 = (int) (x * y) + a;
        int ine = Integer.valueOf(result5);
        System.out.println("Result5: " + result5);

        // Exemple 6 : Opérateurs de module et combinaison avec d'autres opérations
        int result6 = a % b + (int) (x % y);
        System.out.println("Result6: " + result6);

        // Exemple 7 : Utilisation des opérateurs logiques avec des expressions arithmétiques
        boolean result7 = (a + b) > (x * y);
        boolean result70 = b % a > x * y;
        System.out.println("Result70: " + result70);
        System.out.println("Result7: " + result7);

        // Exemple 8 : Complexité avec des parenthèses pour un contrôle précis
        double result8 = ((a + b) * (x - y)) / ((p * q) + a);
        double result80 = a + b * x - y / p * q + a;
        System.out.println("Result80: " + result80);
        System.out.println("Result8: " + result8);
    }
}

class HandleTextEfficiently {
    public static void main(String[] args) {
        // Utilisation de String
        String salutation = "Bonjour, ";
        salutation += "monde!"; // Cela crée un nouvel objet String
        System.out.println(salutation); // Sortie : Bonjour, monde!

        // Utilisation de StringBuilder
        StringBuilder sb = new StringBuilder("Bonjour, ");
        sb.append("monde!"); // Cela modifie l'objet existant
        System.out.println(sb.toString()); // Sortie : Bonjour, monde!

        // Plus d'opérations avec StringBuilder
        sb.insert(9, "beau ");
        System.out.println(sb); // Sortie : Bonjour, beau monde!
        sb.insert(sb.toString().length(), " gnilapon".toCharArray(),0," gnilapon".length());
        sb.insert(sb.toString().length(), " Ebenezaire",0," Ebenezaire".length());
        sb.insert(sb.toString().length()-10, 'E');
        sb.append(2);
        sb.append(2.000000464565);
        sb.append(2.34f);
        sb.append('o');
        sb.append("oeyetytyrteu",5,9);
        System.out.println(sb.toString()); // Sortie : Bonjour, beau monde! gnilapon
        System.out.println(sb.toString().length()); // Sortie : 29

        System.out.println(sb.substring(21,sb.toString().length())); // Sortie : gnilapon
        System.out.println(sb.substring(21)); // Sortie : gnilapon



        sb.reverse();
        System.out.println(sb.toString()); // Sortie : !ednom uaeb ,ruojnoB

        sb.reverse();
        sb.setLength(70);
        System.out.println(sb.toString()); // Sortie : nopalin

        StringBuilder sb1 = new StringBuilder("Java");
        sb1.append(" Programming");
        sb1.delete(5, 16);
        System.out.println(sb1.toString());
    }
}

class Exercise{
    public static void main(String[] args) {

        /*String str = "HelloWorld";
        String result = str.substring(5).concat(str.substring(0, 5));
        System.out.println(result);

        StringBuilder sb = new StringBuilder("12345");
        sb.replace(1, 3, "ABCDE");
        System.out.println(sb.toString());

        String str0 = "abracadabra";
        str0 = str0.replace('a', 'A').replace('b', 'B');
        System.out.println(str0);

        StringBuilder sb0 = new StringBuilder("racecar");
        sb0.reverse().append("racecar");
        System.out.println(sb0.toString());

        StringBuilder sb1 = new StringBuilder("StringBuilder");
        sb1.setCharAt(6, 'b');
        System.out.println(sb1.toString());

        String str1 = "  Hello World  ";
        String trimmed = str1.trim();
        System.out.println(trimmed);*/

        /*String s1 = "abc";
        String s2 = "def";
        String s3 = s1.concat(s2);
        s3 = s3.substring(2, 5);
        System.out.println(s3);*/

        /*String s = "  Java  ";
        s = s.trim();
        s = s.replace(" ", "_");
        System.out.println(s);*/

        /*StringBuilder sb = new StringBuilder("0123456789");
        sb.delete(2, 5);
        sb.insert(2, "abc");
        System.out.println(sb.toString());

        String s = "Java";
        s = s + s.substring(1, 3) + s.charAt(3);
        System.out.println(s);*/

        /*String s = "OpenAI";
        StringBuilder sb = new StringBuilder(s);
        sb.append(" GPT");
        s = sb.toString();
        System.out.println(s);*/

        /*StringBuilder sb = new StringBuilder("Hello");
        sb.append(' ').append("World").insert(6, "Beautiful ");
        System.out.println(sb.toString());*/

        /*StringBuilder sb = new StringBuilder("abcdef");
        sb.setCharAt(2, 'X');
        sb.deleteCharAt(4);
        System.out.println(sb.toString());

        String str = "Java";
        str += " is fun";
        String result = str.substring(5, 7);
        System.out.println(result);*/

        List<Student> ls = Arrays.asList(new Student("S1", Student.Grade.A), new Student("S2", Student.Grade.A), new Student("S3", Student.Grade.C));
        //Map<Student.Grade, List<Student>> grouping = ls.stream().collect(Collectors.groupingBy(Student::getGrade), Collectors.groupingBy(Student::getName, Collectors.toList())));
        //Map<Student.Grade, List<String>> grouping = ls.stream().collect(Collectors.groupingBy(Student::getGrade,Collectors.groupingBy(Student::getName, Collectors.toList())));
        Map<Student.Grade, List<String>> grouping = ls.stream().collect(Collectors.groupingBy(Student::getGrade,Collectors.mapping(Student::getName, Collectors.toList())));
        System.out.println(grouping);
        System.out.println(Student.Grade.B.name());

    }
}

class Student {

    public static enum Grade{ A, B , C, D, F;
        public String retourneA(){
            System.out.println(Grade.values()[0].name());
            return A.name();
        }
    }

    private String name;
    private Grade grade;
    public Student(String name, Grade grade){
        this.name = name;
        this.grade = grade;
    }
    public String toString(){
        return name+":"+grade;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }
}



enum EnumA{ A, AA, AAA};  //1

 class TestClass //2
{
    public enum EnumB{ B, BB, BBB }; //3
    public static enum EnumC{ C, CC, CCC }; //4
    public TestClass()
    {
        //enum EnumD{ D, DD, DDD } //5
    }

    public void methodX()
    {
        //public enum EnumE{ E, EE, EEE } //6
    }

    public static void main(String[] args) //7
    {
        //enum EnumF{ F, FF, FFF}; //8
    }

}

class Calculator{
    public static void main(String[] args) {
        double principle = 100;
        int interestrate = 5;
        double amount = compute(principle, x->x*interestrate);
        System.out.println(amount);
    }

    //INSERT CODE HERE
    public static double compute(double base, Function<Integer, Integer > func){
        return func.apply((int)base);
    }
    /*public static double compute(double base, Function<Double, Double> func){
        return func.apply(base);
    }*/
}

class A{
    public A() { }
    public A(int i) {   System.out.println(i );    }
}
class B{
     B(){

     }
     B(int j){
         System.out.println(j);
     }
    static A s1 = new A(1);
    A d;
    static A c;
    A a = new A(2);
    public static void main(String[] args){
        var b = new B(9);
        var a = new A(3);
    }
    static A s2 = new A(4);

    static {
        c = new A(8);
    }

    {
        d = new A(5);
    }
}

class FileCopier {

    public static void copy1(Path p1, Path p2) throws Exception {
        Files.copy(p1, p2, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
    }


    public static void main(String[] args) throws Exception {
        var p1 = Paths.get("c:\\temp\\test1.txt");
        var p2  = Paths.get("c:\\temp\\test2.txt");
        copy1(p1, p2);
        if(Files.isSameFile(p1, p2)){
            System.out.println("file copied");
        }else{
            System.out.println("unable to copy file");
        }
    }
}

class TestIntUnaryOperator{

    public static int operate(IntUnaryOperator iuo,int i){
        return iuo.applyAsInt(i);
    }

    public static void main(String[] args) {

        IntFunction<IntUnaryOperator> fo = b->a->a-b;  //1

        var x = operate(fo.apply(20),5); //2
        System.out.println(x);
    }
}

class Outer {
     static class Inner { }
}

class TestGarbageCollection  {
    public static void main(String[] args) {
        TestClass obj = new TestClass();
        obj = null;
        System.gc();
        System.out.println("End of main method");
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize method called");
    }
}

class OuterClass {
     String globalVoariable = "Local variable";

    public static void myMethod() {
        final String localVariable = "Local  ";
        var d = new  OuterClass();
        //localVariable+="Gobal";
        // Local inner class
        final class LocalInnerClass {
            public void display() {
                System.out.println("Accessing from Local Inner Class: " + localVariable);
                System.out.println("Accessing from Gobal variable on Inner Class: " + d.globalVoariable);
            }
        }

        LocalInnerClass localInner = new LocalInnerClass();
        localInner.display();
    }

    public static void main(String[] args) {
        OuterClass outer = new OuterClass();
        outer.myMethod();
    }
}


class Main {
    public static void main(String[] args) {
        OuterClasse outer = new OuterClasse("Outer");
        outer.printOuterField();
        outer = null;
        System.gc();
        // Attendre un moment pour donner au garbage collector le temps de s'exécuter
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Forcer l'exécution des finalizers de tous les objets en attente de finalisation
        System.runFinalization();
    }
}

class OuterClasse {
    private String value;

    public OuterClasse(String value) {
        this.value = value;
    }

    public void printOuterField() {
        System.out.println(value);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("OuterClass object is being garbage collected: " +value );
    }
}
class ArrayListDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        list.remove(1);

        System.out.println(list.get(1));
    }
}
class TestString {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "World";
        String s3 = s1 + s2;
        String s4 = "HelloWorld";

        System.out.println(s3 == s4);
        System.out.println(s3.equals(s4));
    }
}

class TestStrings {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "Hello";
        String s3 = new String("Hello");

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));
        System.out.println(s1 == s3);
        System.out.println(s1.equals(s3));
    }
}

class TestInheritance {
    public static void main(String[] args) {
        Parent obj1 = new Child();
        Child obj2 = new Child();

        System.out.println(obj1 instanceof Parent);
        System.out.println(obj1 instanceof Child);
        System.out.println(obj2 instanceof Parent);
        System.out.println(obj2 instanceof Child);
    }
}

class Parent {}
class Child extends Parent {}


class TestLambda {
    interface MyInterface {
        int operate(int a, int b);
        default int surface(String a){
            return toString().length();
        }
    }

    public static void main(String[] args) {
        MyInterface addition = (final int a, int b) -> a + b;

        MyInterface subtraction = ( int a, final int b) -> a - b;

        System.out.println(addition.operate(5, 3));
        System.out.println(subtraction.operate(5, 3));
    }
}

class TestGenerics<T> {
    private T value;

    public TestGenerics(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public static void main(String[] args) {
        TestGenerics<String> strObj = new TestGenerics<>("Hello");
        TestGenerics<Integer> intObj = new TestGenerics<>(100);

        System.out.println(strObj.getValue().toUpperCase());
        System.out.println(intObj.getValue() + 50);
    }
}