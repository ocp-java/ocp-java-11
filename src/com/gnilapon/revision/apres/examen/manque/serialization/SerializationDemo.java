package com.gnilapon.revision.apres.examen.manque.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationDemo {
    public static void main(String[] args) {
        // Create a Person object
        PersonSerialize person = new PersonSerialize("Alice", 30);

        // Serialize the object to a file
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("person.ser"))) {
            out.writeObject(person);
            System.out.println("Serialization completed. Check 'person.ser' file.");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}