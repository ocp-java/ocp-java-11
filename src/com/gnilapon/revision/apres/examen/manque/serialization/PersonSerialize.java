package com.gnilapon.revision.apres.examen.manque.serialization;

import java.io.Serializable;

public class PersonSerialize implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private int age;

    public PersonSerialize(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String toString() {
        return "Person{name='" + name + "', age=" + age + "}";
    }
}