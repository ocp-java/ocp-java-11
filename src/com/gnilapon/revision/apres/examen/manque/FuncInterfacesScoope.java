package com.gnilapon.revision.apres.examen.manque;

import java.util.function.*;

public class FuncInterfacesScoope {
}

interface MyInterface {
    void method();
    static void myStaticMethod() {
        System.out.println("This is a static method.");
    }
    default void myDefaultMethod() {
        System.out.println("This is a default method.");
    }
}

@FunctionalInterface
interface MyInterface1 {
    void method(String s);
}

class Test {
    public static void main(String[] args) {
        MyInterface.myStaticMethod();
    }
}

class MyClass implements MyInterface {
    void callDefaultMethod() {
        MyInterface.super.myDefaultMethod();
    }
    public void method() {
        System.out.println("This is a override method in " + MyClass.class.getName());
    }

    public static void myStaticMethod() {
        System.out.println("This is a static method in " + MyClass.class.getName());
    }
    public static void main(String[] args) {
        var b = new MyClass();
        b.callDefaultMethod();
        MyClass.myStaticMethod();
        //b.method();
        MyInterface my = ()->System.out.println("This is a override method in " + MyClass.class.getName());
        my.method();

        MyInterface1 my1 = s->System.out.println(s+ " This is a override method in " + MyClass.class.getName());
        MyInterface1 my2 = (final var s)->System.out.println(s+ " This is a override method in " + MyClass.class.getName());
        MyInterface1 my3 = (s)->System.out.println(s+ " This is a override method in " + MyClass.class.getName());


        my1.method("String 1");
        my2.method("String 2");
        my3.method("String 3");

        BiFunction<String, String, Integer> compare = (var s1, var s2) -> s1.compareTo(s2);

        System.out.println(compare.apply("String 1","String 3")+ " This is a result of BiFunction in " + MyClass.class.getName());
        System.out.println(compare.apply("String 1","String 1")+ " This is a result of BiFunction in " + MyClass.class.getName());
        System.out.println(compare.apply("String 1","String3 2")+ " This is a result of BiFunction in " + MyClass.class.getName());


        Predicate<String> isEmpty = s -> s.isEmpty();
        System.out.println(isEmpty.test("String 1") + " This is a result of Predicate in " + MyClass.class.getName());
        Consumer<String> print = s -> System.out.println(s);
        print.accept("String 1");

        Supplier<String> supplier = () -> "Hello, World!";

        System.out.println(supplier.get() + " This is a result of Supplier in " + MyClass.class.getName());

        Function<String, Integer> length = s -> s.length();
        System.out.println(length.apply("String 1") + " This is a result of Supplier in " + MyClass.class.getName());
    }
}
