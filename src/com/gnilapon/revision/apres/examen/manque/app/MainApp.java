package com.gnilapon.revision.apres.examen.manque.app;

import com.gnilapon.revision.apres.examen.manque.studentmanagement.StudentManagement;

public class MainApp {
    public static void main(String[] args) {
        StudentManagement management = new StudentManagement();

        // Different ways to create instances of Grade

        // Method 1: Direct instantiation using the enclosing class
        StudentManagement.Grade grade1 = new StudentManagement.Grade(1, "A", 90.0);
        StudentManagement.Grade grade2 = new StudentManagement.Grade(2, "B", 85.0);

        // Method 2: Using a static import (if you add `import static studentmanagement.StudentManagement.Grade;` at the top)
        // Grade grade3 = new Grade(3, "A-", 88.0);

        // Create Students and add Grades
        StudentManagement.Student student1 = management.new Student(1, "John Doe", "john.doe@example.com");
        student1.addGrade(grade1);
        student1.addGrade(grade2);

        StudentManagement.Student student2 = management.new Student(2, "Jane Smith", "jane.smith@example.com");
        student2.addGrade(new StudentManagement.Grade(3, "A-", 88.0));

        // Add Students to Management
        management.addStudent(student1);
        management.addStudent(student2);

        // Display Students and their Grades
        for (StudentManagement.Student student : management.getStudents()) {
            System.out.println(student);
        }
    }
}