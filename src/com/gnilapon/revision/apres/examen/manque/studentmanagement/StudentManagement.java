package com.gnilapon.revision.apres.examen.manque.studentmanagement;

import java.util.ArrayList;
import java.util.List;

public class StudentManagement {
    // Static Nested Class for Grade
    public static class Grade {
        private int gradeId;
        private String grade;
        private double average;

        public Grade(int gradeId, String grade, double average) {
            this.gradeId = gradeId;
            this.grade = grade;
            this.average = average;
        }

        public int getGradeId() {
            return gradeId;
        }

        public String getGrade() {
            return grade;
        }

        public double getAverage() {
            return average;
        }

        @Override
        public String toString() {
            return "{Grade ID: " + gradeId + ", Grade: " + grade + ", Average: " + average+"}";
        }
    }

    // Regular Inner Class for Student
    public class Student {
        private int studentId;
        private String fullName;
        private String email;
        private List<Grade> grades;

        public Student(int studentId, String fullName, String email) {
            this.studentId = studentId;
            this.fullName = fullName;
            this.email = email;
            this.grades = new ArrayList<>();
        }

        public int getStudentId() {
            return studentId;
        }

        public String getFullName() {
            return fullName;
        }

        public String getEmail() {
            return email;
        }

        public void addGrade(Grade grade) {
            grades.add(grade);
        }

        public List<Grade> getGrades() {
            return grades;
        }

        @Override
        public String toString() {
            return "{Student ID: " + studentId + ", Name: " + fullName + ", Email: " + email + ", Grades: " + grades+"}";
        }
    }

    private List<Student> students;

    public StudentManagement() {
        students = new ArrayList<>();
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public List<Student> getStudents() {
        return students;
    }
}
