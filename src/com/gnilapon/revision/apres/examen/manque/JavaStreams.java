package com.gnilapon.revision.apres.examen.manque;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class JavaStreams {
    public static void main0(String[] args) {
        List<String> words = List.of("Java", "Streams", "Filter", "Collect");
        String result = words.stream()
                .filter(s -> s.length() > 5)
                .sorted()
                .collect(Collectors.joining(", "));
        System.out.println(result);
    }

    public static void main1(String[] args) {
        Stream<String> stream = Stream.of("one", "two", "three", "four");
        stream.peek(System.out::println)
                .filter(s -> s.length() > 3)
                .map(String::toUpperCase)
                .forEach(System.out::println);
    }

    public static void main2(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5);
        List<Integer> result = numbers.stream()
                .filter(n -> n % 2 == 0)
                .map(n -> n * n)
                .collect(Collectors.toList());
        System.out.println(result);
    }

    public static void main3(String[] args) {
        int result = IntStream.range(1, 6)
                .peek(System.out::println)
                .reduce(0, (a, b) -> a + b);
        System.out.println(result);
    }

    public static void main4(String[] args) {
        List<String> words = List.of("apple", "banana", "cherry");
        words.stream()
                .map(s -> s.substring(0, 1).toUpperCase() + s.substring(1))
                .forEach(System.out::println);
    }

    public static void main5(String[] args) {
        List<Integer> numbers = List.of(3, 4, 5, 6, 7);
        String result = numbers.stream()
                .filter(n -> n % 2 != 0)
                .map(String::valueOf)
                .collect(Collectors.joining(", "));
        System.out.println(result);
    }

    public static void main6(String[] args) {
        List<Integer> numbers = List.of(10, 20, 30);
        Optional<Integer> result = numbers.stream()
                .filter(n -> n > 15)
                .findFirst();
        result.ifPresent(System.out::println);
    }

    public static void main7(String[] args) {
        Stream<Integer> stream = Stream.of(1, 2, 3, 4);
        long count = stream.peek(System.out::println)
                .count();
        System.out.println("Count: " + count);
    }

    public static void main8(String[] args) {
        Stream<Integer> stream = Stream.of(1, 2, 3, 4);
        stream.peek(System.out::println)
                .forEach(e -> {}); // Utilisation de forEach pour consommer les éléments du flux
        long count = stream.count(); // Cela va provoquer une exception car le flux est déjà consommé
        System.out.println("Count: " + count);
    }

    public static void main9(String[] args) {
        List<String> words = List.of("apple", "banana", "cherry");
        List<String> result = words.stream()
                .map(String::toUpperCase)
                .filter(s -> s.startsWith("A"))
                .collect(Collectors.toList());
        System.out.println(result);
    }

    public static void main10(String[] args) {
        List<String> words = List.of("apple", "banana", "cherry", "date");
        Map<Integer, List<String>> result = words.stream()
                .collect(Collectors.groupingBy(String::length));
        System.out.println(result);
    }

    public static void main11(String[] args) {
        /*List<Integer> numbers = List.of(3, 4, 5, 6, 7);
        boolean result = numbers.stream()
                .allMatch(n -> n % 2 == 0);
        System.out.println(result);*/

        /*List<Integer> result = IntStream.range(1, 11)
                .map(n -> n * n)
                .boxed()
                .collect(Collectors.toList());*/


        List<Integer> result = Stream.iterate(1, n -> n + 1)
                .limit(10)
                .map(n -> n * n)
                .collect(Collectors.toList());
        System.out.println(result);
    }

    public static void main(String[] args) {
        List<String> words = List.of("hello", "world");
        List<String> result = Stream.concat(words.stream(), Stream.of("java"))
                .collect(Collectors.toList());
        System.out.println(result);
    }


}
