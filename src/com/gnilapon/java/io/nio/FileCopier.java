package com.gnilapon.java.io.nio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


/** Question
 *
 * Given that test1.txt exists but test2.txt doesn't exist, consider the following code?
 */

/** Response
 *
 * Copying of the attributes is platform and system dependent.
 *
 * Extrait de la documentation JavaDoc API pour Files.copy : Tente de copier les attributs de fichier associés au fichier source dans le fichier cible.
 * Les attributs de fichier exacts qui sont copiés dépendent de la plate-forme et du système de fichiers et ne sont donc pas spécifiés.
 * Au minimum, la date de dernière modification est copiée dans le fichier cible si elle est prise en charge à la fois par le magasin de fichiers source et le magasin de fichiers cible.
 * La copie des horodatages des fichiers peut entraîner une perte de précision.
 */

/** Explication
 *
 *  La méthode Files.copy copiera le fichier test1.txt dans test2.txt.
 *  Si test2.txt n'existe pas, il sera créé.
 *  Voici une brève description JavaDoc de cette méthode : public static Path copy(Path source, Path target, CopyOption... options) throws IOException Copier un fichier dans un fichier cible.
 *  Cette méthode permet de copier un fichier vers le fichier cible, le paramètre options spécifiant la manière dont la copie est effectuée.
 *  Par défaut, la copie échoue si le fichier cible existe déjà ou s'il s'agit d'un lien symbolique, sauf si la source et la cible sont le même fichier, auquel cas la méthode s'achève sans copier le fichier.
 *  Les attributs du fichier ne doivent pas être copiés dans le fichier cible.
 *  Si les liens symboliques sont pris en charge et que le fichier est un lien symbolique, la cible finale du lien est copiée.
 *  Si le fichier est un répertoire, un répertoire vide est créé à l'emplacement cible (les entrées du répertoire ne sont pas copiées).
 *  Le paramètre options peut inclure l'un des éléments suivants :  REPLACE_EXISTING Si le fichier cible existe, il est remplacé s'il ne s'agit pas d'un répertoire non vide.
 *  Si le fichier cible existe et qu'il s'agit d'un lien symbolique, c'est le lien symbolique lui-même, et non la cible du lien, qui est remplacé.
 *  COPY_ATTRIBUTES Tente de copier les attributs associés à ce fichier dans le fichier cible.
 *  Les attributs de fichier exacts qui sont copiés dépendent de la plate-forme et du système de fichiers et ne sont donc pas spécifiés.
 *  Au minimum, l'heure de la dernière modification est copiée dans le fichier cible si elle est prise en charge à la fois par le magasin de fichiers source et le magasin de fichiers cible.
 *  La copie des horodatages des fichiers peut entraîner une perte de précision.
 *  NOFOLLOW_LINKS Les liens symboliques ne sont pas suivis.
 *  Si le fichier est un lien symbolique, c'est le lien symbolique lui-même, et non la cible du lien, qui est copié.
 *  La possibilité de copier les attributs du fichier sur le nouveau lien dépend de l'implémentation.
 *  En d'autres termes, l'option COPY_ATTRIBUTES peut être ignorée lors de la copie d'un lien symbolique.
 *  Une implémentation de cette interface peut prendre en charge des options supplémentaires spécifiques à l'implémentation.
 *  La copie d'un fichier n'est pas une opération atomique.
 *  Si une exception IOException est levée, il est possible que le fichier cible soit incomplet ou que certains de ses attributs n'aient pas été copiés à partir du fichier source.
 *  Lorsque l'option REPLACE_EXISTING est spécifiée et que le fichier cible existe, ce dernier est remplacé.
 *  La vérification de l'existence du fichier et la création du nouveau fichier peuvent ne pas être atomiques par rapport à d'autres activités du système de fichiers.
 *  Pour information : bien que cela ne soit pas pertinent pour cette question, n'oubliez pas que la méthode Files.isSameFile ne vérifie pas le contenu du fichier.
 *  Elle a pour but de vérifier si les deux objets chemin résolvent le même fichier ou non.
 *  Par conséquent, dans ce cas, si vous essayez de faire Files.isSameFile(p1, p2), le résultat sera faux.
 */
public class FileCopier {
    public static void copy1(Path p1, Path p2) throws Exception {
        Files.copy(p1, p2, StandardCopyOption.COPY_ATTRIBUTES);
    }

    public static void main(String[] args) throws Exception {
        Path p1 = Paths.get("c:\\temp\\test1.txt");
        Path p2 = Paths.get("c:\\temp\\test2.txt");
        copy1(p1, p2);
    }
}
