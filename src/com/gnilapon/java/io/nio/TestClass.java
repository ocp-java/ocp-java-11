package com.gnilapon.java.io.nio;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestClass {

    public static boolean isValid(Path p){
        return p.startsWith("temp") && p.endsWith("clients.dat");
    }
    public static void writeData() {
        var p1 = Paths.get("\\temp\\records");
        var p2 = p1.resolve("clients.dat");
        System.out.println(p2+" "+isValid(p2));
    }

    /** Question
     *
     * What will be printed when the method writeData() is executed?
     */

    /** Reponse
     *
     * \temp\records\clients.dat false
     */

    /** Explication
     *
     * p2 sera défini comme \temp\records\clients.dat. Comme il commence par \temp et non par temp, la méthode isValid renverra un faux.
     */
    public static void main0(String[] args) {
        writeData();
    }

    /** Question
     *
     * Which of the following will be a part of the output of the second piece of code?
     */

    /** Reponse
     * In Boo
     * In BooBoo
     * 20
     */

    /** Explication
     *
     * Lors de la désérialisation, le constructeur de la classe (ou tout bloc statique ou d'instance) n'est pas exécuté.
     * Cependant, si la super classe n'implémente pas Serializable, son constructeur est appelé.
     * Ici, BooBoo et Boo ne sont pas sérialisables.
     * Leur constructeur est donc invoqué.
     * @param args
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void main1(String[] args) throws IOException, ClassNotFoundException {
        createFile();
        FileInputStream fis = new FileInputStream("c:\\temp\\moo1.ser");
        ObjectInputStream is = new ObjectInputStream(fis);
        Moo moo = (Moo) is.readObject();
        is.close();
        System.out.println(moo.moo);
    }

    public static void createFile() throws IOException {
        Moo moo = new Moo();
        moo.moo = 20;
        FileOutputStream fos = new FileOutputStream("c:\\temp\\moo1.ser");
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(moo);
        os.close();
    }

    /** Question
     *
     * Consider the directory structure and its contents shown in the figure. (c:\temp is a directory that contains two text files - test1.txt and text2.txt)  What should be inserted at //Line 10 in the following code so that it will write "hello" to text2.txt?
     * @param args
     */
    /** Reponse
     *
     * p1.resolveSibling("text2.txt");
     * @param args
     */

    /** Explication
     *
     * Vous avez déjà le chemin absolu vers test1.txt dans p1.
     * De plus, le fichier test1.txt se trouve dans le même répertoire que le fichier text2.txt, c'est-à-dire que les deux fichiers sont frères.
     * Pour ouvrir test2.txt, vous devez déterminer le chemin absolu de text2.txt à l'aide du chemin absolu de test1.txt.
     * En d'autres termes, vous essayez d'obtenir le chemin absolu d'un fichier qui existe dans le même répertoire que le fichier original.
     * La méthode resolveSibling est exactement conçue à cette fin.
     * Elle attribue à p2 la valeur c:\temp\text2.txt, qui peut ensuite être utilisée pour créer l'objet File.
     * Nous vous conseillons de consulter la description de l'API JavaDoc suivante pour la méthode resolveSibling.
     * public Path resolveSibling(String other) or public Path resolveSibling(Path other) :- Résout le chemin d'accès donné par rapport au chemin d'accès parent de ce chemin d'accès.
     * Cette méthode est utile lorsqu'un nom de fichier doit être remplacé par un autre nom de fichier. Par exemple, supposons que le séparateur de nom soit "/" et qu'un chemin représente "dir1/dir2/foo", l'invocation de cette méthode avec le chemin "bar" donnera le chemin "dir1/dir2/bar".
     * Si ce chemin n'a pas de chemin parent, ou si other est absolu, cette méthode renvoie other.
     * Si other est un chemin vide, cette méthode renvoie le parent de ce chemin ou, si ce chemin n'a pas de parent, le chemin vide.
     * @param args
     */
    public static void main2(String[] args){
        try {
            /**
             * Notez que . est toujours redondant et est supprimé de lui-même, tandis que .. et le répertoire précédent s'annulent l'un l'autre parce que .. signifie le répertoire parent. Par exemple, a/b/... est identique à a.
             */
            Path p1 = Paths.get("c:.red\\..\\personal\\.\\photos\\..\\readme.txt");
            Path p2 = p1.normalize();
            System.out.println(p2);
            writeDataText();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    public static void writeDataText() throws Exception{
        var p1 = Paths.get("c:\\temp\\test1.txt");
        var p2 = p1.resolveSibling("text2.txt"); //LINE 10 - INSERT CODE HERE
        var p3 = p1.resolve("text2.txt"); //LINE 10 - INSERT CODE HERE
        var p4 = Paths.get("c:", p1.subpath(0, 2).toString(), "text2.txt"); //LINE 10 - INSERT CODE HERE
        var p5 = Paths.get("c:", p1.subpath(1, 2).toString(), "text2.txt"); //LINE 10 - INSERT CODE HERE
        var bw = new BufferedWriter(new FileWriter(p2.toFile()));
        bw.write("hello");

        System.out.println("resolve sibling path "+p1);
        System.out.println("resolve path "+p3);
        System.out.println("subpath 0 2 path "+p4);
        System.out.println("subpath 1 2 path "+p5);
        bw.close();
    }

    /** Question
     *
     * What will the following code fragment print?
     * @param args
     */
    /** Reponse
     * 1. p1 a 6 composants et donc p1.getNameCount() renverra 6.
     * 2. normalize applique tous les .. et . contenus dans le chemin au chemin. Par conséquent, p2 contient beaches\calangute\a.txt, soit 3 composants.
     * 3. p3 contient un chemin vide car les deux chemins sont égaux après la normalisation.
     * 4. p4 contient un chemin vide car les deux chemins sont égaux après normalisation.
     * NOTE : L'implémentation de la méthode relativize a changé dans Java 11. Elle normalise les chemins avant de relativiser.
     * C'est pourquoi Java 11 affiche 6 3 1 1, alors que Java 8 affiche 6 3 9 9.
     * 6 3 1 1
     * @param args
     */
    /** Explication
     *
     * Vous devez comprendre comment fonctionne la relativisation pour les besoins de l'examen.
     * L'idée de base de relativiser est de déterminer un chemin qui, lorsqu'il est appliqué au chemin d'origine, vous donne le chemin qui a été passé.
     * Par exemple, "a/b" relativise "c/d" et donne "../../c/d" parce que si vous êtes dans le répertoire b, vous devez reculer de deux pas, puis avancer d'un pas jusqu'à c et d'un autre pas jusqu'à d pour être en d.
     * Cependant, "a/c" relativise "a/b" et donne ".... /b" parce qu'il suffit de reculer d'un pas jusqu'à a et d'avancer d'un pas jusqu'à b.
     * Veuillez lire la description suivante de la méthode relativize(), qui explique son fonctionnement de manière plus détaillée.
     * public Path relativize(Path other) Construit un chemin relatif entre ce chemin et un chemin donné.
     * La relativisation est l'inverse de la résolution.
     * Cette méthode tente de construire un chemin relatif qui, lorsqu'il est résolu par rapport à ce chemin, produit un chemin qui localise le même fichier que le chemin donné. Par exemple, sous UNIX, si ce chemin est "/a/b" et que le chemin donné est "/a/b/c/d", le chemin relatif résultant sera "c/d".
     * Lorsque ce chemin et le chemin donné n'ont pas de racine, un chemin relatif peut être construit.
     *  Un chemin relatif ne peut pas être construit si un seul des chemins a un composant racine.
     *  Si les deux chemins ont un composant racine, la construction d'un chemin relatif dépend de l'implémentation.
     *  Si ce chemin et le chemin donné sont égaux, un chemin vide est renvoyé.
     *  Pour deux chemins normalisés p et q, où q n'a pas de composante racine, p.relativize(p .resolve(q)).equals(q) Lorsque les liens symboliques sont pris en charge, la question de savoir si le chemin résultant, lorsqu'il est résolu par rapport à ce chemin, produit un chemin qui peut être utilisé pour localiser le même fichier que d'autres dépend de l'implémentation.
     *  Par exemple, si ce chemin est "/a/b" et que le chemin donné est "/a/x", le chemin relatif résultant peut être "../x".
     *  Si "b" est un lien symbolique, il dépend de l'implémentation que "a/b/../x" localise le même fichier que "/a/x".
     *  Paramètres : other - le chemin à relativiser par rapport à ce chemin Returns : le chemin relatif résultant, ou un chemin vide si les deux chemins sont égaux
     * @param args
     */
    public static void main(String[] args) {
        Path p1 = Paths.get("photos\\..\\beaches\\.\\calangute\\a.txt");

        Path p2 = p1.normalize();
        Path p3 = p1.relativize(p2);
        Path p4 = p2.relativize(p1);

        Path p10 = Paths.get("b/c/d/e");
        Path p12 = Paths.get("b/c");
        Path p5 = p10.relativize(p12);

        System.out.println("normalize p1 ==> p2 "+p2);
        System.out.println("relativize where p1 and given p2 ==> p3 "+p3);
        System.out.println("relativize where p2 and given p1 ==> p4 "+p4);
        System.out.println("relativize where p10 and given p12 ==> p5 "+p5);
        System.out.println(     p1.getNameCount()+" "+p2.getNameCount()+" "+     p3.getNameCount()+" "+p4.getNameCount());
    }

}

class Boo {
    public Boo(){
        System.out.println("In Boo");
    }
}
class BooBoo extends Boo {
    public BooBoo(){
        System.out.println("In BooBoo");
    }
}
class Moo extends BooBoo implements Serializable {
    int moo = 10;
    {
        System.out.println("moo set to 10");
    }
    public Moo(){
        System.out.println("In Moo");
    }

}

class TrimTest{
    public static void main(String args[]){
        String blank  = " ";  // one space
        String line = blank + "hello" + blank + blank;
        line.concat("world");
        String newLine  =  line.trim();
        System.out.println((int)(line.length() + newLine.length()));
    }
}

