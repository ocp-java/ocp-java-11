package com.gnilapon.java.io.nio;

import java.nio.file.Path;
import java.nio.file.Paths;

/** Question
 *
 * What will the following code print when run?
 */

/** Reponse
 *
 * main
 */

/** Explication
 *
 * Rappelez-vous les 4 points suivants concernant la méthode Path.getName() :
 * 1. les indices des noms de chemin commencent à 0.
 * 2. la racine (c'est-à-dire c:\) n'est pas incluse dans les noms de chemin.
 * 3. \ n'est PAS une partie d'un nom de chemin.
 * 4. Si vous passez un index négatif ou une valeur supérieure ou égale au nombre d'éléments, ou si ce chemin a zéro nom d'élément, java.lang.IllegalArgumentException est levée. Il ne renvoie PAS null.
 * Ainsi, par exemple, si votre chemin est "c:\code\java\PathTest.java",
 * p1.getRoot() est c:\ ((Pour les environnements Unix, la racine est généralement / ).
 * p1.getName(0) est code
 * p1.getName(1) est java
 * p1.getName(2) est PathTest.java
 * p1.getName(3) provoquera la levée d'une exception IllegalArgumentException.
 */
public class PathTest {
    static Path p1 = Paths.get("c:\\main\\project\\Starter.java");

    public static String getData() {
        return p1.getName(0).toString();
    }

    public static void main(String[] args) {
        System.out.println(getData());
    }
}
