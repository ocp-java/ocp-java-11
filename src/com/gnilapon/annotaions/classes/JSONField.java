package com.gnilapon.annotaions.classes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * L'absence de @Target implique que l'annotation peut être utilisée sur toutes les positions applicables (classe, champ, méthode, constructeur, paramètre, variable locale).
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface JSONField {
    public String name() default "";
}
