package com.gnilapon.annotaions.classes.repetable;

/**
 * La valeur de la méta-annotation @Repeatable, entre parenthèses, est le type d'annotation conteneur que le compilateur Java génère pour stocker les annotations répétitives.
 * Le type d'annotation conteneur doit avoir un élément de valeur de type tableau.
 * Le type de composant du type de tableau doit être le type d'annotation répétable.
 * Il est possible d'utiliser d'autres éléments dans l'annotation conteneur, mais ils doivent avoir des valeurs par défaut.
 */
public @interface Meals{
    Meal[] value();
    String course() default "maincourse";
}
