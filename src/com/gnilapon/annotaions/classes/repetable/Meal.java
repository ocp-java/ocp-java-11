package com.gnilapon.annotaions.classes.repetable;

import java.lang.annotation.Repeatable;


/**
 * Pour faciliter la répétition des annotations, Java n'exige pas l'utilisation de l'annotation « conteneur ». Vous pouvez simplement écrire @Meal(name=« sandwich ») mais, en interne, Java le convertit en @Meals(@Meal(name=« sandwich »)).
 * Si vous appliquez deux annotations de ce type, par exemple : @Meal(name=« sandwich ») @Meal(name=« fries »), le compilateur les convertira en : @Meals({@Meal(name=« sandwich »), @Meal(name=« fries ») }) Une annotation de conteneur est également une annotation et, comme toute autre annotation, elle peut être utilisée indépendamment.
 * Elle peut également comporter d'autres éléments.
 * Par exemple, vous pouvez utiliser l'annotation @Meals comme suit :  @Meals(value={@Meal(name=« sandwich »), @Meal(name=« fries ») }, course=« starter ») Rappelez-vous que les valeurs d'une annotation répétée ne sont pas additives.
 * Ainsi, par exemple, vous ne pouvez pas vous attendre à ce que @Meal(id=1) et @Meal(name=« fries ») se combinent automatiquement en @Meal(id=1, name=« fries »). Étant donné que id est défini à l'aide d'une valeur par défaut, mais que name ne l'est pas, @Meal(name=« fries ») est valide, mais @Meal(id=1) ne l'est pas.
 */
@Repeatable(Meals.class)
public @interface Meal {
    int id() default 0;
    String name();
}
