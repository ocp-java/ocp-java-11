package com.gnilapon.annotaions.classes;

public class Aaaa {

    private String s;

    /**
     * Puisque le code a l'intention de remplacer la méthode equals de la classe Object, l'annotation @Override devrait être appliquée. Cependant, l'implémentation donnée ne surcharge pas correctement la méthode equals de la classe Object (l'argument vers equals devrait être de type Object et non A). Le compilateur génère donc une erreur.  Conformément à JLS 9.6.4.4 @Override Les programmeurs surchargent parfois une déclaration de méthode alors qu'ils veulent la remplacer, ce qui entraîne des problèmes subtils. L'annotation Override permet de détecter rapidement ces problèmes. L'exemple classique concerne la méthode equals. Les programmeurs écrivent ce qui suit dans la classe Foo : public boolean equals(Foo that) { ... } alors qu'ils veulent écrire : public boolean equals(Object that) { ... } C'est parfaitement légal, mais la classe Foo hérite de l'implémentation de la méthode equals de la classe Object, ce qui peut entraîner des bogues subtils.
     * @param a
     * @return
     */
    //1
    //@Override
    public boolean equals(Aaaa a){ //override the equals methods
        return this.s != null && this.s.equals(a.s);
    }
}
