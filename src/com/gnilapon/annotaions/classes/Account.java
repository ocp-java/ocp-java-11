package com.gnilapon.annotaions.classes;


/**
 * Vous ne pouvez omettre le nom de l'élément lorsque vous spécifiez une valeur que si le nom de l'élément est value et que vous ne spécifiez qu'une seule valeur.
 * En d'autres termes, si vous spécifiez des valeurs pour plusieurs éléments, vous devez utiliser le format elementName=elementValue pour chaque élément.
 * L'ordre des éléments n'est pas important.
 */
public class Account {
    @JSONField(name="acctid")
    private String id;
    @JSONField
    private String name;
    @JSONField(name="address")
    public String getAddress(){
        //valid code
        return "";
    }
    //other valid code not shown

}
