package com.gnilapon.annotaions.classes;

public @interface AuthorInfo {
    String[] name() default "je suis";
    int entryTime() default 0;
}

class A0{ @AuthorInfo String x = ""; }

class A1{ void m1(@AuthorInfo int x){ } }

