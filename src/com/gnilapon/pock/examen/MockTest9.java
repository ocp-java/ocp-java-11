package com.gnilapon.pock.examen;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MockTest9 {

    enum Switch{ ON, OFF }
    public static void main(String[] args) {
        var s =  Switch.ON;
        if( s == Switch.OFF) { System.out.println("It is off!"); }

        if( s.equals(Switch.OFF)) { System.out.println("It is off!"); }

        switch(s) { case OFF : System.out.println("It is off!"); break; }
    }
}

class Ao{
    public static void sM1() {  System.out.println("In base static");
    }
}
class Bo extends Ao{
    //public static void sM1() {  System.out.println("In sub static"); }
    //public  void sM1() {  System.out.println("In sub non-static"); }
}


class NewException extends Exception {}

class AnotherException extends Exception {}

class ExceptionT{
    public static void main(String[] args) throws Exception{
        try{
            m3();
        }
        finally{
            m2();
        }
        /*catch (NewException e){
            System.out.printf(e.getMessage());
        }*/
    }

    public static void m2() throws NewException { throw new NewException(); }

    public static void m3() throws AnotherException{ throw new AnotherException(); }

}


class A1 {
public int getCode(){ return 2;}
        }

class AA1 extends A1 {
    public void doStuff() {
    }

    public static void main(String[] args) {
        A a = null;
        AA aa = null;

        a = aa;

        /**
         * pour mettre le grand dans le petit , il faut caster
         */
        //aa = a;
    }
}


/**
 * WordProcessor fails compilation because it does not override the process method correctly.
 * GenericProcessor fails compilation because it inherits two process methods with incompatible return types.
 */
interface Processor {    Iterable process(); }
interface ItemProcessor extends Processor{    Collection process(); }
//interface WordProcessor extends Processor{    String process(); }

/*interface GenericProcessor extends ItemProcessor, WordProcessor{

}*/


/**
 * Compilation failure due to //3.
 * @param <T>
 */
class Counter<T>{  //1
    T t;
    public  <T> int count(T[] ta, T t){  //2
        //this.t = t;  //3
        int count = 0;
        for(T x : ta){
            count =  x == t ? count+1: count;//4
         }
        return count;
    }
}


/**
 * Question 22
 * It will not compile.
 *
 * TestClass.java: incompatible types found: boolean required: int case x < 5 :System.out.println("BIG"); break;
 */
class TestClassSwitch{
    public static void main(String args[]){
        var x = Integer.parseInt(args[0]);
        switch(x){
            //case x < 5 :   System.out.println("BIG"); break;
            //case x > 5 :   System.out.println("SMALL");
            default :    System.out.println("CORRECT"); break;
        }
    }
}


/**
 * Question 17
 *
 * Compilation failure
 *
 * public static <T> Collector<T,?,Long> counting()
 */
class ArraysFilerCounting{
    public static void main(String[] args) {
        List<String> names = Arrays.asList("charles", "chuk", "cynthia", "cho", "cici");
        //int x = names.stream().filter(name->name.length()>4).collect(Collectors.counting());
        System.out.println("x");
    }
}


/**
 * Question 14
 * setting p to a->a<0; will produce no output.
 * setting p to a->a%2==0; will produce 12.
 */
class ArrayListPredicate{
    /**
     * setting p to a->a%2==0; will produce 12.
     * @param args
     */
    public static void main(String[] args) {
        var nums = List.of(1, 2, 3, 4, 5, 6, 7).stream();
        Predicate<Integer> p =  a->a<0;
        //Predicate<Integer> p = //a predicate goes here
        Optional <Integer> value = nums.filter(p).reduce((a, b)->a+b);
        value.ifPresent(System.out::println);
    }

    /**
     * setting p to a->a%2==0; will produce 12
     * @param args
     */
    public static void main1(String[] args) {
        var nums = List.of(1, 2, 3, 4, 5, 6, 7).stream();
        Predicate<Integer> p =  a->a%2==0;
        //Predicate<Integer> p = //a predicate goes here
        Optional <Integer> value = nums.filter(p).reduce((a, b)->a+b);
        value.ifPresent(System.out::println);
    }
}

/**
 * Question 13
 * 48
 *
 */
class CountWhileLoop{
    public static void main(String[] args) {
        int count = 0, sum = 0;
        do{
            if(count % 3 == 0) continue;
            sum+=count;
        }
        while(count++ < 11);
        System.out.println(sum);
    }
}


/**
 * Question 12
 * The following method will compile and run without any problems.
 *
 * True
 *
 * byte, char, short, int, String, and enums. Wrapper classes Byte, Character, Short, and Integer are allowed as well.
 * Note that long, float, double, and boolean are not allowed.
 */
class switchTestByte{
    public void switchTest(byte x){
        switch(x){
            case 'b':   // 1
            default :   // 2
            case -2:    // 3
            case 80:    // 4
        }
    }
    public static void main(String[] args) {
        new switchTestByte().switchTest((byte) 'b');

        Map<String, List<Object>> grouping = new ArrayList<>().stream().collect( Collectors.groupingBy(s ->s.toString()));
    }
}


/**
 * Question 7
 * new Object[]{ "aaa", new Object(), new ArrayList(), 10};
 * 10 is a primitive and not an Object but due to auto-boxing it will be converted into an Integer object and that object will then be stored into the array of Objects.
 *
 * new Object[]{ "aaa", new Object(), new ArrayList(), new String[]{""} };
 * Every array is an Object so new String[]{""} is also an Object and can be placed in an array of objects.
 */
class ArryObject{
    public static void main(String[] args) {
        var t = new Object[]{ "aaa", new Object(), new ArrayList(), 10};
        var b = new Object[]{ "aaa", new Object(), new ArrayList(), new String[]{""} };
    }
}

/**
 * Question 1
 * [3]
 *
 * There are several things going on here: 1. IntStream.range returns a sequential ordered IntStream from startInclusive (inclusive) to endExclusive (exclusive) by an incremental step of 1. Therefore, is1 contains 1, 2. 2. IntStream.rangeClosed returns a sequential ordered IntStream from startInclusive (inclusive) to endInclusive (inclusive) by an incremental step of 1. Therefore, is2 contains 1, 2, 3. 3. IntStream.concat returns a lazily concatenated stream whose elements are all the elements of the first stream followed by all the elements of the second stream. Therefore, is3 contains 1, 2, 1, 2, 3. 4. is3 is a stream of primitive ints. is3.boxed() returns a new Stream containing Integer objects instead of primitives. This allows the use various flavors of collect method available in non-primitive streams. [IntStream does have one collect method but it does not take a Collector as argument.] 5. Collectors.groupingBy(k->k) creates a Collector that groups the elements of the stream by a key returned by the function k->k, which is nothing but the value in the stream itself. Therefore, it will group the elements into a Map<Integer, List<Integer>> containing: {1=[1, 1], 2=[2, 2], 3=[3]} 6. Finally, get(3) will return [3].
 */
class IntStreamCollectorsGroupingBy{
    public static void main(String[] args) {
        /*IntStream is1 = IntStream.range(1, 3);
        IntStream is2 = IntStream.rangeClosed(1, 3);
        IntStream is3 = IntStream.concat(is1, is2);
        Object val = is3.boxed().collect(Collectors.groupingBy(k->k)).get(3);
        System.out.println(val);
        int[] intArr = new int[0];
        int t = intArr.length;

        Integer i1 = 1;
        Integer i2 = new Integer(1);

        System.out.println(i1.equals(i2));*/


        var a = new int[]{ 1, 2, 3, 4, 5};
        var b = new int[]{ 1, 2, 3, 4, 5, 3};
        var c = new int[]{ 1, 2, 3, 4, 5, 6};
        int x = Arrays.compare(a, c);
        int y = Arrays.compare(b, c);
        System.out.println(x+" "+y);
    }
}

//class MyString extends String{    MyString(){ super(); } }

class MyException0 extends Throwable{}
class MyException10 extends MyException0{}
class MyException20 extends MyException0{}
class MyException30 extends MyException20{}
class ExceptionTestEx{
    void myMethod() throws MyException0{
        throw new MyException30();
    }
    public static void main(String[] args){
        ExceptionTest et = new ExceptionTest();
        try{
            et.myMethod();
        }
        catch(MyException0 me){
            System.out.println("MyException thrown");
        }
        /**
         * It fails to compile
         * In this case, catch for MyException3 cannot follow catch for MyException because if MyException3 is thrown, it will be caught by the catch clause for MyException. And so, there is no way the catch clause for MyException3 can ever execute. And so it becomes an unreachable statement.
         */
        //catch(MyException30 me3){System.out.println("MyException30 thrown"); }
        finally{
            System.out.println(" Done");
        }
    }
}


