package com.gnilapon.pock.examen;

public class ConsoleTest {
    /**
     Il imprimera le mot de passe saisi par l'utilisateur.
     * @param args
     */
    public static void main(String[] args) {

        var c = System.console(); //1
        char[] line = c.readPassword("Please enter your pwd:"); //2
        System.out.println("Pwd is " + new String(line));

    }
}
