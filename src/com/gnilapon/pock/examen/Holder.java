package com.gnilapon.pock.examen;

public class Holder {
    int value = 1;
    Holder link;

    public Holder(int val) {
        this.value = val;
    }

    public static void main(String[] args) {
        final var a = new Holder(5);
        var b = new Holder(10);
        a.link = b;
        b.link = setIt(a, b);
        System.out.println(a.link.value + " " + b.link.value);
    }

    /**
     * Lorsque la méthode setIt() est exécutée, x.link = y.link, x.link devient nul parce que y.link est nul, de sorte que a.link.value provoque une NullPointerException.
     *
     * @param x
     * @param y
     * @return
     */
    public static Holder setIt(final Holder x, final Holder y) {
        x.link = y.link;
        return x;
    }

}
