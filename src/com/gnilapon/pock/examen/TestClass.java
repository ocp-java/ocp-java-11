package com.gnilapon.pock.examen;

import com.gnilapon.javadatatypes.Student2;
import com.gnilapon.pock.examen.classes.Book;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.DayOfWeek.FRIDAY;

public class TestClass {
    public static void main0(String[] args) {
        try(BufferedReader bfr = new BufferedReader(new FileReader("c:\\works\\a.java"))){
            String line = null;
            while( (line = bfr.readLine()) != null){
                System.out.println(line);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Unmodifiable collections using of/copyOf and Collections.unmodifiableXXX methods
     * java.util.List and java.util.Set have of and copyOf static factory methods that provide a convenient way to create unmodifiable lists/sets.  The of methods accept either an array or multiple individual parameters. If you pass it a collection, it will be treated as a regular object i.e. it will return a list/set containing the same collection object instead of returning a list/set containing the objects that the passed collection contains.  The copyOf, on the other hand, accepts only a Collection. It iterates through the passed Collection and adds all the elements of that Collection in the returned list/set.  Here are a few important points about these methods:
     * They return unmodifiable copies of the original List/Set. Thus, any operation that tries to modify the returned list throws an java.lang.UnsupportedOperationException.
     * The list/set returned by the of/copyOf methods is completely independent of the original collection. Thus, if you modify the original collection after passing it to of/copyOf methods, those changes will not be reflected in the list returned by the of/copyOf methods.
     * They do not support null elements. Thus, if your array contains a null and if you try to create a List using List.of, it will throw a NullPointerException.
     *
     * Collections.unmodifiableXXX methods
     * java.utils.Collections class also has several variations of unmodifiableXXX static methods (such as unmodifiableList(List ), unmodifiableSet(Set ), and unmodifiableMap(Map ) ). These method return an unmodifiable view of the underlying collection. The fundamental difference between Collections.unmodifiableXXX and List.of/copyOf methods is that Collections.unmodifiableList returns a view (instead of a copy) into the underlying list. Which means, if you make any changes to the underlying list after creating the view, those changes will be visible in the view. Further, Collections.unmodifiableList has no problem with nulls.  The word unmodifiable in unmodifiableXXX method name refers to the fact that you cannot modify the view using a reference to view.
     * @param args
     */
    public static void main1(String[] args) {
        ArrayList<SUV> al3 = new ArrayList<>();
        Drivable d1 = al3.get(0);
        ArrayList<SUV> al4 = new ArrayList<>();
        Car c2 = al4.get(0);
        ArrayList<Car> al2 = new ArrayList<>();
        Drivable c1 = al2.get(0);


        var numA = new Integer[]{1, null, 3}; //1
        var list1 = List.of(numA); //2
        var list2 = Collections.unmodifiableList(list1); //3
        numA[1] = 2; //4
        System.out.println(list1+" "+list2);

        /**
         * Il n'y a aucun problème avec les lignes //2 et //3. La méthode forEach de List prend une instance de java.util.function.Consumer et les deux ont des expressions lambda valides qui capturent cette interface. L'expression x->x=x+1 ; ne modifie pas réellement les éléments de la liste. Vous pouvez considérer x comme une variable locale temporaire et le changement de la valeur de x n'affecte pas l'élément actuel de la liste.
         */
        List<Integer> names = Arrays.asList(1, 2, 3); //1
        names.forEach(x->x=x+1); //2
        names.forEach(System.out::println); //3
    }

    public static void main2(String[] args) {
        List<Integer> values = Arrays.asList(2, 4, 6, 9); //1
        Predicate<Integer> check = (Integer i) -> {
            System.out.println("Checking");
            return i == 4; //2
            };
        /**
         * Observez que l'expression lambda utilisée pour instancier le prédicat utilise Integer comme type de la variable. Pour que cela fonctionne, la partie déclaration doit être typée en Integer. Voici ce que cela donne : Predicate<Integer> even = (Integer i)-> i%2==0;//3 Une autre option consiste à utiliser le type Object comme ceci : Predicate even = (Object i)-> ((Integer)i)%2==0 ; ou Predicate even = i -> ((Integer)i)%2==0 ;
         */
        //Predicate even = (Integer i)-> i%2==0;  //3

        //values.stream().filter(check).filter(even).count(); //4
    }

    /**
     * Un BufferedReader peut contenir n'importe quel Reader. FileReader et BufferedReader sont tous deux des lecteurs, de sorte que //2 et //3 sont valables.
     * @param args
     * @throws Exception
     */
    public static void main3(String[] args) throws Exception {
        var f = new File("x");   //1
        var bfr1 = new BufferedReader(new FileReader(f)); //2
        var bfr2 = new BufferedReader( bfr1 ); //3
        var pw = new PrintWriter(String.valueOf(new FileReader(f))); //4
    }

    public static void main4(String[] args) {
        String sentence1 = "Carpe diem. Seize the day, boys. Make your lives extraordinary.";
        String sentence2 = "Frankly, my dear, I don't give a damn!";
        String sentence3 = "Do I look like I give a damn?";
        List<String> sentences = Arrays.asList(sentence1, sentence2, sentence3);

        /**
         * 1. La méthode flatMap remplace un élément d'un flux par des éléments d'un nouveau flux généré à partir de l'élément d'origine. Ici, les éléments d'origine sont les phrases. Chacun de ces éléments est remplacé dans le flux par les éléments générés par l'application de str.split(« [ ,.!?\r\n] », qui convertit essentiellement le flux de phrases en un flux de mots.
         * 2. Le flux est maintenant filtré et seuls les éléments dont la longueur est supérieure à 0 sont autorisés à figurer dans le flux.
         * 3. Enfin, distinct supprime tous les doublons.
         */
        Stream<String> strm = sentences.stream().flatMap(str-> Stream.of(str.split("[ ,.!?\r\n]"))).filter(s->s.length()>0).distinct();
        //Stream<String> strm = sentences.stream().map(str->Stream.of(str.split("[ ,.!?\r\n]"))).filter(s->s.length()>0) .distinct();
        strm.forEach(System.out::println);
    }


    /**
     * Le collecteur créé par Collectors.toMap lance une exception java.lang.IllegalStateException si l'on tente de stocker une clé qui existe déjà dans la carte. Si vous souhaitez collecter des éléments dans une carte et si vous vous attendez à des entrées en double dans la source, vous devez utiliser la méthode Collectors.toMap(Function, Function, BinaryOperator). Le troisième paramètre est utilisé pour fusionner les entrées en double afin d'obtenir une seule entrée. Par exemple, dans ce cas, vous pouvez faire : Collectors.toMap(b->b.getTitle(), b->b.getPrice(), (v1, v2)->v1+v2) Ce collecteur additionnera les valeurs des entrées qui ont la même clé. Par conséquent, il imprimera : Autant en emporte le vent 15.0 Atlas Shrugged 15.0
     * @param args
     */
    public static void main5(String[] args) {
        List<Book> books = Arrays.asList(
                new Book("Gone with the wind", 5.0),
                new Book("Gone with the wind", 10.0),
                new Book("Atlas Shrugged", 15.0) );
        books.stream().collect(Collectors.toMap((Book::getTitle), Book::getPrice)).forEach((a, b)->System.out.println(a+" "+b));
        books.stream().collect(Collectors.toMap(Book::getTitle, Book::getPrice, Double::sum) ).forEach((a, b)->System.out.println(a+" "+b));
    }


    /**
     * Cette question est basée sur 2 concepts :
     * 1. i == ++j n'est pas la même chose que i == j++ ; Dans le cas de i == ++j, j est d'abord incrémenté et ensuite comparé à i. Alors que dans le cas de i == j++ ;, j est d'abord comparé à i et ensuite incrémenté.
     * 2. L'opérateur |, lorsqu'il est appliqué à des opérandes booléens, garantit que les deux côtés sont évalués. Contrairement à || qui n'évalue pas le côté droit si le résultat peut être connu en évaluant uniquement le côté gauche.
     * Voyons maintenant les valeurs de i et j à chaque étape :
     *              int i = 1 ;
     *              int j = i++ ;
     *              // on attribue 1 à j et on incrémente i à 2 if( (i==++j) | (i++ == j) )
     *              // incrémente j (donc, j devient 2) et //puis compare avec i, donc, il retournera vrai.
     *              //puisque c'est |, évaluer la condition suivante :
     *              //compare i avec 2 et incrémente i. Cela signifie que i devient 3. { i+=j ; //i = 3+2 = 5 }
     *              System.out.println(i) ; //imprime 5
     * @param args
     */
    public static void main6(String[] args) {
        int i = 1;
        int j = i++;
        if( (i==++j) | (i++ == j) ){
            i+=j;
        }
        System.out.println(i);
    }


    /**
     * Le seul problème avec le code donné est qu'un flux ne peut pas être réutilisé une fois qu'une opération terminale a été invoquée sur lui. C'est pourquoi la ligne 6 va générer une exception java.lang.IllegalStateException : le flux a déjà fait l'objet d'une opération ou a été fermé.
     * @param args
     */
    /**
     * It will print 34 if lines 4 to 7 are replaced with: primeStream.collect(Collectors.partitioningBy(test1, Collectors.counting())) .values().forEach(System.out::print);
     * @param args
     */
    public static void main(String[] args) {
        List<Integer> primes = Arrays.asList(2, 3, 5, 7, 11, 13, 17); //1
        Stream<Integer> primeStream = primes.stream(); //2
        Predicate<Integer> test1 = k->k<10; //3
        /**long count1 = primeStream.filter(test1).count();//4
        Predicate<Integer> test2 = k->k>10; //5
        long count2 = primeStream.filter(test2).count(); //6
        //System.out.println(count1+" "+count2); //7
        */
        primeStream.collect(Collectors.partitioningBy(test1, Collectors.counting())) .values().forEach(System.out::print);
    }



    /**
     * Le code ne sera pas compilé.
     * Notez que la déclaration de retour après le bloc finally est inaccessible. Sinon, si cette ligne n'était pas là, les choix 1, 2, 3 sont valables.
     * @param s
     * @return
     */
    public float parseFloat(String s){
        float f = 0.0f;
        try{
            f = Float.valueOf(s).floatValue();
            return f ;
        }
        catch(NumberFormatException nfe){
            System.out.println("Invalid input " + s);
            f = Float.NaN ;
            return f;
        }
        finally { System.out.println("finally");  }
        //return f ; ce bloc finally est inaccessible.
    }

    /**
     * otez que if et else n'agissent pas en cascade. Ils sont comme des accolades ouvrantes et fermantes. if (flag) //1 if (flag) //2 System.out.println(« True False ») ; else // 3 This closes //2 System.out.println(« True True ») ; else // 4 This closes //1 System.out.println(« False False ») ; Ainsi, else à //3 est associé à if à //2 et else à //4 est associé à if à //1
     * @param flag
     */
    public void ifTest(boolean flag){
        if (flag)   //1
        if (flag)   //2
        System.out.println("True False");
        else        // 3
        System.out.println("True True");
        else        // 4
        System.out.println("False False");
    }

}

abstract class Vehicle{ }
interface Drivable{ }
class Car extends Vehicle implements Drivable{

}
class SUV extends Car  { }



class A {
    public int getCode(){ return 2;}
    protected String a(String s){
        return s;
    }
    public Number getCode(List<Integer> i){ return i.size();}
    protected Number getCode(int i){ return i;}
    public List<? super Integer>  getCode(Number i){ return Collections.singletonList(i);}
    public List<? extends Number>  getCode(Integer i){ return Collections.singletonList(i);}
}

class AA extends A {
    /**
     * La classe AA tente de remplacer la méthode getCode() de la classe A, mais son type de retour est incompatible avec la méthode getCode de la classe A.  Lorsque le type de retour de la méthode surchargée (c'est-à-dire la méthode de la classe de base/supérieure) est une primitive, le type de retour de la méthode surchargée (c'est-à-dire la méthode de la classe inférieure) doit correspondre au type de retour de la méthode surchargée.  Dans le cas des objets, la méthode de la classe de base peut avoir un type de retour covariant, ce qui signifie qu'elle peut renvoyer soit la même classe, soit un objet de la sous-classe. Par exemple, si la méthode de la classe de base est : public A getA(){ ... }, une sous-classe peut la remplacer par : public AA getA(){ ... } car AA est une sous-classe de A.
     * @return
     */
    //public long getCode(){ return 3;}

    public Integer getCode(List<Integer> i){ return i.size()+2;}
    @Override
    protected Integer getCode(int i){ return i*i;}
    @Override
    public List<? super Number>  getCode(Number i){ return Collections.singletonList(i);}
    @Override
    public List<? extends Integer>  getCode(Integer i){ return Collections.singletonList(i);}
    protected String a(String s){
        return s+"je";
    }
}


interface House{
    default void lockTheGates(){
        System.out.println("Locking House");
    }
}
interface Office {
    public void lockTheGates();
}
//class HomeOffice implements House, Office{}//1 }


interface T1{

}
interface T2{
    int VALUE = 10;
    void m1();
}
interface T3 extends T1, T2{
    public void m1();
    public void m1(int x);
}


interface Bozo{
    int type = 0;
    public void jump();
}
class Type1Bozo implements Bozo{
    public Type1Bozo(){
        /**
         * Fields defined in an interface are ALWAYS considered as public, static, and final. Even if you don't explicitly define them as such. In fact, you cannot even declare a field to be private or protected in an interface. Therefore, you cannot assign any value to 'type' outside the interface definition.
         */
        //type = 1;
    }
    public void jump(){
        System.out.println("jumping..."+type);
    }
    public static void main(String[] args){
        Bozo b = new Type1Bozo();
        b.jump();
    }
}

 class TestClasss{
    static int si = 10;
    public static void main (String args[]){
        new TestClass();
    }
    TestClasss(){
        System.out.println(this);
    }
    public String toString(){
        return "TestClass.si = "+this.si;
    }
}




interface Birdie {
    void fly();
}
class Dino implements Birdie {
    public void fly(){ System.out.println("Dino flies");
    }
    public void eat(){ System.out.println("Dino eats");
    }
}
class Bino extends Dino {
    public void fly(){ System.out.println("Bino flies");
    }
    public void eat(){ System.out.println("Bino eats");
    }
}
class TestClassss {
    public static void main(String[] args)    {
        List<Birdie> m = new ArrayList<>();
        m.add(new Dino());
        m.add(new Bino());
        for(Birdie b : m) {
            if (b != null)
            b.fly();
            /**
             * Notez que dans la boucle for, b a été déclaré comme étant de type Birdie. Mais Birdie ne définit pas la méthode eat(), donc le compilateur n'autorisera pas b.eat() même si la classe réelle de l'objet auquel se réfère b a une méthode eat().
             */
            //b.eat();
        }
    }
}
class ABCD{
    int x = 10;
    static int y = 20;
}
class MNOP extends ABCD{
    int x = 30;
    static int y = 40;
}

class TestClass4 {
    public static void main(String[] args) {
        ABCD nop = new MNOP();
        System.out.println(nop.x+", "+ nop.y);
    }
}


/**
 * Un livelock se produit lorsque plusieurs threads tentent d'acquérir un verrou, mais qu'aucun d'entre eux ne parvient à l'obtenir et qu'aucun d'entre eux ne se retrouve bloqué.  Le code donné tente d'acquérir des verrous dans une boucle non bloquante. Chaque thread vérifie si les données sont libres ou non jusqu'à ce qu'il obtienne le verrou pour l'objet Data. Cette approche, en général, peut provoquer un blocage (au lieu d'un blocage) mais dans ce cas, elle ne provoquera pas de blocage parce que les ressources sont acquises dans le même ordre par chaque thread. Ainsi, si une donnée d1 est acquise par le rédacteur 1, personne d'autre ne peut acquérir la donnée d2. Si un autre thread possède déjà le verrou de d2, la seule action possible pour ce thread est de libérer le verrou de d2, auquel cas le scripteur 1 acquerra le verrou de d2.
 */
class Writer{
    private static final int LOOPSIZE = 5;
    public synchronized void write(Data... da){
        for(int i=0; i<LOOPSIZE; i++){
            while(!da[0].own(this));
            while(!da[1].own(this));
            da[0].write(i); da[1].write(i);
            da[1].release();
            da[0].release();
        }
    }
}
class Data{
    Writer writer;
    int id;
    public Data(int id){ this.id = id; }
    public synchronized boolean own(Writer w){
        if(writer == null){
            writer  = w; return true;
        } else
            return false;     }
    public synchronized void release(){
        writer = null;
    }
    public void write(int i){
        System.out.println("data written W"+i+" D"+id);
    }
}
class TestClass5 {
    public static void main(String[] args) {
        var w1 = new Writer();
        var w2 = new Writer();
        var d1 = new Data(1);
        var d2 = new Data(2);
        new Thread(()-> w1.write(d1, d2)).start();
        new Thread(()-> w2.write(d1, d2)).start();
    }
}




class TestClass6{
    void probe(Object x) { System.out.println("In Object"); } //3

    void probe(Number x) { System.out.println("In Number"); } //2

    void probe(Integer x) { System.out.println("In Integer"); } //2

    void probe(Long x) { System.out.println("In Long"); } //4

    public static void main(String[] args){
        double a = 10;
        new TestClass6().probe(a);
    }
}

class TestClass7{
    public void method(Object o){
        System.out.println("Object Version");
    }

    /**
     * La raison en est très simple : la méthode la plus spécifique est appelée en fonction de l'argument. Ici, null peut être transmis aux trois méthodes, mais la classe FileNotFoundException est une sous-classe de IOException qui, à son tour, est une sous-classe d'Object. La classe FileNotFoundException est donc la plus spécifique. C'est donc cette méthode qui est appelée.
     * @param s
     */
    public void method(java.io.FileNotFoundException s){
        System.out.println("java.io.FileNotFoundException Version");
    }
    public void method(java.io.IOException s){
        System.out.println("IOException Version");
    }
    public static void main(String args[]){
        TestClass7 tc = new TestClass7();
        tc.method(null);
    }
}

class TestClass8{
    public void method(Object o){
        System.out.println("Object Version");
    }
    public void method(String s){
        System.out.println("String Version");
    }
    public void method(StringBuffer s){
        System.out.println("StringBuffer Version");
    }

    public static void main(String args[]){
        TestClass8 tc = new TestClass8();
        /**
         * Il y avait eu deux méthodes les plus spécifiques, la compilation n'aurait même pas eu lieu car le compilateur n'aurait pas été en mesure de déterminer quelle méthode appeler.
         */
        //tc.method(null);
    }

}

class A0{
    private String s;
    //1
    /**
     * Puisque le code a l'intention de remplacer la méthode equals de la classe Object, l'annotation @Override devrait être appliquée. Cependant, l'implémentation donnée ne surcharge pas correctement la méthode equals de la classe Object (l'argument vers equals devrait être de type Object et non A). Le compilateur génère donc une erreur.  Conformément à JLS 9.6.4.4 @Override Les programmeurs surchargent parfois une déclaration de méthode alors qu'ils veulent la remplacer, ce qui entraîne des problèmes subtils. L'annotation Override permet de détecter rapidement ces problèmes. L'exemple classique concerne la méthode equals. Les programmeurs écrivent ce qui suit dans la classe Foo : public boolean equals(Foo that) { ... } alors qu'ils veulent écrire : public boolean equals(Object that) { ... } C'est parfaitement légal, mais la classe Foo hérite de l'implémentation de la méthode equals de la classe Object, ce qui peut entraîner des bogues subtils.
     * @param a
     * @return
     */
    public boolean equals(A a){
        boolean b = true|false;
        //override the equals methods
        //return this.s != null && this.s.equals(a.s);
        return true;
    }
}


/**
 * Opérateur | (OU bit à bit)
 * L'opérateur | effectue une opération OR bit à bit. Chaque bit du résultat est mis à 1 si l'un des bits correspondants des opérandes est 1.
 */
class BitwiseOrExample {
    public static void main(String[] args) {
        int a = 5;  // 0101 en binaire
        int b = 3;  // 0011 en binaire
        int result = a | b;  // 0111 en binaire, soit 7 en décimal
        System.out.println("Result of 5 | 3: " + result);
    }
}

/**
 * Opérateur & (ET bit à bit)
 * L'opérateur & effectue une opération AND bit à bit. Chaque bit du résultat est mis à 1 si les deux bits correspondants des opérandes sont 1.
 */
class BitwiseAndExample {
    public static void main(String[] args) {
        int a = 5;  // 0101 en binaire
        int b = 3;  // 0011 en binaire
        int result = a & b;  // 0001 en binaire, soit 1 en décimal
        System.out.println("Result of 5 & 3: " + result);
    }
}

/**
 * Utilisation Pratique
 * Les opérateurs | et & peuvent être utilisés dans divers contextes, par exemple :
 *
 * Manipulation des bits : Pour effectuer des opérations sur des bits individuels dans des applications nécessitant un contrôle bas niveau, comme la programmation système.
 * Masquage : Utiliser & pour sélectionner certains bits d'un nombre.
 * Mise en drapeau : Utiliser | pour définir des bits spécifiques (comme des drapeaux) dans des structures de données.
 * Exemple Pratique : Masquage de Bits
 * Supposons que nous voulons vérifier si un nombre est pair ou impair en utilisant l'opérateur & :
 */
class CheckEvenOdd {

    public void method1(int i, double d) {
        int j = (i * 30 - 2) / 100;

        POINT1:
        for (; j < 10; j++) {
            var flag = false;
            while (!flag) {
                if (d > 0.5) break POINT1;
            }
        }

        /**
         * Rappelez-vous qu'une instruction break ou continue étiquetée doit toujours exister à l'intérieur de la boucle où l'étiquette est déclarée. Ici, if(j == 4) break POINT1 ; est une interruption étiquetée qui se produit dans la deuxième boucle alors que l'étiquette POINT1 est déclarée pour la première boucle.
         */

        /**while (j > 0) {
            System.out.println(j--);
            if (j == 4) break POINT1;
        }*/

    }
        public static void main(String[] args) {
        int num = 10;  // Nombre à vérifier
        if ((num & 1) == 0) {
            System.out.println(num + " is even.");
        } else {
            System.out.println(num + " is odd.");
        }
    }
}

/**
 * Il s'agit d'un code simple destiné à vous embrouiller. Remarquez l'instruction for : for(int i=10 ; i<0 ; i--). i est initialisé à 10 et le test est i<0, ce qui est faux. Par conséquent, le contrôle n'entrera jamais dans la boucle for, aucun code bizarre ne sera exécuté et x restera 0, ce qui est imprimé.
 */
class TestClass9{
    public static void main(String args[]){
        int x  = 0;
        labelA:   for (var i=10; i<0; i--){
            var j = 0;
            labelB:
            while (j < 10){
                if (j > i) break labelB;
                if (i == j){
                    x++;
                    continue labelA;
                }
                j++;
            }
            x--;
        }
        System.out.println(x);
    }
}


/**
 * Notez que Double.parseDouble(str) renvoie un double primitif mais qu'il sera transformé en Double lorsqu'il sera assigné à dVal car dVal est une variable Double. La comparaison d'une référence numérique d'un objet enveloppant avec une variable primitive numérique en utilisant == est légalement valide. L'objet enveloppant sera décomposé en une valeur primitive, puis les deux valeurs primitives seront comparées. C'est pourquoi, si vous comparez une valeur numérique primitive avec un objet enveloppant primitif contenant la même valeur, le résultat est vrai.  Cependant, une comparaison entre deux types différents de variables enveloppantes (par exemple, une variable Double et une variable Integer) entraînera un échec de la compilation. Ainsi, par exemple, si iVal avait été déclaré comme Integer iVal = 0, une erreur de compilation serait levée à //2.  Le code suivant illustre les règles décrites ci-dessus :    Double d1 = 1000.0 ; double d2 = 1000.0 ; int i1 = 1000 ; Integer i2 = Integer.valueOf(1000) ; //renvoie un nouvel objet Integer contenant 1000 Integer i3 = Integer.valueOf(1000) ;
 * //renvoie un nouvel objet Integer contenant 1000 System.out.println(d1 == d2) ; //imprime true, d1 est unboxed //et les deux valeurs sont les mêmes System.out.println(d1 == i1) ; //imprime true, d1 est unboxed //et les deux valeurs sont les mêmes System. out.println(i1 == i2) ; //imprime true, i2 est unboxed //et les deux valeurs sont identiques System.out.println(i2 == i3) ; //imprime false, i2 et i3 pointent vers //deux objets Integer différents i2 = Integer. valueOf(100) ; //returns a cached Integer object because //the integer value is between -128 and 127 i3 = Integer.valueOf(100) ; //returns the same cached Integer object //because the integer value is between -128 and 127 System.out.println(i2 == i3) ; //prints true, because i2 and i3 //point to the same Integer object System.out.println(d1 == i2) ; //ne se compilera pas.Il n'y a pas d'unboxing ici //parce qu'aucun des opérandes n'est primitif.
 */
class TestClass10{
    public static void main(String[] args) {
        String str = "10";
        int iVal = 0;
        Double dVal = 0.0;
        try{
            iVal = Integer.parseInt(str, 2);  //1
            if((dVal = Double.parseDouble(str)) == iVal){ //2
                System.out.println("Equal");
            }
        }catch(NumberFormatException e){
            System.out.println("Exception in parsing");
        }
        System.out.println(iVal+" "+dVal);
    }
}



class NewClass {
    public static Optional<String> getGrade(int marks){
        Optional<String> grade = Optional.empty();
        if(marks>50){
            grade = Optional.of("PASS");
        } else {
            grade.of("FAIL");
        }
        return grade;
    }
    public static void main(String[] args) {
        Optional<String> grade1 = getGrade(50);
        Optional<String> grade2 = getGrade(55);
        System.out.println(grade1.orElse("UNKNOWN"));
        if(grade2.isPresent()){
            grade2.ifPresent(x->System.out.println(x));
        }else{
            System.out.println(grade2.orElse("Empty"));
        }
    }
}

class Test{
    public static void main(String[] args){
        if (args[0].equals("open"))
            if (args[1].equals("someone"))
                System.out.println("Hello!");
        else System.out.println("Go away "+ args[1]);
    }
}

    class TestClass11{
        public static void main(String args[]){
            int k = 0;
            int m = 0;
            for ( var i = 0; i <= 3; i++){
                k++;

                System.out.println( k);

                if ( i == 2){
                    break;
                }
                m++;
            }
            System.out.println( k + ", " + m );
        }
    }

class InitTest{
    public InitTest(){
        s1 = sM1("1");
    }
    static String s1 = sM1("a");
    String s3 = sM1("2");{
        s1 = sM1("3");
    }
    static{
        s1 = sM1("b");
    }
    static String s2 = sM1("c");
    String s4 = sM1("4");
    public static void main(String args[]){
        InitTest it = new InitTest();
    }
    private static String sM1(String s){
        System.out.println(s);  return s;
    }
}


class LoopTest{
    public static void main(String args[]) {
        var counter = 0;
        outer:
        for (var i = 0; i < 3; i++) {
            middle:
            for (var j = 0; j < 3; j++) {
                inner:
                for (var k = 0; k < 3; k++) {
                    if (k - j > 0) {
                        break middle;
                    }
                    counter++;
                    System.out.println("breaking middle "+counter);
                }
            }
        }
        System.out.println(counter);
    }
}


abstract class Bang{
    //abstract void f();  // LINE 0
    final    void g(){}
    // final    void h(){} // LINE 1
    protected static int i;
    private int j;
}
final class BigBang extends Bang{
    // BigBang(int n) { m = n; } // LINE 2
    public static void main(String args[]){
        Bang mc = new BigBang();
    }
    // @Override // LINE 3
    void h(){}

    /**
     * Le constructeur par défaut (sans argument) n'est automatiquement créé que si la classe ne définit aucun constructeur. Ainsi, dès que //2 est inséré, le constructeur par défaut ne sera pas créé.
     */
    void k(){ i++; } // LINE 4
    // void l(){ j++; } // LINE 5
    int m;
}


class Testes{    public static void main(String[ ] args){       var a = new int[]{ 1, 2, 3, 4 };       int[] b = { 2, 3, 1, 0 };       System.out.println( a [ b[3] ] );    } }


interface House1{
    public default String getAddress(){
        return "101 Main Str";   }
}
interface Office1 {
    public default String getAddress(){
        return "101 Smart Str";   }
}
class HomeOffice1 implements House1, Office1 {
    public String getAddress() {
        return "R No 1, Home";
    }
}




interface House2{
    public default String getAddress(){
        return "101 Main Str";
    }
}
interface Office2 {
    public static String getAddress(){
        return "101 Smart Str";
    }
}
interface WFH2 extends House2, Office2{
    private boolean isOffice(){
        return true;
    }
}

class HomeOffice2 implements House2, Office2{
    public String getAddress(){
        return "R No 1, Home";
    }
}

class TestClass12 {
    public static void main(String[] args) {
    Office2 off = new HomeOffice2();  //1
    //System.out.println(off.getAddress()); //2 ne fonctionne pas.
        Queue<Number> col = new ArrayDeque<>();
        col.add(1);
        var list1 = List.of(col); //1
        System.out.println(list1);
        col.add(2); //2
        col.add(3); //2
        col.add(4); //2
        //col.add(null); //2
        var list2 = List.copyOf(col); //3
        System.out.println(list1+", "+list2);

        List<String> ls = Arrays.asList("Tom Cruise", "Tom Hart","Tom Hanks", "Tom Brady");
        Predicate<String> p = str->{
            System.out.println("Looking...");
            return str.contains("Tom");
        };
        boolean flag = ls.stream().filter(str->str.length()>8).allMatch(p);
        System.out.println(flag);
    }

}

class Test2{
    public static void main(String[ ] args){
        var a = new int[]{ 1, 2, 3, 4 };
        int[] b = { 2, 3, 1, 0 };
        /**
         * Dans un accès à un tableau, l'expression située à gauche des crochets semble être entièrement évaluée avant toute partie de l'expression située entre les crochets. Dans l'expression a[(a=b)[3]], l'expression a est entièrement évaluée avant l'expression (a=b)[3] ; cela signifie que la valeur originale de a est récupérée et mémorisée pendant que l'expression (a=b)[3] est évaluée. Ce tableau référencé par la valeur originale de a est ensuite inscrit en indice par une valeur qui est l'élément 3 d'un autre tableau (peut-être le même tableau) qui était référencé par b et qui est maintenant également référencé par a. Il s'agit donc en fait de a[0] = 1. Notez que si l'évaluation de l'expression à gauche des parenthèses se termine brusquement, aucune partie de l'expression entre les parenthèses ne semblera avoir été évaluée.
         */
        System.out.println( a [ (a = b)[3] ] );

        List<Integer> names = Arrays.asList(1, 2, 3);
        //System.out.println(names.stream().collect(Collectors.mapping(x->x, Collectors.summarizingInt(x->x))).getSum());
        System.out.println(names.stream().collect( Collectors.summarizingInt(x->x)).getSum());
    }
}

class A000 {
    public void doA(int k) throws Exception {  // 0
        for(int i=0; i< 10; i++) {
            if(i == k)
                throw new Exception("Index of k is "+i); // 1
        }
    }
    public void doB(boolean f) throws Exception { // 2
        if(f) {
            doA(15); // 3
        }
        else return;
    }
    public static void main(String[] args) throws Exception { // 4
        A000 a = new A000();
        a.doB(args.length>0); // 5
        Locale myLocale = new Locale("ru", "RU");
        List<StringBuilder> messages = Arrays.asList(                         new StringBuilder(), new StringBuilder()); messages.stream().forEach(s->s.append("helloworld")); messages.forEach(s->{     s.insert(5,",");     System.out.println(s); });
        List<String> values = Arrays.asList("Java EE", "C#", "Python"); boolean flag = values.stream().allMatch(str->{         System.out.println("Testing: "+str);         return str.equals("Java");         }); System.out.println(flag);
        var day = LocalDate.now().with(FRIDAY).getDayOfWeek();
        switch(day){
            case MONDAY:
            WEDNESDAY:
            THURSDAY:
            FRIDAY:
            System.out.println("working");
            case     SATURDAY:
                System.out.println("off");
        }
    }
}

class TestClass13{
    public static int operate(IntUnaryOperator iuo){
        return iuo.applyAsInt(5);
    }
    public static void main(String[] args) throws IOException {
        IntFunction<IntUnaryOperator> fo = a-> b->a-b;  //1
        int x = operate(fo.apply(20)); //2
          System.out.println(x);


        /**
         * Le compilateur se plaindra que la méthode intValue() n'est pas disponible dans Object. En effet, l'opérateur . est plus important que l'opérateur cast. Vous devez donc l'écrire comme suit : int k = ((Integer) t).intValue()/9 ; Maintenant, puisque les deux opérandes de / sont des ints, il s'agit d'une division entière. Cela signifie que la valeur résultante est tronquée (et non arrondie). Par conséquent, l'instruction ci-dessus affichera 11 et non 12.
         */
        //Object t = new Integer(107);
        // int k = (Integer) t.intValue()/9;
        // System.out.println(k);

        Path p1 = Paths.get("c:\\temp\\src\\foo.bar\\module-info.java");
        Path p2  = Paths.get("c:\\temp\\out\\foo.bar");
        Files.move(p1, p2);

    }
}


class TestClass14 implements T1, T2{    public void m1(){} }
//interface T1{    int VALUE = 1;    void m1(); } interface T2{    int VALUE = 2;    void m1(); }

class Am{
    public void mA(){
        System.out.println("Am mA");
    };
}

class Bm extends Am {
    public void mA(){
        System.out.println("Bm mA");
    }
    public void mB() {
        System.out.println("Bm mB");
    }
}

class Cm extends Bm {
    public void mC(){
        System.out.println("Cm mC");
    }
}
class Polymorphisme{
    public static void main(String[] args) {
        Am x = new Bm();
        Bm y = new Bm();
        Bm z = new Cm();
        x.mA();
        y.mA();
        y.mB();
        z.mA();
        z.mB();
    }
}


/**
 * InnerPeace en tant que classe imbriquée non statique
 */
class OuterWorld {
    public InnerPeace i;

    public OuterWorld() {
        i = new InnerPeace("none");
    }

    class InnerPeace {
        private String reason;

        InnerPeace(String reason) {
            this.reason = reason;
        }
    }

    public static void main(String[] args) {
        var out = new OuterWorld();
        var ip = out.new InnerPeace("yoga");
        System.out.println(out.i.reason);
    }
}

/**
 * InnerPeace en tant que classe imbriquée statique
 */
class OuterWorldSa {
    // Une instance de InnerPeace est créée avec "none" et assignée à i
    public InnerPeace i = new InnerPeace("none");

    // Classe imbriquée statique InnerPeace
    static class InnerPeace {
        private String reason;

        // Constructeur qui initialise reason
        InnerPeace(String reason) {
            this.reason = reason;
        }
    }

    public static void main(String[] args) {
        // Création d'une instance de InnerPeace directement
        var ip = new InnerPeace("yoga");

        // Création d'une instance de OuterWorld
        var out = new OuterWorldSa();

        // Accès à la valeur de reason
        System.out.println(out.i.reason);
    }
}

class ForEarch{
    public static void generateMultiplicationTable(int number){
        Stream<Integer> sin = Stream.of(1, 2, 3 );
        Consumer<Integer> c1 = System.out::print;
        Consumer<Integer> c2 = x->{ System.out.println(" * "+number+" = "+x*number);};
        //INSERT CODE HERE
        sin.forEach(c1.andThen(c2));
    }
    public static void main(String[] args) throws Exception{
        generateMultiplicationTable(2);
        var forE =new ForEarch();
        T2 t1 = forE.new TmplClass();
        final var value = t1.VALUE;
    }

    class TmplClass implements T1, T2{
        public void m1(String s) {

        }
        public void m1(){}
    }
    interface T1{
        int VALUE = 1;
        void m1();
    }
    interface T2{
        int VALUE = 2;
        void m1(String s);
        void m1();
    }
}


class TestClassException {
    public static void m1() throws Exception{
        throw new Exception("Exception from m1");
    }
    public static void m2() throws Exception{
        try{
            m1();
        }catch(Exception e){
            //Can't do much about this exception so rethrow it
            throw e;
        }finally{
            throw new RuntimeException("Exception from finally");
        }
    }
    public static void main(String[] args) {

        try{
            m2();
        }catch(Exception e){
            Throwable[] ta = e.getSuppressed();
            for(Throwable t : ta) {
                System.out.println(t.getMessage());
            }
        }
    }
}


/**
 * Le code donné pose deux problèmes : 1. si vous déclarez qu'un champ est final, il doit être explicitement initialisé au moment où la création d'un objet de la classe est terminée. Vous pouvez donc soit l'initialiser immédiatement : private final double ANGLE = 0 ; soit l'initialiser dans le constructeur ou dans un bloc d'instance.  2. Puisque ANGLE est final, vous ne pouvez pas modifier sa valeur une fois qu'elle est définie. Par conséquent, la méthode setAngle ne sera pas compilée non plus.
 */
class Triangle{
    public int base;
    public int height;
    //private final double ANGLE;

    public  void setAngle(double a){
        //ANGLE = a;
    }

    public static void main(String[] args) {
        var t = new Triangle();
        t.setAngle(90);
    }
}


/**
 * Vous pouvez avoir plusieurs blocs catch pour attraper différents types d'exceptions, y compris des exceptions qui sont des sous-classes d'autres exceptions. Toutefois, la clause catch pour les exceptions plus spécifiques (c'est-à-dire une SubClassException) doit précéder la clause catch pour les exceptions plus générales (c'est-à-dire une SuperClassException). Si ce n'est pas le cas, une erreur de compilation se produit, car l'exception plus spécifique est inaccessible.  Dans ce cas, catch for MyException3 ne peut pas suivre catch for MyException parce que si MyException3 est lancée, elle sera rattrapée par la clause catch de MyException. Il est donc impossible que la clause catch pour MonException3 s'exécute. Elle devient donc une déclaration inaccessible.
 */
class MyException extends Throwable{}
class MyException1 extends MyException{}
class MyException2 extends MyException{}
class MyException3 extends MyException2{}
class ExceptionTest{
    void myMethod() throws MyException0{
        throw new MyException30();
    }
    public static void main(String[] args){
        ExceptionTest et = new ExceptionTest();
        try{
            et.myMethod();
        }catch(MyException0 me){
            System.out.println("MyException thrown");
        //}  catch(MyException3 me3){
        //    System.out.println("MyException3 thrown");
        } finally{
            System.out.println(" Done");
        }
    }
}


/**
 * Observez qu'une variable locale nommée global est définie dans le bloc d'essai par l'instruction String global = arg ;. Cette variable n'est accessible qu'à l'intérieur de ce bloc d'essai et elle fait de l'ombre au champ d'instance du même nom à l'intérieur de ce bloc d'essai. Puisque arg est 333, cette variable locale globale est fixée à 333.  C'est cette variable qui est transmise à l'appel à parseInt. Par conséquent, la valeur est fixée à 333. Cependant, lorsque vous imprimez global dans la méthode parse, le global défini dans le bloc try est hors de portée et le champ d'instance nommé global est utilisé. Par conséquent, la valeur affichée est 111.  Il n'y a pas d'exception car 333 peut être analysé en un int. Si vous transmettez à la méthode parseInt une chaîne de caractères qui ne peut pas être analysée en un nombre entier, une exception java.lang.NumberFormatException sera levée.
 */
class ATest {
    String global = "111";
    public int parse(String arg){
        int value = 0;
        try{
            String global = arg;
            value = Integer.parseInt(global);
        }
        catch(Exception e){
            System.out.println(e.getClass());
        }
        System.out.print(global+" "+value+" ");
        return value;
    }
    public static void main(String[] args) {
        ATest ct = new ATest();
        System.out.print(ct.parse("333"));
    }
}

/**
 * Arrays.sort(sa);
 * Cette ligne utilise la méthode sort de la classe Arrays pour trier le tableau sa. La méthode Arrays.sort trie les éléments d'un tableau en ordre croissant en utilisant l'ordre naturel des éléments (par exemple, l'ordre alphabétique pour les chaînes de caractères).
 * Exemple :
 * Si sa est un tableau de chaînes de caractères, par exemple {"john", "alice", "bob"}, après le tri, sa sera {"alice", "bob", "john"}.
 * Cette ligne utilise la méthode binarySearch de la classe Arrays pour effectuer une recherche binaire du terme "bob" dans le tableau sa, et imprime l'index où l'élément "bob" se trouve dans le tableau trié sa.
 *
 * System.out.println(Arrays.binarySearch(sa, "bob"));
 * La méthode Arrays.binarySearch recherche l'élément spécifié dans le tableau trié et retourne l'index de l'élément si celui-ci est trouvé. Si l'élément n'est pas trouvé, elle retourne (-(insertion point) - 1), où insertion point est l'endroit où l'élément devrait être inséré pour maintenir l'ordre de tri.
 */
class TestClass15 {
    static String[] sa = { "charlie", "bob", "andy", "dave" };
    public static void main(String[] args)     {
        Arrays.sort(sa);
        System.out.println(Arrays.binarySearch(sa, "abo0tyb"));
    }
}


class Testclass {
    public static void main(String[] args) {
        NavigableMap<String, String> mymap = new TreeMap<String, String>();
        mymap.put("a", "apple");
        mymap.put("b", "boy"); mymap.put("c", "cat");
        mymap.put("aa", "apple1"); mymap.put("bb", "boy1");
        mymap.put("cc", "cat1");
        /**
         * Le TreeMap est trié par les clés dans l'ordre naturel :
         * mymap ==>> {a=apple, aa=apple1, b=boy, bb=boy1, c=cat, cc=cat1}
         */

        /**
         * Suppression de la dernière et de la première entrée :
         */
        mymap.pollLastEntry(); //LINE 1
        mymap.pollFirstEntry(); //LINE 2

        /**
         * Après ces opérations, mymap devient :
         * mymap ==>> {aa=apple1, b=boy, bb=boy1, c=cat}
         */

        /**
         * Création d'une sous-carte avec tailMap :
         */
        NavigableMap<String, String> tailmap = mymap.tailMap("bb", false); //LINE 3
        /**
         * tailMap("bb", false) crée une vue de la portion de mymap dont les clés sont strictement supérieures à "bb".
         * Donc, tailmap contiendra : {c=cat}
         */


        /** NavigableMap<String, String> tailmapsup = mymap.tailMap("bb", true); // LINE 3
         * tailMap("bb", true) crée une vue de la portion de mymap dont les clés sont supérieures ou égales à "bb".
         * Donc, tailmap contiendra :
         * tailmapsup ==> {bb=boy1, c=cat}
         */


        /**
         * Suppression de la première entrée dans tailmap :
         */
        System.out.println(tailmap.pollFirstEntry()); //LINE 4

        /**
         * pollFirstEntry() retire et retourne la première entrée de tailmap, qui est aussi retirée de mymap car tailmap est une vue sur mymap.
         * tailmap et mymap deviennent :
         * tailmap: {}
         * mymap: {aa=apple1, b=boy, bb=boy1}
         */
        System.out.println(mymap.size()); //LINE 5


    }

}


/**
 * Si la classe interne est non statique, tous les membres statiques et non statiques de la classe externe sont accessibles (sinon, seuls les membres statiques sont accessibles). Les options 1 et 2 sont donc valables.
 * Avant Java 8, seules les variables locales finales étaient accessibles à la classe interne, mais dans Java 8, même les variables locales effectivement finales de la méthode sont également accessibles à la classe interne définie dans cette méthode. L'option 4 est donc correcte.
 */
class BookStore {
    private static final int taxId = 300000;
    private String name;
    public String searchBook( final String criteria )    {
        int count = 0;

        /**
         * sum est une variable locale et sa valeur change dans le code (en raison de sum++).
         * Par conséquent, elle n'est pas effectivement finale et n'est donc pas accessible à partir de la classe interne.
         */
        int sum = 0;
        sum++;
        class Enumerator       {
            String iterate( int k)          {
                //line 1
                return "";
            }
            // lots of code.....
        }
        // lots of code.....
        return "";
    }
}


/**
 * Les objets d'une classe qui n'est pas marquée Serializable ne peuvent pas être sérialisés.
 * Dans cette question, la classe Booby n'implémente pas Serializable et ses objets ne peuvent donc pas être sérialisés.
 * La classe Dooby implémente Serializable et puisque Tooby étend Dooby, elle est également Serializable.
 * Maintenant, lorsque vous sérialisez un objet de la classe Tooby, seuls les membres de données de Dooby et Tooby seront sérialisés.
 * Les membres de données de Booby ne seront pas sérialisés.
 * Ainsi, la valeur de i (qui est 100) au moment de la sérialisation ne sera pas sauvegardée dans le fichier.
 * Lors de la relecture de l'objet (c'est-à-dire la désérialisation), les constructeurs des classes sérialisables ne sont pas appelés.
 * Leurs membres de données sont définis directement à partir des valeurs présentes dans les données sérialisées.
 * Le constructeur des classes non sérialisables est appelé.
 * Ainsi, dans ce cas, les constructeurs de Tooby et Dooby ne sont pas appelés, mais le constructeur de Booby l'est.
 * Par conséquent, i est défini dans le constructeur à 10 et j et k sont définis en utilisant les données du fichier à 20 et 30 respectivement.
 */
class Booby{     int i; public Booby(){ i = 10; System.out.print("Booby"); } }
class Dooby extends Booby implements Serializable {     int j; public Dooby(){ j = 20; System.out.print("Dooby"); } }
class Tooby extends Dooby{     int k; public Tooby(){ k = 30; System.out.print("Tooby"); } }
class TestClass16 {   public static void main(String[] args) throws Exception{
    var t = new Tooby();
    t.i = 100;
    var oos  = new ObjectOutputStream(new FileOutputStream("c:\\temp\\test.ser"));
    oos.writeObject(t); oos.close();
    var ois = new ObjectInputStream(new FileInputStream("c:\\temp\\test.ser"));
    t = (Tooby) ois.readObject();ois.close();
    System.out.println(t.i+" "+t.j+" "+t.k);

    List<String> names = Arrays.asList("greg", "dave", "don", "ed", "fred" );
    Map<Integer, Long> data = names.stream().collect(Collectors.groupingBy(                              String::length,
            Collectors.counting()) ); System.out.println(data.values());

    List<Student2> slist = List.of(new Student2("S1", 40),
            new Student2("S2", 35),
            new Student2("S3", 30));
    Consumer<Student2> increaseMarks = s->s.addMarks(10);
    slist.forEach(increaseMarks); slist.forEach(Student2::debug);
    //Object[] sa = { 100, 100.0, "100" };         Collections.sort(Arrays.asList(sa), null);         System.out.println(sa[0]+" "+sa[1]+" "+sa[2] );
}
}

enum SIZE {     TALL, JUMBO, GRANDE; }  class CoffeeMug {      public static void main(String[] args)     {         var hs = new HashSet<SIZE>();         hs.add(SIZE.TALL); hs.add(SIZE.JUMBO); hs.add(SIZE.GRANDE);         hs.add(SIZE.TALL); hs.add(SIZE.TALL); hs.add(SIZE.JUMBO);         for(SIZE s : hs) System.out.print(s+" ");     } }