package com.gnilapon.pock.examen;

public class SimpleLoop {
    public static void main(String[] args) {
        int i = 0, j = 10;
        while (i <= j) {
            i++;
            j--;
            System.out.println(i + " " + j);
        }
        System.out.println(i + " " + j);
    }
}
