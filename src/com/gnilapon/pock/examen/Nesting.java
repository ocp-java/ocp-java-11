package com.gnilapon.pock.examen;


public class Nesting {
    public static void main(String[] args)     {
        B.C obj = new B( ).new C( );
    }
}
class Aaaa  {
    char c;
    Aaaa(char c) {
        this.c = c;
    }
}
class B extends Aaaa {
    char c = 'a';
    B( ) {
        super('b');
    }
    class C extends Aaaa    {
        char c = 'c';

        /***
         * Chaque objet de classe interne non statique possède une référence à son objet de classe externe, à laquelle on peut accéder en faisant OuterClass.this.
         * Ainsi, l'expression B.this.c fera référence au c de B, dont la valeur est 'a'. Dans une classe interne non statique, 'InnerClass.this' est équivalent à 'this'.
         * Ainsi 'C.this.c' fait référence au c de C qui est 'c'.
         * L'expression super.c permet d'accéder à la variable de A, la superclasse de C, dont la valeur est 'd'.
         */
        C( ) {  super('d');
            System.out.println(B.this.c);
            System.out.println(C.this.c);
            System.out.println(super.c);
        }
    }
}
