package com.gnilapon.pock.examen;

public class DataWrapper {
    public static Integer wiggler(Integer x) {
        Integer y = x + 10;
        x++;
        System.out.println(x);
        return y;
    }

    /**
     * 1. Les objets enveloppants sont toujours immuables. Par conséquent, lorsque dataWrapper est passé dans la méthode wiggler(), il n'est jamais modifié, même lorsque x++ ; est exécuté. Cependant, x, qui pointait vers le même objet que dataWrapper, se voit attribuer un nouvel objet Integer (différent de dataWrapper) contenant 6.
     * 2. Si les deux opérandes de l'opérateur + sont numériques, il les additionne. Ici, les deux opérandes sont Integer 5 et Integer 15, donc il les décompose, les ajoute et imprime 20.
     *
     * @param args
     */
    public static void main(String[] args) {
        Integer dataWrapper = 5;
        Integer value = wiggler(dataWrapper);
        System.out.println(dataWrapper + value);
    }
}
