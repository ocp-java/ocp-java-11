package com.gnilapon.pock.examen.classes;

/**
 * Par défaut, toutes les méthodes d'une interface sont publiques et abstraites. Il n'est donc pas nécessaire de spécifier explicitement le mot-clé « abstract » pour la méthode draw() si vous faites de Shape une interface. Il n'est donc pas nécessaire de spécifier explicitement le mot-clé « abstract » pour la méthode draw() si vous faites de Shape une interface.
 */
interface Shape {
    public abstract void draw();
}

class Circle implements Shape {
    @Override
    public void draw() {

    }
}
