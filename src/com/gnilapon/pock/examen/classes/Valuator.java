package com.gnilapon.pock.examen.classes;

import java.util.concurrent.atomic.AtomicInteger;

public class Valuator {
    public AtomicInteger status = new AtomicInteger(0);

    public void valuate() {
        int oldstatus = status.get();
        /* valid code here */
        int newstatus = status.getAndAdd(2);//determine new status
        // INSERT CODE HERE
        /**
         * status.compareAndSet(oldstatus, newstatus) ;
         * compareAndSet(expectedValue, newValue) est exactement conçu à cette fin. Elle vérifie d'abord si la valeur actuelle est identique à la valeur attendue et, si c'est le cas, la met à jour avec la nouvelle valeur.
         */
        status.compareAndSet(oldstatus, newstatus);
    }
}
