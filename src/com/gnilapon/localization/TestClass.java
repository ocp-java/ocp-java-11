package com.gnilapon.localization;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class TestClass {

    /**
     * Pour obtenir un NumberFormat pour une locale spécifique, y compris la locale par défaut, appelez l'une des méthodes d'usine de NumberFormat, telle que getInstance().
     * En général, n'appelez pas directement les constructeurs de DecimalFormat, car les méthodes de la fabrique de NumberFormat peuvent renvoyer des sous-classes autres que DecimalFormat.
     * Si vous devez personnaliser l'objet format, faites quelque chose comme ceci :
     * NumberFormat f = NumberFormat.getInstance(loc) ; if (f instanceof DecimalFormat) { ((DecimalFormat) f).setDecimalSeparatorAlwaysShown(true) ; }
     *
     * @param args
     */
    public static void main0(String[] args) {
        double amount = 53000.35;
        Locale jp = new Locale("jp", "JP");
        //1 create formatter here.

        /**
         * Ceci est valable car java.text.NumberFormat s'étend à partir de java.text.Format .
         * Le type de retour de la méthode getCurrencyInstance() est NumberFormat.
         */
        Format formatter0 = NumberFormat.getCurrencyInstance(jp);
        /**
         * getCurrencyInstance est en fait défini dans NumberFormat. Cependant, comme DecimalFormat étend NumberFormat, ceci est valide.
         * Pour formater un nombre au format devise, vous devez utiliser getCurrencyInstance() au lieu de getInstance() ou getNumberInstance().
         * Ceci s'imprimera : JPY 53,000
         */
        NumberFormat formatter = DecimalFormat.getCurrencyInstance(jp);

        System.out.println(   formatter.format(amount)  );
    }

    /**
     * Pour les besoins de l'examen, vous devez connaître les codes de base pour imprimer une date. Les plus importants sont m, M, d, D, e, y, s, S, h, H et z.
     * Vous pouvez consulter les détails complets de ces modèles ici : https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/format/DateTimeFormatter.html
     * Le point important à comprendre est la façon dont la longueur du motif détermine la sortie du texte et des nombres.
     * Texte : Le style de texte est déterminé en fonction du nombre de lettres du motif utilisé. En dessous de 4 lettres, la forme courte est utilisée. Exactement 4 lettres de motif utiliseront la forme complète. La forme étroite est utilisée pour 5 lettres patron. Les lettres-types « L », « c » et « q » spécifient la forme autonome des styles de texte.
     * Nombre : Si le nombre de lettres est égal à un, la valeur est éditée en utilisant le nombre minimum de chiffres et sans remplissage. Dans le cas contraire, le nombre de chiffres est utilisé comme largeur de la zone de sortie, et la valeur est mise à zéro si nécessaire. Les lettres modèles suivantes ont des contraintes sur le nombre de lettres. Une seule lettre de « c » et « F » peut être spécifiée.
     * Il est possible de spécifier jusqu'à deux lettres parmi « d », « H », « h », « K », « k », « m » et « s ». Il est possible de spécifier jusqu'à trois lettres de « D ».
     * Nombre/Texte : Si le nombre de lettres du motif est égal ou supérieur à 3, utiliser les règles de texte ci-dessus. Dans le cas contraire, utilisez les règles relatives aux nombres ci-dessus.
     * @param args
     * @throws Exception
     */
    public static void main1(String[] args) throws Exception     {
        LocalDate d = LocalDate.now();
        //INSERT CODE HERE
        /**
         * Si vous utilisez le même motif plus de 5 fois, une exception sera levée.
         * Pour imprimer la forme complète du composant, utilisez le motif 4 fois.
         * Ainsi, eeee et MMMM fonctionneront parfaitement ici.
         * Exactement 4 lettres de motif utiliseront la forme complète.
         */
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("eeee d'st day of' MMMM yyyy");
        String s = dtf.format(d);
        System.out.println(s);
    }

    /**
     * message.properties est le fichier de base de cet ensemble de ressources.
     * Étant donné que le fichier spécifique à la langue et à la région est également présent (_en_UK), il sera également chargé et les valeurs de ce fichier seront superposées aux valeurs du fichier de base.
     * Rappelez-vous que si un autre fichier de propriétés nommé message_en.properties était également présent, les valeurs de ce fichier seraient superposées aux valeurs de message.properties, puis les valeurs de message_en_UK.properties seraient superposées à l'union qui en résulterait.
     * Ainsi, les valeurs d'une clé seront recherchées dans l'ordre suivant : message_fr_UK.properties>message_fr.properties > message.properties. Si une clé n'est pas trouvée dans une liasse de priorité supérieure (également appelée liasse enfant), la liasse de priorité inférieure ou la liasse enfant sera vérifiée.
     * @param args
     */
    public static void main2(String[] args) {
        Locale myloc = new Locale.Builder().setLanguage("en").setRegion("UK").build(); //L1
        ResourceBundle msgs = ResourceBundle.getBundle("com.gnilapon.localization.resources.message", myloc);
        Enumeration<String> en = msgs.getKeys();
        while(en.hasMoreElements()){
            String key = en.nextElement();
            String val = msgs.getString(key);
            System.out.println(key+" : "+val);
        }
    }

    /**
     * If helloMsg is defined in all the four properties files, the value from mymsgs_en.properties will be displayed.
     *
     * If helloMsg is not defined in mymsgs_en.properties file but is defned in mymsgs.properties and mymsgs_en_IT.properties, then the value from mymsgs.properties will be displayed.
     * @param args
     */
    public static void main3(String[] args) {
        Locale.setDefault(Locale.ITALY);
        Locale loc = new Locale.Builder().setLanguage("en").build();
        ResourceBundle rb = ResourceBundle.getBundle("com.gnilapon.localization.resources.message", loc);
        System.out.println(rb.getString("helloMsg"));
    }

    /**
     * Lors de la récupération d'un paquet de messages, vous passez explicitement une locale (jp/JP). Par conséquent, il essaiera d'abord de charger appmessages_jp_JP.properties. Comme ce fichier n'est pas présent, il cherchera un paquet de ressources pour la locale par défaut. Comme vous changez la locale par défaut en « fr », « CA », il cherchera appmessages_fr_CA.properties, qui n'est pas présent non plus.
     * Rappelez-vous que lorsqu'un ensemble de ressources n'est pas trouvé pour une locale donnée, la locale par défaut est utilisée pour charger l'ensemble de ressources.  Tous les efforts sont faits pour charger un ensemble de ressources s'il n'est pas trouvé et il existe plusieurs options de repli (en l'absence de appmessages_fr_CA.properties, il cherchera appmessages_fr.properties). En dernier recours, il essaiera de charger un ensemble de ressources sans informations sur les paramètres linguistiques, c'est-à-dire appmessages.properties dans le cas présent. (Une exception est levée si même ce resource bundle n'est pas trouvé).
     * Comme appmessages.properties est disponible, le code ne lèvera jamais d'exception, quelle que soit la locale.
     * @param args
     */
    public static void main4(String[] args) {
        Locale.setDefault(new Locale("fr", "CA")); //Set default to French Canada
        Locale l = new Locale("jp", "JP");
        ResourceBundle rb = ResourceBundle.getBundle("com.gnilapon.localization.resources.message", l);
        String msg = rb.getString("greetings");
        System.out.println(msg);
    }

    /**
     * Lors de la récupération d'un paquet de messages, vous passez explicitement une locale (jp/JP). Par conséquent, il essaiera d'abord de charger appmessages_jp_JP.properties. Comme ce fichier n'est pas présent, il cherchera un paquet de ressources pour la locale par défaut. Comme vous changez la locale par défaut en « fr », « CA », il cherchera appmessages_fr_CA.properties, qui n'est pas présent non plus.
     *
     * Rappelez-vous que lorsqu'un ensemble de ressources n'est pas trouvé pour une locale donnée, la locale par défaut est utilisée pour charger l'ensemble de ressources.  Tous les efforts sont faits pour charger un ensemble de ressources s'il n'est pas trouvé et il existe plusieurs options de repli (en l'absence de appmessages_fr_CA.properties, il cherchera appmessages_fr.properties). En dernier recours, il essaiera de charger un ensemble de ressources sans informations sur les paramètres linguistiques, c'est-à-dire appmessages.properties dans le cas présent. (Une exception est levée si même ce resource bundle n'est pas trouvé).
     *
     * Comme appmessages.properties est disponible, le code ne lèvera jamais d'exception, quelle que soit la locale.
     * @param args
     */
    public static void main5(String[] args) {
        Locale myloc = new Locale.Builder().setLanguage("hinglish").setRegion("IN").build(); //L1
        ResourceBundle msgs = ResourceBundle.getBundle("com.gnilapon.localization.resources.mymsgs", myloc);
        Enumeration<String> en = msgs.getKeys();
        while(en.hasMoreElements()){
            String key = en.nextElement();
            String val = msgs.getString(key);
            System.out.println(key+"="+val);
        }
    }

    /**
     * Rappelez-vous que la méthode parse() de DateFormat et NumberFormat lance java.text.ParseException. Elle doit donc être déclarée dans la clause throws de la méthode main() ou l'appel à parse() doit être enveloppé dans un bloc try/catch.
     * @param args
     */
    /**
     * Notez que si vous corrigez le problème d'exception, la sortie dépendra de l'endroit où vous exécutez ce programme. Par exemple, si vous l'exécutez sur une machine dont la locale est US, il imprimera deux nombres inégaux parce que la chaîne générée par le formateur créé en utilisant la locale française sera 123 456 789. Cette chaîne ne sera pas analysée correctement par le second formateur, qui est créé en utilisant la locale par défaut (qui est US). En fait, il l'analysera comme 123.
     * @param args
     * @throws ParseException
     */
    public static void main(String[] args) throws ParseException {
        double amount = 123456.789;
        Locale fr = new Locale("fr", "FR");
        NumberFormat formatter = NumberFormat.getInstance(fr);
        String s = formatter.format(amount) ;
        formatter = NumberFormat.getInstance();
        Number amount2 = formatter.parse(s);
        System.out.println( amount + " " + amount2 );
    }
}
