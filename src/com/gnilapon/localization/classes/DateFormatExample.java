package com.gnilapon.localization.classes;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatExample {
    public static void main(String[] args) {
        // Crée une date actuelle
        Date currentDate = new Date();

        // Formateur de date
        SimpleDateFormat dateFormat;

        // Affiche le mois (1-12)
        dateFormat = new SimpleDateFormat("M");
        System.out.println("Mois (1-12) : " + dateFormat.format(currentDate));

        // Affiche le mois en lettres (Jan, Feb, ...)
        dateFormat = new SimpleDateFormat("MMM");
        System.out.println("Mois en lettres (Jan, Feb, ...) : " + dateFormat.format(currentDate));

        // Affiche le jour du mois (1-31)
        dateFormat = new SimpleDateFormat("d");
        System.out.println("Jour du mois (1-31) : " + dateFormat.format(currentDate));

        // Affiche le jour de l'année (1-365)
        dateFormat = new SimpleDateFormat("D");
        System.out.println("Jour de l'année (1-365) : " + dateFormat.format(currentDate));

        // Affiche le jour de la semaine (1-7)
        dateFormat = new SimpleDateFormat("u");
        System.out.println("Jour de la semaine (1-7) : " + dateFormat.format(currentDate));

        // Affiche l'année (2 chiffres)
        dateFormat = new SimpleDateFormat("yy");
        System.out.println("Année (2 chiffres) : " + dateFormat.format(currentDate));

        // Affiche l'année (4 chiffres)
        dateFormat = new SimpleDateFormat("yyyy");
        System.out.println("Année (4 chiffres) : " + dateFormat.format(currentDate));

        // Affiche les secondes
        dateFormat = new SimpleDateFormat("s");
        System.out.println("Secondes : " + dateFormat.format(currentDate));

        // Affiche les millisecondes
        dateFormat = new SimpleDateFormat("S");
        System.out.println("Millisecondes : " + dateFormat.format(currentDate));

        // Affiche l'heure (1-12)
        dateFormat = new SimpleDateFormat("h");
        System.out.println("Heure (1-12) : " + dateFormat.format(currentDate));

        // Affiche l'heure (0-23)
        dateFormat = new SimpleDateFormat("H");
        System.out.println("Heure (0-23) : " + dateFormat.format(currentDate));

        // Affiche le fuseau horaire
        dateFormat = new SimpleDateFormat("z");
        System.out.println("Fuseau horaire : " + dateFormat.format(currentDate));
    }
}
