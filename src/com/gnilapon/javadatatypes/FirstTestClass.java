package com.gnilapon.javadatatypes;

public class FirstTestClass{
    public void testRefs(String str, StringBuilder sb){
        str = str + sb.toString();
        sb.append(str);
        str = null;
        sb = null;
    }
    public static void main0(String[] args){
        String s = "aaa";
        StringBuilder sb = new StringBuilder("bbb");
        new FirstTestClass().testRefs(s, sb);
        System.out.println("s="+s+" sb="+sb);
    }

    public static void main1(String args[] ){
        StringBuilder sb = new StringBuilder("12345678");
        sb.setLength(5);
        sb.setLength(10);
        System.out.println(sb.length());
    }

    public static void main2(String[] args){
        int k = 1;
        int[] a = { 1 };
        k += (k = 4) * (k + 2);
        a[0] += (a[0] = 4) * (a[0] + 2);
        System.out.println( k + " , " + a[0]);
    }

    public static void main3(String[] args){
        switch(Integer.parseInt(args[1]))  //1
        {
            case 0 :
                var bb = false; //2
                break;
            case 1 :
                    bb = true; // 3
                break;
        }
        //if(bb) System.out.println(args[2]); //4
        /**
         * It will not compile because of if(b) because b is declared in the switch block and it is out of scope after the switch block ends.
         * Pay close attention to question text.
         * It may seem to test you on one concept but actually it could be testing something entirely different.
         */
    }

    public static int m1(int i){
        return ++i;
    }

    public static void main4(String[] args) {

        int k = m1(args.length);
        k += 3 + ++k;
        System.out.println(k);

        char a = 'a', b = 98; //1
        int a1 = a; //2
        int b1 = b; //3
        System.out.println((char)a1+(char)b1); //4
    }


    class X{ int val = 10; }
    class Y extends X{ Y val = null; //1

    }
    public class TestClass extends X{
        public void main(String[] args){
            Y y = new Y();
            //int k = (X) y.val ; //2
                //System.out.println(k);
        }
    }

    class Data {
        private int x = 0;
        private String y = "Y";
        public Data(int k){
            this.x = k;
        }
        public Data(String k){
            this.y = k;
        }
        public void showMe(){
            System.out.println(x+y);
        }
    }

    public void main(String[] args){
        new Data(10).showMe();
        new Data("Z").showMe();
    }

}
