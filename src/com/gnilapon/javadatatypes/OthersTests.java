package com.gnilapon.javadatatypes;

public class OthersTests {

    public static class SM{
        public String checkIt(String s){
            if(s.length() == 0 || s == null){
                return "EMPTY";
            }
            else return "NOT EMPTY";
        }

        public static void main(String[] args){
            SM a = new SM();
            System.out.println(a.checkIt(null));
        }
    }
}
