package com.gnilapon.javadatatypes;

/**
 * Which statements can be inserted at line 1 in the following code to make the program write x on the standard output when run?
 */

public class AccessTest{
    String a = "x";
    static char b = 'x';
    static String  c = "x";
    class Inner{
        String  a = "y";
        String  get(){
            String c = "temp";
            // Line 1
            return c;
        }
    }

    AccessTest() {
        System.out.println(  new Inner().get()  );
    }

    public static void main(String args[]) {
        /**
         * It will reassign "temp" to c
         */
        // c = c;

        /**
         * It will assign "y" to c.
         */

        // c = this.a;

        /**
         *
         * Since b is a static field of AccessTest, this is valid.
         */

        c = ""+AccessTest.b;
        //c = AccessTest.this.a;
        c = ""+b;


        new AccessTest();
    }
}
