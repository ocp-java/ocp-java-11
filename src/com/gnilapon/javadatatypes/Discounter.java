package com.gnilapon.javadatatypes;


/**
 * What will the following code print when compiled and run?
 */

public class Discounter {
    static double percent; //1
    int offset = 10, base= 50; //2
    /*public static double calc(double value) {
        var coupon, offset, base; //3
        if(percent <10){ //4
            coupon = 15;
            offset = 20;
            base = 10;
        }
        return coupon*offset*base*value/100; //5
    }*/
    public static void main(String[] args) {
        //System.out.println(calc(100));
    }
}
/**
 * There are two problems here -
 * 1. 'var' is not allowed in a compound declaration.
 * In other words, you can define only one variable using var.
 * 2. Variables declared with var must be assigned a value in the declaration itself because without the value, the compiler will not be able to infer the type of the variable.
 */
