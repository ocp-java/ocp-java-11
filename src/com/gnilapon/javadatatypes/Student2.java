package com.gnilapon.javadatatypes;

public class Student2 {
    private String s;
    private int i;

    public Student2(String s, int i) {
        this.s = s;
        this.i = i;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public void addMarks(int n) {
        this.i +=n;
    }

    public void debug(){         System.out.println(s+":"+i);     }
}
