package com.gnilapon.javadatatypes;

public class StringKwnoladge {


    /**
     * First the value of 'str1' is evaluated (i.e. one).
     * Now, before the method is called, the operands are evaluated, so str1 becomes "two".
     * so "one".equals("two") is false.
     * @param agrs
     */
    public static void main(String[] agrs){
        String str1 = "one";
        String str2 = "two";
        System.out.println( str1.equals(str1=str2) );
        //String str3 =  str1.replace(str1,str2);
        System.out.println( str1=str2 );
        System.out.println( mothode().replace("on",str2) );
    }

    public static String mothode(){
        return "onerryr";
    }

    /**
     * ❌ You answered incorrectly You had to select 2 options
     *
     * You cannot call methods like "1234".replace('1', '9'); and expect to change the original String. **** OK ****
     * Calling methods such as replace that seem to alter the given String do not actually alter the String at all. They create a new String with the required changes and return that new String. Remember that Strings are immutable.
     *
     *
     * You cannot change a String object, once it is created.  **** OK ****
     *
     *
     * You can change a String object only by the means of its methods.
     *
     *
     * You cannot extend String class.
     * That's because it is final, not because it is immutable. You can have a final class whose objects are mutable.
     *
     *
     * You cannot compare String objects.
     * String class implements Comparable interface, which has the int compareTo(String ) method. So you can definitely compare two Strings.
     */
    public static String mothode(String s){
        return "onerryr";
    }
}
