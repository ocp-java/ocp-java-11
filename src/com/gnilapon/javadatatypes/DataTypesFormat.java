package com.gnilapon.javadatatypes;

public class DataTypesFormat {
    /**
     * primitives orders
     * boolean < byte < short < char < int < long < float < double.
     */
    //double d = 10,000,000.0;
    //double d1 = 10-000-000;
    double d3 = 10_000_000;
    //double d4 = 10 000 000;



    /**
     *  N'oubliez pas les règles suivantes pour les types primitifs :
     *  1. tout ce qui est plus grand qu'un int ne peut JAMAIS être assigné à un int ou à tout ce qui est plus petit qu'un int ( byte, char, ou short) sans un cast explicite.
     *  2. Les valeurs CONSTANTES jusqu'à int peuvent être assignées (sans cast) à des variables de taille inférieure (par exemple, short à byte) si la valeur est représentable par la variable (c'est-à-dire si elle s'adapte à la taille de la variable).
     *  3. les opérandes des opérateurs mathématiques sont TOUJOURS promus à au moins int (c'est-à-dire que pour octet * octet, les deux octets seront d'abord promus à int) et la valeur de retour sera AU MOINS int.
     *   Les opérateurs d'affectation composés ( +=, *=, etc.) ont des comportements étranges, il faut donc lire attentivement ce qui suit :  Une expression d'affectation composée de la forme E1 op= E2 est équivalente à E1 = (T)((E1) op (E2)), où T est le type de E1, sauf que E1 n'est évalué qu'une seule fois.
     *   Notez que la conversion implicite vers le type T peut être soit une conversion d'identité, soit une conversion primitive restrictive. Par exemple, le code suivant est correct : short x = 3 ; x += 4.6 ; et donne à x la valeur 7 car il est équivalent à : short x = 3 ; x = (short)(x + 4.6) ;
     */

    /**
     * Voici l'ordre croissant des types primitifs que vous avez mentionnés :
     *
     * boolean: Stocke les valeurs booléennes true ou false.
     * byte: 8 bits, permet de stocker des entiers compris entre -128 et 127.
     * char: 16 bits, utilisé pour stocker des caractères Unicode.
     * short: 16 bits, permet de stocker des entiers compris entre -32768 et 32767.
     * int: 32 bits, utilisé pour stocker des entiers compris entre -2^31 et (2^31)-1.
     * long: 64 bits, utilisé pour stocker des entiers compris entre -2^63 et (2^63)-1.
     * float: 32 bits, utilisé pour stocker des nombres décimaux à virgule flottante simple précision.
     * double: 64 bits, utilisé pour stocker des nombres décimaux à virgule flottante double précision.
     */

    byte b = 1;
    char c = 1;
    short s = 1;
    int i = 1;
    float f = 1;

    float ief= i;
    int fi = (int) f;
    short ss = (short) i;
    int ss1 = s;

    /**
     * b * b returns an int.
     * short s = b * b ;
     */
    int s1 = b * b ;
    /**
     * All compound assignment operators internally do an explicit cast.
     * short s *= b;
     */
     short s2 = (short) (s* b);

    /**
     * c + b returns an int
     * char c = c + b
     */
    int c1 = c + b ;

    /**
     * All compound assignment operators internally do an explicit cast.
     * s += i ;
     */
    short s3 = (short) (s+ i);


    /**
     * Operators , promotion, casting : Tough
     * L'expression b1 = i1 == i2 sera évaluée comme b1 = (i1 == i2) parce que == a une priorité plus élevée que =.
     * De plus, tout ce dont une instruction if a besoin, c'est d'un booléen.
     * Or, i1 == i2 renvoie false, qui est un booléen, et comme b1 = false est une expression et que toute expression a une valeur de retour (qui est en fait le côté gauche de l'expression), elle renvoie false, qui est à nouveau un booléen.
     * Par conséquent, dans ce cas, la condition else sera exécutée.
     *
     */

    public void evaluatePrint(){
        boolean b1 = false;
        int i1 = 2;
        int i2 = 3;
        if (b1 = i1 == i2){
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }

    /**
     * ❌ You answered incorrectly You had to select 4 options
     */
    //float f4 = 43e1; **43e1 is a double.**
    //var f1 = 1.0; **1.0 is a double.**
    //float f2 = f1; ** float < double.**

}
