package com.gnilapon.javadatatypes;

/**
 * Assuming that the following method will always be called with a phone number in the format ddd-ddd-dddd (where d stands for a digit), what can be inserted at //1 so that it will return a String containing the same number except its last four digits will be masked with xxxx?
 */
public class StringStringBuilder {
    public static String hidePhone(String fullPhoneNumber){
        //1 Insert code here
        //return new StringBuilder(fullPhoneNumber).substring(0, 8)+"xxxx";  //OK
        //return new StringBuilder(fullPhoneNumber).replace(8, 12, "xxxx").toString(); //OK

        /**
         * This will actually throw an IndexOutOfBoundsException because the call to append will look for characters starting from index 8 to 11 in string "xxxx", which has only 4 characters.
         */
        //return new StringBuilder(fullPhoneNumber).append("xxxx", 8, 12).toString();


        /**
         * This will return xxxxddd-ddd-.
         */
        //return new StringBuilder("xxxx").append(fullPhoneNumber, 0, 8).toString();

        return new StringBuilder("xxxx").insert(0, fullPhoneNumber, 0, 8).toString();
    }

    public static void main(String[] args) {
        System.out.println(hidePhone("xxxxddd-ddd"));
    }
}
