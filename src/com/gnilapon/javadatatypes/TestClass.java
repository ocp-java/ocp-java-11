package com.gnilapon.javadatatypes;

import java.util.ArrayList;

public class TestClass {
    //var students = new ArrayList<Student>(); //1   var doit etre dans la methode
    public static void main0(String[] args) {
        var student = new Student(); //2
        var allStudents = new ArrayList<>();  //3
        allStudents.add(student); //4
        for (var s : allStudents) { //5
            System.out.println(s);
        }
        //Student s2 = allStudents.get(0); //6 list non explicit a la declaration
        var var = "what?"; //7
    }

    public static void main1(String[] args) {
        var s= "    hello java guru   ".strip();
        var s2= "    hello java guru   ".trim();
        System.out.println(s);
        System.out.println(s2.length());
        byte starting = 3;
        short firstValue = 5;
        int secondValue = 7;
        int functionValue = (int) (starting/2 + firstValue/2 + (int) firstValue/3 )  + secondValue/2;
        System.out.println(functionValue);
    }

    public static void main2(String args[ ] ){
        String s = "blooper";
        System.out.println(s.substring(4));
        StringBuilder sb = new StringBuilder(s);
        sb.append(s.substring(4));
        System.out.println(sb);
        sb.delete(3, 5);
        System.out.println(sb);

        int i = 0 ;
        boolean bool1 = true ;
        boolean bool2 = false;
        boolean bool  = false;
        bool = ( bool2 &  method1(i++) ); //1
        /**
         * & and | do not short circuit the expression but && and || do.
         * As the value of all the expressions ( 1 through 4) can be determined just by looking at the first part, && and || do not evaluate the rest of the expression, so method1() is not called for 2 and 4.
         * Hence, the value of i is incremented only twice.
         */
        bool = ( bool2 && method1(i++) ); //2
        bool = ( bool1 |  method1(i++) ); //3
        bool = ( bool1 || method1(i++) ); //4
        System.out.println(i);
    }
    public static boolean method1(int i){
        return i>0 ? true : false;
    }

    /*public static void main(String[] args){
        var values = new ArrayList<String>();
        for(var value : values){}
        values.forEach(var k->System.out.print(k.length()));
        values.forEach( k -> System.out.print(k.length()));
        var k = values.get(0); values.add(k);

        values.add(1);
        int i = 1234567890;
        float f = i;
        System.out.println(i - (int)f);
    }*/

    public static int operators(){
        int x1 = -4;
        int x2 = x1--;
        System.out.println("x2 = " +x2);
        int x3 = ++x2;
        System.out.println("x3 = " +x3);
        x1++;
        System.out.println("x1 = " +x1);
        return x1 + x2 + x3;
    }
    public static void main(String[] args) {
        System.out.println(operators());
    }

}
class Student{ }