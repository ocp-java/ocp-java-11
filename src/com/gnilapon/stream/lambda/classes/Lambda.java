package com.gnilapon.stream.lambda.classes;

import java.util.Optional;

public class Lambda {
    /**
     * Optional<Double> price = Optional.ofNullable(getPrice(« 1111 »)) ; Double y = price.orElseGet(()->getPrice(« 333 »)) ;
     * La méthode orElseGet d'Optional prend en argument une fonction java util.function.Supplier et invoque cette fonction pour obtenir une valeur si l'Optional lui-même est vide. Tout comme la méthode orElse, cette méthode ne lève aucune exception même si la fonction Supplier renvoie null. Elle lève cependant une exception NullPointerException si l'option est vide et que la fonction Supplier elle-même est nulle.
     */
    Optional<Double> price = Optional.ofNullable(getPrice("1111"));
    Double x = price.orElse(getPrice("2222"));
    private Double getPrice(String number) {
        return  Double.valueOf(number);
    }
}
