package com.gnilapon.stream.lambda;

import com.gnilapon.stream.lambda.classes.MyProcessor;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestClass {
    /**
     * Un point important à comprendre en ce qui concerne les références à des méthodes ou à des constructeurs est que vous ne pouvez jamais passer d'arguments lorsque vous faites référence à un constructeur ou à une méthode.
     * N'oubliez pas que ces références sont de simples références.
     * Il ne s'agit pas d'invocations réelles.
     * Par exemple, lorsque vous faites Supplier<MyProcessor> s = MyProcessor::new ; vous dites au compilateur de vous donner la référence du constructeur qui ne prend aucun argument.
     * En effet, la méthode fonctionnelle du fournisseur ne prend pas d'argument.
     * Bien sûr, cela ne compilera pas car MyProcessor n'a pas de constructeur de ce type.
     * D'autre part, lorsque vous faites Function<Integer, MyProcessor> f = MyProcessor::new ; vous dites au compilateur de vous donner la référence du constructeur qui prend un argument Integer.
     * Le compilateur s'en rend compte parce que la méthode fonctionnelle de l'interface Function nécessite un argument et que vous l'avez paramétrée en Integer.
     * Le compilateur recherche donc un constructeur qui prend un argument Integer (ou int) et vous donne la référence de ce constructeur.
     * Le constructeur ou la méthode n'est pas invoqué(e) à ce moment-là et aucun argument n'est donc nécessaire à ce moment-là.
     * Les arguments ne sont nécessaires que lorsque vous invoquez le constructeur ou une méthode.
     * Par conséquent, un code tel que MyProcessor::new(10) ; n'a pas de sens.
     * Vous ne pouvez pas passer d'arguments lorsque vous prenez une référence.
     * Vous passez des arguments lorsque vous utilisez la référence pour l'invoquer, comme dans l'option 4 : MonProcesseur mp = f.apply(10) ; Cela fonctionne parce que f est déjà défini pour utiliser une référence de constructeur qui prend un paramètre.
     * 10 est passé à ce constructeur.
     * @param args
     */
    public static void main0(String[] args) {
        List<Integer> ls = Arrays.asList(1, 2, 3);

        /**
         * Ici, la méthode map a un objet Integer implicite dans le contexte, qui est en fait l'élément courant de la liste.
         * Cet élément sera transmis en tant qu'argument au constructeur de MyProcessor.
         * De même, forEach a un objet MyProcessor dans le contexte lorsqu'il invoque la méthode process.
         * Comme process est une méthode d'instance de MyProcessor, l'instance de MyProcessor disponible dans le contexte sera utilisée pour invoquer la méthode process.
         */
        ls.stream().map(MyProcessor::new).forEach(MyProcessor::process);
    }


    /**
     * 1. La méthode forEachOrdered traite les éléments du flux dans l'ordre où ils sont présents dans la source sous-jacente. Dans le cas présent, la source sous-jacente du premier flux est la liste de tableaux « source ».
     * Dans cette liste de tableaux, les éléments sont déjà dans l'ordre requis et c'est dans cet ordre qu'ils seront imprimés, même si le flux est un flux parallèle en raison de la méthode forEachOrdered.
     *
     * 2. Les flux parallèles permettent d'exécuter des opérations telles que peek et map sur les éléments du flux à partir de plusieurs threads.
     * Cela signifie qu'elles peuvent être exécutées dans n'importe quel ordre. Par conséquent, dans ce cas, le code qui ajoute les éléments à la destination (c'est-à-dire l'appel à peek à //2) peut ajouter des éléments à la liste de destination dans n'importe quel ordre.
     * Cela signifie qu'en réalité, l'ordre des éléments dans la liste de destination est inconnu. C'est ce problème qui doit être résolu ici.
     * Changer parallelStream en stream sur la source rectifiera ce problème car les éléments seront alors ajoutés à la destination dans le même ordre.
     *
     * Observez qu'il n'y a aucun avantage à utiliser forEachOrdered sur un flux parallèle si c'est tout ce que vous voulez faire avec le flux.
     * Toutefois, si vous souhaitez appliquer des opérations intermédiaires coûteuses telles que filter, peek ou map, tout en continuant à traiter les éléments dans l'ordre à la fin, l'utilisation de forEachOrdered peut être utile car les opérations intermédiaires peuvent toujours être exécutées en parallèle.
     * @param args
     */
    public static void main1(String[] args) {
        ArrayList<Integer> source = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        List<Integer> destination = Collections.synchronizedList(new ArrayList<>());
        source.stream()  //1
              .peek(destination::add) //2
              .forEachOrdered(System.out::print);
        System.out.println("");
        destination.stream() //3
                    .forEach(System.out::print); //4
                    System.out.println("");
    }

    /**
     * Il s'agit d'une question très simple. La méthode filter filtrera le flux en fonction des critères donnés et la méthode count comptera le nombre d'éléments dans la liste résultante. Il n'est pas nécessaire de trier la liste car vous souhaitez simplement compter le nombre d'éléments qui répondent aux critères donnés.
     * @param args
     */
    public static void main2(String[] args) {
        List<String> al = Arrays.asList("aa", "aaa", "b", "cc", "ccc", "ddd", "a");
        //int count = al.stream().filter((str)->str.compareTo("c")<0).count(); La méthode count renvoie un long. Il n'est pas possible d'affecter un long à un int sans un cast explicite.
        //int count = al.stream().filter((str)->str.compareTo("c")<0).sort().count(); sort() methode inexistante
        long count = al.stream().filter((str)->str.compareTo("c")<0).count();
        //INSERT CODE HERE
        System.out.println(count);
    }


    /**
     * Observez qu'à la ligne //2, e.age est fixé à 50.
     * Cependant, à la ligne //3, l'expression lambda définie à la ligne //1 est exécutée. Elle change e.age en 40.
     * Ainsi, même si, visuellement, e.age=50 apparaît dans le code après e.age=40, il est en fait exécuté avant e.age=40.
     * @param args
     */
    public static void main3(String[] args) {
        Employee e = new Employee();
        Supplier<Employee> se = ()->{ e.age = 40; return e; }; //1
        e.age = 50;//2
        System.out.println(se.get().age); //3
    }

    /**
     * La méthode Comparator.comparing nécessite une fonction qui prend une entrée et renvoie un comparable.
     * Ce comparable est à son tour utilisé par la méthode comparing pour créer un comparateur.
     * La méthode max utilise le comparateur pour comparer les éléments du flux.
     * L'expression lambda a->a crée une fonction qui prend un entier et renvoie un entier (qui est un comparable).
     * Ici, l'expression lambda ne fait pas grand-chose, mais dans les situations où vous avez une classe qui n'implémente pas Comparable et que vous voulez comparer des objets de cette classe en utilisant une propriété de cette classe qui est Comparable, c'est très utile.
     * L'appel à get() est nécessaire car max(Comparator ) renvoie un objet Optional.
     * @param args
     */
    public static void main4(String[] args) {
        List<Integer> ls = Arrays.asList(10, 47, 33, 23);
        int max =ls.stream().max(Comparator.comparing(a->a)).get();
        //INSERT code HERE
        System.out.println(max); //1
    }

    /**
     * La description de l'API JavaDoc explique exactement le fonctionnement de la méthode merge.
     * Vous devriez la parcourir car elle est importante pour l'examen. public V merge(K key, V value, BiFunction< ? super V, ? super V, ? extends V> remappingFunction) Si la clé spécifiée n'est pas déjà associée à une valeur ou si elle est associée à null, l'associe à la valeur non nulle donnée.
     * Sinon, la valeur associée est remplacée par le résultat de la fonction de conversion donnée, ou supprimée si le résultat est nul.
     * Cette méthode peut être utile pour combiner plusieurs valeurs mappées pour une clé. Par exemple, pour créer ou ajouter une chaîne msg à un mappage de valeurs : map.merge(key, msg, String::concat) Si la fonction renvoie un résultat nul, le mappage est supprimé.
     * Si la fonction elle-même lève une exception (non vérifiée), l'exception est relancée et la correspondance actuelle est laissée inchangée.
     * Paramètres : key - clé à laquelle la valeur résultante doit être associée value - valeur non nulle à fusionner avec la valeur existante associée à la clé ou, si aucune valeur existante ou une valeur nulle est associée à la clé, à associer à la clé remappingFunction - fonction permettant de recalculer une valeur si elle existe Returns : la nouvelle valeur associée à la clé spécifiée, ou null si aucune valeur n'est associée à la clé Throws : UnsupportedOperationException - si l'opération put n'est pas prise en charge par cette map (facultatif) ClassCastException - si la classe de la clé ou de la valeur spécifiée empêche son stockage dans cette map (facultatif) NullPointerException - si la clé spécifiée est nulle et que cette map ne prend pas en charge les clés nulles ou que la valeur ou remappingFunction est nulle.
     * @param args
     */
    public static void main5(String[] args) {
        Map<String, Integer> map1 = new HashMap<>();
        map1.put("a", 1);
        map1.put("b", 1);
        // Si la clé 'a' existe, additionner les valeurs existantes (1) et nouvelles (1)
        map1.merge("b", 1, (i1, i2)->i1+i2);
        // Ajouter la clé 'c' avec la valeur 3, car 'c' n'existe pas encore
        map1.merge("c", 3, (i1, i2)->i1+i2);
        System.out.println(map1);
    }


    /**
     * La description suivante de la méthode joining dans la JavaDoc est utile : public static Collector<CharSequence, ?,String> joining(CharSequence delimiter, CharSequence prefix, CharSequence suffix) Renvoie un collecteur qui concatène les éléments d'entrée, séparés par le délimiteur spécifié, avec le préfixe et le suffixe spécifiés, dans l'ordre de la rencontre. Paramètres : delimiter - le délimiteur à utiliser entre chaque élément prefix - la séquence de caractères à utiliser au début du résultat joint suffix - la séquence de caractères à utiliser à la fin du résultat joint Returns : Un collecteur qui concatène les éléments CharSequence, séparés par le délimiteur spécifié, dans l'ordre de rencontre.
     */
    /**
     * Collectors.joining(« , », « - », « + ») renvoie un collecteur qui joint toutes les chaînes du flux donné séparées par une virgule, puis préfixe la chaîne résultante par « - » et la suffixe par « + ».
     * @param args
     */
    public static void main(String[] args) {
        Stream<String> ss = Stream.of("a", "b", "c");
        String str = ss.collect(Collectors.joining("******", "-", "+"));
        System.out.println(str);
    }
}

