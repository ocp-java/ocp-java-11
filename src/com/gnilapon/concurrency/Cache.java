package com.gnilapon.concurrency;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Il est important de savoir que les itérateurs extraits d'une ConcurrentHashMap sont soutenus par cette ConcurrentHashMap, ce qui signifie que toute opération effectuée sur l'instance de la ConcurrentHashMap peut être répercutée sur l'itérateur.
 *
 * Ainsi, dans ce cas, le thread qui parcourt les entrées peut ou non voir les entrées supprimées de la Map par un autre thread. Voici ce que dit la description de l'API JavaDoc à propos de ConcurrentHashMap :
 *
 * Les opérations de récupération (y compris get) ne sont généralement pas bloquées et peuvent donc se chevaucher avec les opérations de mise à jour (y compris put et remove).
 * Les extractions reflètent les résultats des opérations de mise à jour les plus récentes, en les conservant dès leur début.
 * Pour les opérations d'agrégation telles que putAll et clear, les extractions simultanées peuvent refléter l'insertion ou la suppression de certaines entrées seulement.
 * De même, les itérateurs et les énumérations renvoient des éléments reflétant l'état de la table de hachage à un moment donné ou depuis la création de l'itérateur ou de l'énumération.
 * Ils ne déclenchent pas d'exception de modification simultanée (ConcurrentModificationException). Cependant, les itérateurs sont conçus pour être utilisés par un seul thread à la fois.
 *
 * et voici la description du comportement de l'ensemble d'entrées extrait d'une instance de ConcurrentHashMap à l'aide de la méthode entrySet() :
 * entrySet() renvoie une vue de l'ensemble des correspondances contenues dans cette Map. L'ensemble est soutenu par la Map, de sorte que les changements apportés à la Map sont reflétés dans l'ensemble, et vice-versa.
 * L'ensemble supporte la suppression d'éléments, qui supprime le mappage correspondant de la Map, via les opérations Iterator.remove, Set.remove, removeAll, retainAll et clear. Il ne prend pas en charge les opérations add et addAll.
 *
 * L'itérateur de la vue est un itérateur "faiblement cohérent" qui ne lèvera jamais d'exception de type ConcurrentModificationException, et qui garantit de parcourir les éléments tels qu'ils existaient lors de la construction de l'itérateur, et qui peut (mais n'est pas garanti) refléter toute modification postérieure à la construction.
 */
public class Cache {
    static ConcurrentHashMap<String, Object> chm = new ConcurrentHashMap<String, Object>();

    /**
     * C'est correct car l'ordre d'itération n'est pas connu et donc le thread qui supprime "a" et "b" peut les supprimer dans n'importe quel ordre.
     * Ainsi, l'itérateur peut ou non voir "a" et/ou "b" à travers son itérateur.
     * Cependant, "c" n'est jamais supprimé de la Map et c sera donc toujours imprimé.
     * @param args
     */
    public static void main(String[] args) {
        chm.put("a", "aaa");
        chm.put("b", "bbb");
        chm.put("c", "ccc");
        new Thread() {
            public void run() {
                Iterator<Map.Entry<String, Object>> it = Cache.chm.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, Object> en = it.next();
                    if (en.getKey().equals("a") || en.getKey().equals("b")) {
                        it.remove();
                    }
                }
            }
        }.start();
        new Thread() {
            public void run() {
                Iterator<Map.Entry<String, Object>> it = Cache.chm.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, Object> en = it.next();
                    System.out.print(en.getKey() + ", ");
                }
            }
        }.start();
    }
}
