package com.gnilapon.concurrency;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Étant donné qu'un thread appelle la méthode addData sur une instance de la classe ci-dessus avec une liste contenant 10 chaînes de caractères et qu'un autre thread appelle la méthode getCacheSize sur la même instance en même temps, laquelle des options suivantes est correcte ?
 */

/**
 * Toutes les opérations de modification d'une CopyOnWriteArrayList sont considérées comme atomiques. Ainsi, le thread qui appelle size() ne verra aucune donnée dans la liste ou verra tous les éléments ajoutés à la liste.
 */


/**
 * public class CopyOnWriteArrayList<E> implémente List<E>, RandomAccess, Cloneable, Serializable
 * Une variante thread-safe de ArrayList dans laquelle toutes les opérations mutatives (add, set, etc.) sont implémentées en faisant une nouvelle copie du tableau sous-jacent.
 * Cela est généralement trop coûteux, mais peut être plus efficace que d'autres solutions lorsque les opérations de traversée sont beaucoup plus nombreuses que les mutations, et est utile lorsque vous ne pouvez pas ou ne voulez pas synchroniser les traversées, mais que vous avez besoin d'éviter les interférences entre les threads simultanés.
 * La méthode d'itérateur de type "snapshot" utilise une référence à l'état du tableau au moment où l'itérateur a été créé.
 * Ce tableau ne change jamais pendant la durée de vie de l'itérateur, de sorte que les interférences sont impossibles et que l'itérateur est assuré de ne pas déclencher d'exception de modification concurrente (ConcurrentModificationException).
 * L'itérateur ne reflète pas les ajouts, les suppressions ou les modifications apportées à la liste depuis sa création.
 * Les opérations de modification d'éléments sur les itérateurs eux-mêmes (remove, set et add) ne sont pas prises en charge.
 * Ces méthodes provoquent des exceptions de type UnsupportedOperationException.
 */
public class MyCache0 {
    private CopyOnWriteArrayList<String> cal = new CopyOnWriteArrayList<>();

    public void addData(List<String> list) {
        cal.addAll(list);
    }

    public int getCacheSize() {
        return cal.size();
    }
}

