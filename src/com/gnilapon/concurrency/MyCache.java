package com.gnilapon.concurrency;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;


/**
 * CopyOnWriteArrayList garantit que l'itérateur acquis à partir de son instance n'obtiendra jamais cette exception. Cela est rendu possible par la création d'une copie du tableau sous-jacent des données.
 * L'itérateur est soutenu par ce tableau dupliqué.
 * Il en résulte que toute modification apportée à la liste n'est pas reflétée dans l'itérateur et qu'aucune modification ne peut être apportée à la liste à l'aide de cet itérateur (par exemple en appelant iterator.remove() ).
 * Les appels qui tentent de modifier l'itérateur obtiendront une exception de type UnsupportedOperationException.
 */
public class MyCache {
    private CopyOnWriteArrayList<String> cal = new CopyOnWriteArrayList<>();
    public void addData(List<String> list){
        cal.addAll(list);
    }
    public Iterator getIterator(){
        return cal.iterator();
    }
}

