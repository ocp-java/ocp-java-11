package com.gnilapon.concurrency;

/**
 * The above code may result in a deadlock and so nothing can be said for sure about the output.
 */

/**
 * Il s'agit d'une question complexe.
 * Considérons la situation suivante :
 * Le premier thread acquiert le verrou de sb1.
 * Il ajoute X à sb1.
 * Juste après cela, le système d'exploitation arrête ce thread et démarre le second thread.
 * (Notez que le premier thread possède toujours le verrou de sb1).
 * Le deuxième thread acquiert le verrou de sb2 et ajoute Y à sb2.
 * Il essaie maintenant d'acquérir le verrou de sb1.
 * Mais le verrou de sb1 est déjà acquis par le premier thread, de sorte que le second thread doit attendre.
 * Le système d'exploitation démarre alors le premier thread.
 * Il essaie d'acquérir le verrou de sb2 mais ne peut pas l'obtenir car le verrou est déjà acquis par le deuxième thread.
 */
public class TestClass {
    static StringBuffer sb1 = new StringBuffer();
    static StringBuffer sb2 = new StringBuffer();

    public static void main(String[] args) {
        new Thread(new Runnable() {
            public void run() {
                synchronized (sb1) {
                    sb1.append("X");
                    synchronized (sb2) {
                        sb2.append("Y");
                    }
                }
                System.out.println(sb1);
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                synchronized (sb2) {
                    sb2.append("Y");
                    synchronized (sb1) {
                        sb1.append("X");
                    }
                }
                System.out.println(sb2);
            }
        }).start();
    }
}