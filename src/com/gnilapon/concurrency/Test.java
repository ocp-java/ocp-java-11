package com.gnilapon.concurrency;

/**
 * It may result in a deadlock and the program may get stuck.
 */

/**
 * Nothing can be said for sure.
 */


/**
 * Considérons la situation suivante :
 * le thread 1 entre dans run(), il obtient le verrou pour obj1 et avant qu'il ne puisse obtenir le verrou pour obj2, le thread 2 s'exécute.
 * Le thread 2 obtient le verrou pour obj2 et essaie d'obtenir le verrou pour obj1.
 * Maintenant, les deux threads attendent les verrous acquis par l'autre.
 * Personne ne peut donc s'exécuter.
 * Le programme est donc bloqué.
 * Notez que les threads ne sont pas morts.
 * Ils continuent simplement d'attendre.
 * Le programme ne se termine donc jamais dans ce cas.
 */
public class Test extends Thread {
    boolean flag = false;

    public Test(boolean f) {
        flag = f;
    }

    static Object obj1 = new Object();
    static Object obj2 = new Object();

    public void m1() {
        synchronized (obj1) {
            System.out.print("1 ");
            synchronized (obj2) {
                System.out.println("2");
            }
        }
    }

    public void m2() {
        synchronized (obj2) {
            System.out.print("2 ");
            synchronized (obj1) {
                System.out.println("1");
            }
        }
    }

    public void run() {
        if (flag) {
            m1();
            m2();
        } else {
            m2();
            m1();
        }
    }

    public static void main(String[] args) {
        new Test(true).start();
        new Test(false).start();
    }
}

