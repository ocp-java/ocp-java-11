package com.gnilapon.concurrency;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

/**
 * What can be inserted in the above code so that both - Runnable r and Callable c - will be executed?
 */


/**
 * Tout ce que vous devez faire pour exécuter les Runnables et les Callables est de les soumettre au pool de threads mis en cache.
 * Un pool de threads mis en cache crée de nouveaux threads en fonction des besoins, mais réutilise les threads précédemment construits lorsqu'ils sont disponibles.
 * Ce pool améliore généralement les performances des programmes qui exécutent de nombreuses tâches asynchrones de courte durée.
 * Les appels à l'exécution réutiliseront les threads précédemment construits s'ils sont disponibles.
 * Si aucun thread existant n'est disponible, un nouveau thread sera créé et ajouté au pool.
 * Les threads qui n'ont pas été utilisés pendant soixante secondes sont interrompus et retirés du cache.
 * Ainsi, un pool qui reste inactif suffisamment longtemps ne consommera pas de ressources.
 * Notez que les pools ayant des propriétés similaires mais des détails différents (par exemple, les paramètres de temporisation) peuvent être créés à l'aide des constructeurs de ThreadPoolExecutor.
 */
public class ThreadPoolTest {
    public static void main(String[] args) {
        Runnable r = () -> System.out.println("In Runnable");

        Callable<Integer> c = () -> {
            System.out.println("In Callable");
            return 0;
        };
        var es = Executors.newCachedThreadPool();
        es.submit(c);
        es.submit(r);
        es.shutdown();

    }
}

