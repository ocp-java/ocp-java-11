package com.gnilapon.concurrency;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Ce code illustre comment un blocage peut se produire lorsque plusieurs threads tentent d'acquérir des verrous sur plusieurs objets dans des séquences différentes.
 * Considérons la situation suivante : - le thread 1 tente d'exécuter le premier transfert et acquiert le verrou du compte a1.
 * Le thread 1 tente d'exécuter le premier transfert et acquiert le verrou pour le compte a1. Il met à jour le solde de a1 mais avant qu'il ne puisse acquérir le verrou pour a2, le thread2 s'exécute et acquiert le verrou pour le compte a2.
 * Le thread 2 met à jour le solde et tente d'acquérir le verrou de a1. Il est maintenant bloqué parce que le verrou de a1 est déjà acquis par le thread1.
 * Il ne pourra pas continuer tant que le thread 1 n'aura pas libéré le verrou de a1. En même temps, il gardera son propre verrou pour a2 parce qu'il ne l'a pas libéré.
 * Maintenant, le thread 1 s'exécute et tente d'acquérir le verrou de a2, qui se trouve dans le thread 2. Il ne peut donc pas aller plus loin.
 *
 * Comme vous pouvez le voir, les deux threads sont bloqués.
 * Personne n'exécute de code.
 * Cette situation, dans laquelle aucun des threads n'est en mesure d'exécuter un code faute de verrou acquis par l'autre, s'appelle un blocage(deadlock).
 */
public class Transfer implements Runnable {
    Account from, to;
    double amount;

    public Transfer(Account from, Account to, double amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public void run() {
        synchronized (from) {
            from.setBalance(from.getBalance() - amount);
            synchronized (to) {
                to.setBalance(to.getBalance() + amount);
            }
        }
    }
}


