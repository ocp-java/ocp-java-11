package com.gnilapon.concurrency;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Explications
 * Classe Counter :
 *
 * La classe Counter contient une variable count partagée entre plusieurs threads.
 * Un objet Lock est utilisé pour synchroniser l'accès à la variable count.
 * Méthode increment :
 *
 * Utilise lock.lock() pour acquérir le verrou avant d'incrémenter le compteur.
 * Le bloc try-finally assure que lock.unlock() est toujours appelé pour libérer le verrou, même si une exception se produit.
 * Méthode getCount :
 *
 * Retourne simplement la valeur de count.
 * Méthode main :
 *
 * Crée un objet Counter et deux threads qui exécutent une tâche commune consistant à incrémenter le compteur 1000 fois.
 * Les threads sont démarrés et attendus (join()) pour s'assurer qu'ils terminent avant d'afficher le compteur final.
 * Points Clés
 * Verrous (Locks) :
 *
 * ReentrantLock est une implémentation de Lock qui permet un verrouillage explicite.
 * Contrairement à la synchronisation intégrée (synchronized), les verrous offrent une plus grande flexibilité, comme les tentatives de verrouillage avec délai (tryLock).
 * Synchronisation :
 *
 * Assure que le bloc de code critique (incrémentation du compteur) est accessible par un seul thread à la fois, empêchant les conditions de course.
 * Ce code est un bon point de départ pour comprendre comment les verrous peuvent être utilisés pour gérer la synchronisation entre threads en Java.
 */

public class Counter {
    private int count = 0;
    private final Lock lock = new ReentrantLock();

    public void increment() {
        lock.lock();
        try {
            count++;
            System.out.println(Thread.currentThread().getName() + " incremented count to: " + count);
        } finally {
            lock.unlock();
        }
    }

    public int getCount() {
        return count;
    }

    public static void main(String[] args) {
        Counter counter = new Counter();
        Runnable task = () -> {
            for (int i = 0; i < 1000; i++) {
                counter.increment();
            }
        };

        Thread thread1 = new Thread(task, "Thread 1");
        Thread thread2 = new Thread(task, "Thread 2");

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            System.out.printf(e.getMessage());
        }

        System.out.println("Final count: " + counter.getCount());
    }
}


