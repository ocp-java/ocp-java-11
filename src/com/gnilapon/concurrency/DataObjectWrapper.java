package com.gnilapon.concurrency;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DataObjectWrapper {
    private final Object obj;

    public DataObjectWrapper(Object pObj) {
        obj = pObj;
    }

    public Object getObject() {
        return obj;
    }
}

