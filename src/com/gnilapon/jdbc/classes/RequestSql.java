package com.gnilapon.jdbc.classes;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;


/**
 * 1. L'expression « select * » signifie que vous sélectionnez toutes les colonnes.
 * 2. Vous pouvez extraire les valeurs d'un ResultSet en utilisant les indices des colonnes, qui commencent par 1, ou en utilisant les noms des colonnes des tables de la base de données.
 * 3. Les noms de colonnes utilisés comme entrée dans les méthodes getter ne sont pas sensibles à la casse.
 * Lorsqu'une méthode getter est appelée avec un nom de colonne et que plusieurs colonnes ont le même nom, la valeur de la première colonne correspondante sera renvoyée.
 * Comme la requête renvoie une ligne, on peut supposer que EMAILID est un nom de colonne valide dans cette table.
 * Par conséquent, il n'y a pas de problème avec le code et il devrait afficher la valeur 'bob@gmail.com'.
 * javax.sql.DataSource Bien que cela ne soit pas mentionné dans les objectifs de l'examen, vous pouvez voir l'utilisation de la méthode DataSource.getConnection dans les questions de l'examen.
 * Ne vous inquiétez pas car cela n'a aucun impact sur la réponse.
 * javax.sql.DataSource est en fait la méthode préférée pour obtenir une connexion dans les applications d'entreprise au lieu de java.sql.DriverManager.
 * L'avantage le plus important est que le code de l'application n'a pas besoin d'avoir accès aux détails de la connexion à la base de données (c'est-à-dire userid, pwd et dburl).
 * Une source de données est configurée dans le serveur d'application et l'application la recherche simplement à l'aide de JNDI.
 * Pour obtenir une connexion à partir de la source de données, il suffit de procéder comme suit :
 * Contexte ctx = nouveau InitialContext() ;
 * DataSource ds = (DataSource) ctx.lookup(« java:/comp/env/jdbc/MyLocalDB ») ;
 * Connexion c = dataSource.getConnection() ; //pas d'arguments
 * DataSource améliore également les performances de l'application car les connexions ne sont pas créées/fermées au sein d'une classe.
 * Elles sont gérées par le serveur d'application et peuvent être récupérées au moment de l'exécution.
 * Elle permet de créer un pool de connexions et est donc très utile pour les applications d'entreprise.
 */
public class RequestSql {
    public void requete() throws SQLException, NamingException {
        Context ctx = new InitialContext();
        DataSource dataSource = (DataSource) ctx.lookup("java:/comp/env/jdbc/MyLocalDB");
        Connection connection = dataSource.getConnection();
        PreparedStatement stmt = connection.prepareStatement("select * from CUSTOMER where EMAILID=?");
        stmt.setObject(1, "bob@gmail.com"); //LINE 10
        ResultSet rs = stmt.executeQuery();
        while(rs.next()){
            System.out.println(rs.getString("EMAILID")); //LINE 12
        }
        connection.close();
    }

    public void prepareStatement() throws NamingException, SQLException {
        Context ctx = new InitialContext();
        DataSource dataSource = (DataSource) ctx.lookup("java:/comp/env/jdbc/MyLocalDB");
        Connection connection = dataSource.getConnection();
        String qr = "insert into USERINFO values( ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(qr)) {

            ps.setObject(1, 1, JDBCType.INTEGER);
            ps.setObject(2, "Ally A", JDBCType.VARCHAR);
            ps.setObject(3, "101 main str", JDBCType.VARCHAR);
            int i = ps.executeUpdate(); //1
            ps.setObject(1, 2, JDBCType.INTEGER);
            ps.setObject(2, "Bob B", JDBCType.VARCHAR);
            i = ps.executeUpdate(); //2
        }
    }
}
