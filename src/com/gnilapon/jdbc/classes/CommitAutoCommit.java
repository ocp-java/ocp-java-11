package com.gnilapon.jdbc.classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CommitAutoCommit {

    /**
     * Lorsqu'une connexion est créée, elle est en mode auto-commit, c'est-à-dire que l'auto-commit est activé. Cela signifie que chaque instruction SQL est traitée comme une transaction et est automatiquement validée dès qu'elle est terminée. Par conséquent, dans ce cas, la mise à jour sera validée automatiquement après la fin de l'appel à executeUpdate.
     * @throws SQLException
     */
    public void autocommit() throws SQLException {
        Connection con = DriverManager.getConnection("dbURL");
        String updateString ="update SALES " + "set T_AMOUNT = 100 where T_NAME = 'BOB'";
        Statement stmt = con.createStatement();
        stmt.executeUpdate(updateString);
    }

    /**
     * Puisque l'auto-commit a été désactivé dans le code donné (en appelant con.setAutoCommit(false)), vous devez appeler commit() sur l'objet Connection explicitement pour valider les changements dans la base de données.
     * @throws SQLException
     */
    public void commite() throws SQLException {
        Connection con = DriverManager.getConnection("dbURL");
        con.setAutoCommit(false);
        Statement stmt = con.createStatement();
        String updateString = "update SALES " + "set T_AMOUNT = 100 where T_NAME = 'BOB'";
        stmt.executeUpdate(updateString);
        con.commit();
    }
}
