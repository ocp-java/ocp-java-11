package com.gnilapon.jdbc.classes;

import java.sql.*;

public class EnquoteIdentifierExample {
    public static void main(String[] args) {
        // URL de la base de données
        String url = "jdbc:your_database_url";
        // Nom d'utilisateur et mot de passe de la base de données
        String user = "your_database_user";
        String password = "your_database_password";

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            // Établir la connexion
            connection = DriverManager.getConnection(url, user, password);

            // Créer un Statement
            statement = connection.createStatement();

            // Utiliser enquoteIdentifier pour protéger les noms d'identificateurs
            String tableName = statement.enquoteIdentifier("your_table_name", true);
            String columnName = statement.enquoteIdentifier("your_column_name", true);

            // Construire la requête SQL en utilisant les identificateurs enquoted
            String query = "SELECT " + columnName + " FROM " + tableName;

            // Exécuter la requête
            resultSet = statement.executeQuery(query);

            // Traiter les résultats
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            // Fermer les ressources
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
