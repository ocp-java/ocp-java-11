package com.gnilapon.jdbc.classes;

import java.sql.*;

/**
 * Cette question ne porte pas vraiment sur la requête SET SESSION mais sur les méthodes enquoteIdentifier et enquoteLiteral qui ont été ajoutées en Java 9 à l'interface java.sql.Statement pour aider à prévenir les attaques par injection SQL.
 * Bien que l'examen ne se concentre pas trop sur l'injection SQL, nous avons vu une question de l'examen qui attend de vous que vous connaissiez l'idée générale derrière ces deux méthodes.
 * Nous vous conseillons de lire un peu sur l'injection SQL et de consulter la description de ces deux méthodes dans l'API JavaDoc.
 * Différence entre enquoteLiteral et enquoteIdentifier :  En bref, les identificateurs sont des objets de base de données créés par l'utilisateur.
 * Par exemple, si vous créez une table nommée Employés, Employés est un identifiant.
 * Par défaut, les identificateurs ne sont pas sensibles à la casse.
 * Par conséquent, les deux commandes select * from Employees et select * from EMPLOYEES sont équivalentes et fonctionneront correctement si la table s'appelle Employés.
 * Toutefois, dans certaines situations, on peut vouloir utiliser le nom exact sans tenir compte de la casse.
 * Pour ce faire, on peut mettre l'identifiant entre guillemets doubles.
 * Si un identifiant est mis entre guillemets, la base de données effectue une correspondance exacte.
 * Par exemple, select * from « Employees » et select * from « EMPLOYEES » échoueront tous deux si la table s'appelle employees.
 * La déclaration dispose d'une méthode par défaut String enquoteIdentifier(String identifier, boolean alwaysQuote) à cet effet.
 * Nous vous conseillons de consulter la description de l'API JavaDoc pour plus de détails.
 * Les littéraux sont les valeurs transmises dans une requête.
 * Par exemple, dans la requête select * from employees where dept='IT', IT est un littéral.
 * Les littéraux sont toujours entre guillemets simples et sont généralement sensibles à la casse.
 * Ainsi, « IT » ne correspondra pas à « it » ou « It ».
 * Comme les valeurs sont généralement fournies par l'utilisateur (par exemple à partir d'un formulaire dans une page web), elles sont sujettes à des attaques par injection SQL.
 * Par exemple, la ligne de code suivante produira select * from employees where dept='IT' ; delete from emloyees where ''='' ; si l'utilisateur saisit la chaîne IT' ; delete from emloyees where ''=' dans le champ DEPT d'un formulaire :
 * String qr = « select * from employees where dept=“ »+dept+« ” » ; Par conséquent, il est toujours bon d'interroger les valeurs littérales à l'aide de la méthode enquoteLiteral de Statement.
 * Cette méthode garantit que si la valeur contient un guillemet simple, celui-ci est échappé correctement.
 * Par exemple, enquoteLiteral(« IT“ ; delete from emloyees where ”“=” ») ; produira IT'' ; delete from emloyees where ''''=''.
 * Cela élimine la possibilité d'une injection sql en rendant une requête insidieuse inexécutable (à cause d'une mauvaise syntaxe).
 * String qr = « select * from employees where dept= »+stmt.enquoteLiteral(dept)+« » ;
 */
public class EnquoteLiteralExample {

    /**
     * mode should enquoted like this: String qr = "SET SESSION sql_mode = "+stmt.enquoteLiteral(mode)+";"; because enquoting values provided by the calling code prevents SQL injection attacks.
     */
    /**
     * La méthode enquoteLiteral de Statement renvoie une chaîne de caractères entre guillemets simples.
     * Toute occurrence d'un guillemet simple dans la chaîne sera remplacée par deux guillemets simples.
     * Cela permet d'éviter les attaques par injection SQL.
     * Lors de l'examen, vous pourrez également voir l'utilisation de la méthode enquoteIdentifier.
     * @param c
     * @param mode
     * @throws Exception
     */

    /**
     * Puisque l'auto-commit a été désactivé dans le code donné (en appelant con.setAutoCommit(false)), vous devez appeler commit() sur l'objet Connection explicitement pour valider les changements dans la base de données.
     * @param c
     * @param mode
     * @throws Exception
     */
    public void setSQLMode(Connection c, String mode) throws Exception{
        Statement stmt = c.createStatement();
        c.setAutoCommit(false);
        //String qr = "SET SESSION sql_mode = '"+mode+"';";
        String qr = "SET SESSION sql_mode = "+stmt.enquoteLiteral(mode)+";";
        stmt.execute(qr);
    }
}


