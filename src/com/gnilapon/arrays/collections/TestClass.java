package com.gnilapon.arrays.collections;

import com.gnilapon.arrays.collections.classes.MyGenericClass;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Le Deque est une interface importante pour l'examen.
 * Pour répondre aux questions, vous devez vous rappeler qu'une Deque peut agir aussi bien comme une Queue que comme une Pile.
 * Sur la base de ce fait, vous pouvez déduire ce qui suit :
 * 1. La Queue étant une structure FIFO (First In First Out, c'est-à-dire ajouter à la fin et retirer à l'avant), elle possède les méthodes offer(e)/add(e) (pour ajouter un élément à la fin ou à la queue) et poll()/remove() (pour retirer un élément de l'avant ou de la tête) à cette fin.
 * Notez que offer et add sont similaires, tandis que poll et remove sont similaires.
 * 2. La pile étant une structure LIFO (Last In First Out, c'est-à-dire ajouter à l'avant et retirer de l'avant), elle fournit les méthodes push(e) et pop() à cette fin, où push ajoute à l'avant et pop retire de l'avant.
 * Outre les méthodes susmentionnées, Deque propose également des variantes de ces méthodes.
 * Mais il est facile de comprendre ce qu'elles font : pollFirst()/pollLast() - poll est une méthode de la Queue.
 * Par conséquent, pollFirst et pollLast supprimeront respectivement les éléments du début et de la fin.
 * removeFirst()/removeLast() - Il s'agit de méthodes spécifiques à Deque.
 * Elles suppriment les éléments du début et de la fin respectivement.
 * Ces méthodes ne diffèrent de pollFirst/pollLast que par le fait qu'elles lèvent une exception si la file d'attente est vide.
 * offerFirst(e)/offerLast(e) - offer est une méthode de file d'attente. Par conséquent, offerFirst et offerLast ajouteront des éléments au début et à la fin respectivement.
 * addFirst(e)/addLast(e) - add est une méthode de file d'attente.
 * Par conséquent, addFirst et addLast ajouteront respectivement des éléments au début et à la fin.
 * peek(), peekFirst() : renvoie le premier élément du début de la file d'attente sans le supprimer.
 * peekLast() : renvoie le dernier élément de la fin de la file d'attente sans le supprimer.
 * element() : récupère, sans la supprimer, la tête de la file d'attente représentée par cette queue (en d'autres termes, le premier élément de cette queue).
 * Cette méthode ne diffère de peek que par le fait qu'elle lève une exception si la file d'attente est vide.
 * Remarquez qu'il n'y a pas de pushFirst(e) et de pushLast(e).
 * Vous vous demandez peut-être pourquoi il existe plusieurs méthodes pour la même chose, comme offer(e) et add(e).
 * add(e) lève une exception si l'élément ne peut être ajouté à la file d'attente en raison d'un manque de capacité, ce qui n'est pas le cas de offer(e).
 * Il existe des différences similaires dans d'autres méthodes, mais elles ne sont pas trop importantes pour l'examen.
 */
public class TestClass {
    /**
     * push() est une méthode de pile qui ajoute l'élément à l'avant.
     * Par conséquent, le contenu de d change comme suit : 1
     * offer(e) est une méthode de file d'attente qui ajoute l'élément à la fin et offerLast(e) est équivalent à offer(e).
     * La deque contient donc maintenant : 1, 2 push(3) le change en : 3, 1, 2
     * Les méthodes peek ne modifient pas la structure, donc même si peekFirst renvoie 3, la deque ne change pas.
     * removeLast() supprime l'élément de la fin, donc d contient maintenant : 3, 1
     * pop() est une méthode de pile qui retire l'élément du début.
     * Par conséquent, le contenu de d devient : 1
     *
     * @param args
     */
    public static void main0(String[] args) {
        Deque<Integer> d = new ArrayDeque<>();
        d.push(1);  // push() est une méthode de pile qui ajoute l'élément à l'avant.
        d.offerLast(2); //offer(e) est une méthode de file d'attente qui ajoute l'élément à la fin et offerLast(e) est équivalent à offer(e)
        d.push(3);
        d.peekFirst(); //Les méthodes peek ne modifient pas la structure, donc même si peekFirst renvoie 3, la deque ne change pas.
        d.removeLast(); //removeLast() supprime l'élément de la fin
        d.pop(); //pop() est une méthode de pile qui retire l'élément du début.
        System.out.println(d);
    }


    /**
     * La cause de la confusion dans le code donné est la présence de <T> dans la déclaration de la classe ainsi que dans la déclaration de la méthode.
     * Il faut savoir que les deux variables de type générique sont indépendantes l'une de l'autre. Cela signifie que le <T> dans MyGenericClass<T> n'a aucune relation avec le <T> dans public <T> String transform(T t).
     * Ainsi, si vous tapez un objet MyGenericClass en String à l'aide de MyGenericClass<String>, cela ne signifie pas que la méthode transform est également typée en String. Le T de la méthode de transformation sera typé en fonction du type de l'argument transmis à cette méthode.
     * Par conséquent, lorsque vous appelez gcStr(1.1), T est typé en Double pour les besoins de cette méthode (même si T est typé en Chaîne pour la classe). Idéalement, il convient d'utiliser des noms différents pour les différents types afin d'éviter toute confusion.
     * Par exemple : class MyGenericClass<T>{
     *                          T t ;
     *                          public <P> String transform(P p){ //<-Utilisation d'un nom différent P au lieu de T
     *                          this.t = p ; //ne compilera PAS
     *                          return p.toString()+"-"+p.hashCode() ;
     *                          }
     *                     }
     * Si vous souhaitez utiliser le même type que celui utilisé pour la classe, vous ne devez pas le redéclarer pour la méthode.
     * Par exemple : class MyGenericClass<T>{
     *                          T t ;
     *                          public String transform(T t){ //<-- a supprimé <T> de cette ligne
     *                          this.t = t ; //valide parce que T est le même pour la classe et pour la méthode
     *                          //maintenant t est le même que le type utilisé pour la classe
     *                          return t.toString()+"-"+t.hashCode() ;
     *                      }
     *                    }
     * @param args
     */
    public static void main1(String[] args) {
        MyGenericClass gc = new MyGenericClass();
        System.out.println(gc.transform(1)); //1
        System.out.println(gc.transform("hello")); //2
        MyGenericClass<String> gcStr = new MyGenericClass<String>();
        System.out.println(gcStr.transform(1.1)); //3
    }

    /**
     * Si vous utilisez add, les objets seront ajoutés à la fin et si vous utilisez addFirst, les objets seront ajoutés à l'avant.
     * Ainsi, après add, add et addFirst, la deque contient 3, 1, 2 Maintenant, poll est une méthode de file d'attente et elle supprime les éléments à l'avant.
     * Mais pollLast retire les éléments de la fin.
     * Par conséquent, elle renverra 2, puis 1, puis 3, et imprimera donc 2, 1, 3.
     * @param args
     */
    public static void main2(String[] args) {
        Deque<Integer> d = new ArrayDeque<>();
        d.add(1);
        d.add(2);
        d.addFirst(3);
        System.out.println(d.pollLast());
        System.out.println(d.pollLast());
        System.out.println(d.pollLast());
    }

    /**
     * 1. List.of et List.copyOf renvoient tous deux une liste non modifiable.
     * Par conséquent, toute tentative de tri de leurs éléments provoquera une exception java.lang.UnsupportedOperationException à l'exécution.
     * 2. List.sort prend en argument un comparateur, qui est utilisé pour trier les éléments de la liste.
     * Le comparateur est une interface fonctionnelle dotée de la méthode fonctionnelle suivante : int compare(T o1, T o2) : compare ses deux arguments pour en déterminer l'ordre.
     * 3. java.lang.Integer possède la méthode statique suivante : static int compare(int x, int y) : Compare numériquement deux valeurs int.
     * Ainsi, Integer::compare est une référence de méthode valide ici.
     * @param args
     */
    public static void main3(String[] args) {
        List<Integer> aList = List.of(40, 30, 20);  //1
        List<Integer> bList = List.copyOf(aList); //2
        bList.sort((Integer::compare)); //3
        System.out.println( bList ); //4
        aList.sort((Integer::compare)); //5
        System.out.println( aList ); //6
    }


    /**
     * TreeSet est un NavigableSet et supporte donc la méthode subSet() : NavigableSet<E> subSet(E fromElement, boolean fromInclusive, E toElement, boolean toInclusive) Renvoie une vue de la portion de cet ensemble dont les éléments vont de fromElement à toElement.
     * Le sous-ensemble renvoyé est adossé à l'ensemble d'origine.
     * Ainsi, si vous insérez ou supprimez un élément du sous-ensemble, la même chose sera répercutée sur l'ensemble d'origine.
     * De plus, étant donné que le sous-ensemble est créé à l'aide d'un intervalle (fromElement to toElement), l'élément que vous insérez doit se trouver dans cet intervalle.
     * Dans le cas contraire, une exception de type IllegalArgumentException est émise avec le message "key out of range".
     * C'est ce qui se passe dans cette question.
     * L'intervalle des sous-sections est compris entre 326 et 328 et 329 est en dehors de cet intervalle.
     * Par conséquent, une exception IllegalArgumentException est levée au moment de l'exécution.
     * @param args
     */
    public static void main4(String[] args) {
        TreeSet<Integer> s = new TreeSet<Integer>();
        TreeSet<Integer> subs = new TreeSet<Integer>();
        for(int i = 324; i<=328; i++){
            s.add(i);
        }
        subs = (TreeSet) s.subSet(326, true, 328, true );
        subs.add(329);
        System.out.println(s+" "+subs);
    }

    /**
     * La méthode remove(Object ) de ArrayList renvoie un booléen.
     * Elle renvoie un résultat vrai si l'élément est trouvé dans la liste et faux dans le cas contraire.
     * La description de l'API JavaDoc de cette méthode est importante pour l'examen - public boolean remove(Object o) Supprime la première occurrence de l'élément spécifié de cette liste, s'il est présent (opération facultative).
     * Si cette liste ne contient pas l'élément, elle reste inchangée.
     * Plus formellement, elle supprime l'élément ayant l'indice i le plus bas tel que (o==null ? get(i)==null : o.equals(get(i))) (si un tel élément existe).
     * Retourne vrai si cette liste contenait l'élément spécifié (ou, de manière équivalente, si cette liste a changé à la suite de l'appel).
     * Notez qu'il ne supprime pas toutes les occurrences de l'élément.
     * Il ne supprime que la première.
     * Dans ce cas, seul le premier "a" sera supprimé.
     * @param args
     */
    public static void main5(String[] args) {
        List s1 = new ArrayList( );
        s1.add("a");
        s1.add("b");
        s1.add("c");
        s1.add("a");
        System.out.println(s1.remove("a")+" "+s1.remove("x"));
    }

    public static void main6(String[] args) throws Exception {
        List list = new ArrayList();
        list.add("val1"); //1

        /**
         * Cette ligne tente de placer une valeur dans une liste de tableaux à l'index 2 (c'est-à-dire en troisième position).
         * Pour pouvoir placer une valeur à l'index 2, l'ArrayList doit déjà contenir au moins 2 valeurs.
         * Par conséquent, cette ligne lancera l'exception java.lang.IndexOutOfBoundsException.
         */
        list.add(2, "val2"); //2
        list.add(1, "val3"); //3
        System.out.println(list);


        /**
         * Comme un véhicule n'est pas un SUV, vous ne pouvez pas affecter une instance de véhicule directement à une variable de type SUV sans passer par un cast.
         */
        ArrayList<Vehicle> al1 = new ArrayList<>();
        // SUV s = al1.get(0);

        /**
         * Puisqu'un Drivable n'est pas une Voiture, vous ne pouvez pas affecter une instance de Drivable directement à une variable de type Voiture sans passer par un cast.
         */
        ArrayList<Drivable> al2 = new ArrayList<>();
        // Car c1 = al2.get(0);

        /**
         * Puisqu'un SUV est un Drivable, vous pouvez assigner une instance de SUV à une variable de type Drivable.
         */
        ArrayList<SUV> al3 = new ArrayList<>();
        Drivable d1 = al3.get(0);

        /**
         * Puisqu'un SUV est une voiture, vous pouvez assigner une instance de SUV à une variable de type Voiture.
         */
        ArrayList<SUV> al4 = new ArrayList<>();
        Car c2 = al4.get(0);

        /**
         * Puisqu'un véhicule n'est pas une voiture, vous ne pouvez pas assigner une instance de véhicule à une variable de type voiture sans passer par un cast.
         */
        ArrayList<Vehicle> al5 = new ArrayList<>();
        // Drivable d2 = al5.get(0);

        var cin = new Comparator<Integer>(){
            public int compare(Integer i1, Integer i2){
                return i1 - i2;
            }
        };

        Comparator<Integer> cin1 = (i1, i2)-> i1-i2;

        var cin2 = new Comparator<Integer>(){
            public int compare(Integer i1, Integer i2){
                return i1.compareTo(i2);
            }
        };
    }

    /**
     * Rappelez-vous que HashMap prend en charge l'ajout de clés et de valeurs nulles, ce qui n'est pas le cas de ConcurrentHashMap.
     * L'insertion d'une clé nulle ou d'une valeur nulle dans un ConcurrentHashMap provoquera une exception NullPointerException.
     * Certains candidats ont signalé avoir reçu une question sur cet aspect de ConcurrentHashMap.
     * @param args
     */
    public static void main7(String[] args) {
        Map hm = new ConcurrentHashMap();
        hm.put(null, "asdf");  //1
        hm.put("aaa", null);  //2

        hm = new HashMap();
        hm.put(null, "asdf");  //3
        hm.put("aaa", null);  //4
        List list = new ArrayList();
        list.add(null); //5
        list.add(null); //6

        list = new CopyOnWriteArrayList();
        list.add(null); //7
    }


    /**
     * Le tableau désigné par a est le préfixe propre du tableau désigné par b (c'est-à-dire que le premier tableau est plus petit que le second), par conséquent, compare renverra un nombre négatif.
     * De plus, comme le premier tableau a deux éléments de moins que le second, compare renverra -2.
     * Comme les deux tableaux sont différents à l'index 3, mismatch renverra 3.
     * @param args
     */
    public static void main(String[] args) {
        int[] a = { 'h', 'e', 'l'};
        int[] b = { 'h', 'e', 'l', 'l', 'o'};

        int la = a.length;
        int lb = b.length;
        System.out.println("length a : "+la+"\n length b : "+lb);
        int x = Arrays.compare(a, b);
        int y = Arrays.mismatch(a, b);
        System.out.println(x+" "+y);
    }
}

abstract class Vehicle{ }
interface Drivable{ }
class Car extends Vehicle implements Drivable{ }
class SUV extends Car {}


enum SIZE {
    TALL, JUMBO, GRANDE;
}
class CoffeeMug {
    public static void main0(String[] args)     {
        var hs = new HashSet<SIZE>();
        hs.add(SIZE.TALL); hs.add(SIZE.JUMBO); hs.add(SIZE.GRANDE);
        hs.add(SIZE.TALL); hs.add(SIZE.TALL); hs.add(SIZE.JUMBO);
        for(SIZE s : hs) System.out.print(s+" ");
    }

    static String[] sa = { "a", "aa", "aaa", "aaaa" };
    static {
        Arrays.sort(sa);
    }
    public static void main(String[] args)     {
        String search = "";
        if(args.length != 0) search = args[0];
        System.out.println(Arrays.binarySearch(sa, search));
    }
}

class Person {
    private String name;
    public Person(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String toString() {
        return name;
    }
}
class Helper {
    public void helpPeople(Queue people, Queue helped) {
        do {
            Person p = (Person) people.poll();
            System.out.println("Helped : " + p + " ");
            helped.offer(p.getName());
        } while (!people.isEmpty());
    }

    /**
     * La méthode helpPeople() est une méthode ancienne qui utilise des files d'attente non sécurisées.
     * Au moment de l'exécution, la JVM ne dispose d'aucune information sur le type des classes génériques.
     * Par conséquent, cette méthode est libre d'ajouter n'importe quel type d'objet dans les files d'attente sans aucune exception, même si la file d'attente "aidée" n'est censée contenir que des objets de type Personne.
     * N'oubliez pas que la sécurité de type des classes génériques n'est vérifiée qu'au moment de la compilation.
     * Pour permettre au code existant de fonctionner sans aucune modification, le compilateur assouplit les règles lorsque vous passez une classe de type sûr à une méthode existante.
     * Toutefois, le compilateur vous avertit qu'il s'agit d'une opération potentiellement dangereuse, car le code hérité peut ajouter à votre collection sécurisée tout élément qui n'est pas censé être ajouté.
     * La méthode poll() supprime le 0e élément. Ainsi, elle imprime Helped : Pope et ensuite Helped : John.
     * Notez que la méthode offer de Queue ajoute un élément à la fin de la file d'attente.
     * @param args
     */
    public static void main(String[] args) {
        Queue<Person> q = new LinkedList<>();
        q.offer(new Person("Pope"));
        q.offer(new Person("John"));
        q.offer(new Person("Jonathan"));
        q.offer(new Person("nullity"));
        System.out.println(q.size());
        System.out.println(q);
        Queue<Person> helpedQ = new LinkedList<>();
        Helper h = new Helper();
        h.helpPeople(q, helpedQ);
    }
}

