package com.gnilapon.arrays.collections.classes;


public class TesClass{

    /**
     * La ligne //1 sera autorisée lors de la compilation, car l'affectation est effectuée d'une référence de sous-classe à une référence de superclasse.
     * Le cast de la ligne //2 est nécessaire parce qu'une référence de superclasse est assignée à une variable de référence de sous-classe.
     * Et cela fonctionne à l'exécution car l'objet référencé par a est en fait un tableau de B.
     * Maintenant, le cast à la ligne //3 dit au compilateur de ne pas s'inquiéter, que je suis un bon programmeur et que je sais ce que je fais et que l'objet référencé par la référence de la super-classe (a1) sera en fait de la classe B au moment de l'exécution.
     * Il n'y a donc pas d'erreur à la compilation.
     * Mais au moment de l'exécution, cela échoue parce que l'objet réel n'est pas un tableau de B mais un tableau de A.
     * @param args
     */
    public static void main(String args[]){
        A[] a, a1;
        B[] b;
        a = new A[10]; a1  = a;
        b =  new B[20];
        a = b;  // 1
        b = (B[]) a;  // 2
        b = (B[]) a1; // 3
    }
}
class A { }
class B extends A { }


