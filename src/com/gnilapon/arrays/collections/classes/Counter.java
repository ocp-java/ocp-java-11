package com.gnilapon.arrays.collections.classes;

public class Counter<T> {  //1
    T t;

    /**
     * La cause de la confusion dans le code donné est la présence de <T> dans la déclaration de la classe ainsi que dans la déclaration de la méthode.
     * Il faut savoir que les deux variables de type générique sont indépendantes l'une de l'autre.
     * Cela signifie que le <T> de la classe publique Counter<T> n'a aucune relation avec le <T> de la méthode publique <T> int count(T[] ta, T t).
     * Étant donné que les deux types sont différents, vous ne pouvez pas faire ceci.t = t ; à l'intérieur de la méthode.
     * Idéalement, il convient d'utiliser des noms différents pour les différents types afin d'éviter toute confusion.
     * Par exemple : class Counter<T>{
     *                  T t ;
     *                  public <P extends T> int count(P[] ta, P t){
     *                      this.t = t ; //ceci est valide car P extends T, donc un P est un T
     *                      int count = 0 ;
     *                      for(P x : ta){
     *                           count = x == t ? count+1 : count ;
     *                           }
     *                      return count ;
     *                  }
     *               }
     * Si vous souhaitez utiliser le même type que celui utilisé pour la classe, vous ne devez pas le redéclarer pour la méthode.
     * Par exemple : class Counter<T>{
     *                           //<--- <T> est déclaré ici
     *                           T t ;
     *                           public int count(T[] ta, T t){
     *                              //<--- il n'y a pas de déclaration <T> sur cette ligne
     *                              this.t = t ;
     *                              //cela est valable maintenant car t et this.t sont du même type
     *                              int count = 0 ; for(T x : ta){
     *                                                  count = x == t ? count+1 : count ;
     *                                               }
     *                              return count ;
     *                              }
     *                 }
     * @param ta
     * @param t
     * @return
     * @param <T>
     */
    public <T> int count(T[] ta, T t) {  //2
        //this.t = t;  //3
        int count = 0;
        for (T x : ta) {
            count = x == t ? count + 1 : count;//4
        }
        return count;
    }
}
