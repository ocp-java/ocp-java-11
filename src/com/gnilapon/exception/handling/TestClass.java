package com.gnilapon.exception.handling;
import java.io.*;

public class TestClass{

    /**
     * IndexOutOfBoundsException est gérée par le premier bloc de catch.
     * À l'intérieur de ce bloc, une nouvelle exception NullPointerException est déclenchée.
     * Comme cette exception n'est pas lancée à l'intérieur du bloc try, elle ne sera pas prise en compte par les autres blocs catch.
     * Elle sera en fait envoyée à l'appelant de la méthode main() après l'exécution du bloc finally.
     * (Le code qui imprime END n'est jamais atteint, puisque l'exception NullPointerException n'est pas prise en compte après l'exécution du bloc finally.
     * À la fin, une trace de pile pour l'exception NullPointerException sera imprimée.
     * @param args
     */
    public static void main0(String args[]){
        try{
            m1();
        }catch(IndexOutOfBoundsException e){
            System.out.println("1");
            throw new NullPointerException();
        }catch(NullPointerException e){
            System.out.println("2");
            return;
        }catch (Exception e) {
            System.out.println("3");
        }finally{
            System.out.println("4");
        }
        System.out.println("END");
    }
    static void m1(){
        System.out.println("m1 Starts");
        throw new IndexOutOfBoundsException( "Big Bang " );
    }

    /**
     * Étant donné que la méthode amethod() ne lève aucune exception, try est imprimé et le contrôle passe à finally qui imprime finally.
     * Après cela, out est imprimé.
     * @param args
     * @throws Exception
     */
    public static void main1(String[] args) throws Exception{
        try{
            amethod();
            System.out.println("try ");
        }catch(Exception e){
            System.out.print("catch ");
        }finally{
            System.out.print("finally ");
        }
        System.out.print("out ");
    }
    public static void amethod(){ }


    /**
     * finally est toujours exécuté (même si vous levez une exception dans try ou catch) mais c'est l'exception à la règle.
     * Lorsque vous appelez la méthode System.exit, la JVM se termine.
     * Il n'y a donc aucun moyen d'exécuter le bloc finally.
     * @param args
     */
    public static void main2(String[] args){
        try{
            System.exit(0);
        }
        finally{
            System.out.println("finally is always executed!");
        }
    }

    public static void doStuff() throws Exception{
        System.out.println("Doing stuff...");
        if(Math.random()>0.1){
            throw new Exception("Too high!");
        }
        System.out.println("Done stuff.");
    }

    /**
     * Il n'y a que deux possibilités :
     * 1. Si Math.random() génère un nombre supérieur à 0,4, la partie if lèvera une exception.
     * Dans ce cas, le code restant de doStuff ne sera pas appelé et main() recevra une exception due à l'appel à doStuff.
     * Comme doStuff() n'est pas dans un bloc try/catch, l'exception se propagera vers le haut et le code restant dans main() ne sera pas exécuté non plus.
     * Puisque l'exception n'est rattrapée nulle part dans le code, elle atteindra finalement le thread de la JVM qui a appelé la méthode main.
     * Ce thread attrape l'exception et imprime la trace de la pile.
     * 2. Si Math.random() génère un nombre inférieur ou égal à 0,4, la partie ne sera pas exécutée et le message "Done stuff." sera imprimé.
     * Après le retour de l'appel à main(), "Over" sera également imprimé.
     * @param args
     * @throws Exception
     */
    public static void main3(String[] args) throws Exception {
        doStuff();
        System.out.println("Over");
    }

    public static void m() throws Exception{
        throw new Exception("Exception from m1");
    }
     public static void m2() throws Exception{
        try{
            m();
        }catch(Exception e){
            //Can't do much about this exception so rethrow it
            throw e;
        }
        finally{
            throw new RuntimeException("Exception from finally");
        }
    }

    /**
     * Dans le code donné, la méthode m2() lève une exception explicitement dans le bloc catch ainsi que dans le bloc finally.
     * Comme il s'agit d'un bloc finally explicite (et non d'un bloc finally implicite créé lorsque vous utilisez une instruction try-with-resources), l'exception levée par le bloc finally est celle qui est levée par la méthode.
     * L'exception levée par le bloc catch est perdue.
     * Elle n'est pas ajoutée à la liste des exceptions supprimées de l'exception lancée par le bloc finally.
     * Par conséquent, e.getSuppressed() renvoie un tableau avec 0 élément et rien n'est imprimé.  Si le code de m2() avait été quelque chose comme ceci : public static void m2() throws Exception{ try( SomeResource r ... ){ m1() ; }     } Maintenant, si m1() lève une exception et que r.close() en lève également une, l'exception levée par m1 aurait été celle levée par la méthode m2 et l'exception levée par r.close() aurait été ajoutée à la liste des exceptions supprimées de l'exception levée dans le bloc try.
     * Notez que le bloc try-with-resource a été amélioré dans Java 9 et qu'il vous permet maintenant d'utiliser une variable déclarée avant l'instruction try dans le bloc try-with-resource.
     * Par exemple, ce qui suit est valable depuis Java 9 : Statement stmt = c.createStatement() ; try(stmt){ ... } Cependant, try(stmt = c.createStatement() ;) n'est toujours pas valide.
     * @param args
     */
    public static void main4(String[] args) {
        try{
            m2();
        }catch(Exception e){
            Throwable[] ta = e.getSuppressed();
            for(Throwable t : ta) {
                System.out.println(t.getMessage());
            }
        }
    }


    /**
     * La méthode principale lance une exception vérifiée mais il n'y a pas de bloc try/catch pour la gérer et il n'y a pas non plus de clause throws qui déclare l'exception vérifiée.
     * Elle ne sera donc pas compilée.
     * Si vous placez un bloc try/catch ou déclarez une clause throws pour la méthode, celle-ci lancera une NullPointerException au moment de l'exécution parce que e est null.
     * Une méthode qui lève une exception "vérifiée", c'est-à-dire une exception qui n'est pas une sous-classe d'Error ou de RuntimeException, doit soit la déclarer dans la clause throws, soit placer le code qui lève l'exception dans un bloc try/catch.
     * @param args
     */
    public static void main5(String args[]){
        Exception e = null;
        //throw e;
    }


    /**
     * Les ressources sont fermées automatiquement à la fin du bloc try dans l'ordre inverse de leur création.
     * @param args
     * @throws Exception
     */
    public static void main6(String[] args) throws Exception {
        try(FileReader fr = new FileReader("c:\\temp\\license.txt");
            FileWriter fw = new FileWriter("c:\\temp\\license2.txt") )
        {
            int x = -1;
            while( (x = fr.read()) != -1){
                fw.write(x);
            }
        }
    }

    static int i1, i2, i3;

    /**
     * La règle est la suivante : Si l'évaluation d'une expression d'argument se termine brusquement, aucune partie de l'expression d'argument à sa droite n'est évaluée.
     * Initialement, les valeurs de i1, i2, i3 sont toutes égales à 0 ; Maintenant, la seule instruction importante de ce code est : test(i1 = 1, oops(i2=2), i3 = 3) ; Avant d'appeler la méthode test, les paramètres doivent évidemment être évalués.
     * Par conséquent, 1 est assigné à i1, 2 est assigné à i2.
     * Maintenant, avant que 3 soit assigné à i3, oops(2) sera appelé (puisque i2 est maintenant 2) Cependant, oops() lève une exception, donc la JVM n'a jamais l'occasion d'exécuter i3 = 3 ; et la méthode test() n'est pas non plus appelée (à cause de l'exception dans l'évaluation des paramètres).
     * Donc, dans le bloc catch, il est imprimé 1 2 0.
     * @param args
     */
    public static void main(String[] args)    {
        try {
            test(i1 = 1, oops(i2=2), i3 = 3);
        } catch (Exception e) {
            System.out.println(i1+" "+i2+" "+i3);
        }
    }
    static int oops(int i) throws Exception    {
        throw new Exception("oops");    }
    static int test(int a, int b, int c) {
        return a + b + c;
    }
}


