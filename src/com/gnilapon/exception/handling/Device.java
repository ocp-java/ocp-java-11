package com.gnilapon.exception.handling;

import java.io.IOException;

public class Device implements AutoCloseable {
    String header = null;

    public void open() {
        header = "OPENED";
        System.out.println("Device Opened");
    }

    public String read() throws IOException {
        throw new IOException("Unknown");
    }

    public void writeHeader(String str) throws IOException {
        System.out.println("Writing : " + str);
        header = str;
    }

    public void close() {
        header = null;
        System.out.println("Device closed");
    }


    /**
     * Notez que le try-with-resource a été amélioré dans Java 9 et qu'il vous permet désormais d'utiliser une variable déclarée avant l'instruction try dans le bloc try-with-resource.
     * Par exemple, l'exemple suivant est valable depuis Java 9 : Device d = new Device() ; try(d){ ... }  Cependant, l'instruction try(d = new Device() ;) n'est toujours pas valide.
     */
    public static void testDevice() {
        Device d = new Device();
        try (d) {
            d.open();
            d.read();
            d.writeHeader("TEST");
            d.close();
        } catch (IOException e) {
            System.out.println("Got Exception");
        }
    }

    /**
     * Les blocs catch et finally sont exécutés après la fermeture de la ressource ouverte dans try-with-resources.
     * Par conséquent, le message Device Closed sera imprimé avant le message Got Exception.
     * @param args
     */
    public static void main(String[] args) {
        Device.testDevice();
    }
}
