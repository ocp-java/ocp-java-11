package com.gnilapon.exception.handling;

public class Test {
    static String j = "";

    public static void method(int i) {
        try {
            if (i == 2) {
                throw new Exception();
            }
            j += "1";
        } catch (Exception e) {
            j += "2";
            return;
        } finally {
            j += "3";
        }
        j += "4";
    }

    /**
     * Essayez de suivre le flux de contrôle :
     * 1. dans la méthode(1) : i n'est pas 2 donc, j obtient "1" puis finally est exécuté ce qui fait que j = "13" et ensuite la dernière instruction (j +=4) est exécutée ce qui fait que j = "134".
     * 2. in method(2) : i est 2, donc il va dans le bloc if qui lance une exception.
     * Ainsi, aucune des instructions du bloc try n'est exécutée et le contrôle passe à catch qui rend j = "1342", puis finalement rend j = "13423" et le contrôle est retourné. Notez que la dernière instruction ( j+=4) n'est pas exécutée car une exception a été levée dans le bloc try, ce qui fait que le contrôle passe au bloc catch, qui renvoie à son tour.
     * @param args
     */
    public static void main(String args[]) {
        method(1);
        method(2);
        System.out.println(j);
    }
}
