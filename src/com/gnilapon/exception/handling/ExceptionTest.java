package com.gnilapon.exception.handling;

public class ExceptionTest {
    void myMethod() throws MyException {
        throw new MyException3();
    }

    /**
     * Vous pouvez avoir plusieurs blocs catch pour attraper différents types d'exceptions, y compris des exceptions qui sont des sous-classes d'autres exceptions.
     * Toutefois, la clause catch pour les exceptions plus spécifiques (c'est-à-dire une SubClassException) doit précéder la clause catch pour les exceptions plus générales (c'est-à-dire une SuperClassException).
     * Dans le cas contraire, une erreur de compilation se produit, car l'exception plus spécifique est inaccessible.
     * Dans ce cas, catch for MyException3 ne peut pas suivre catch for MyException parce que si MyException3 est lancée, elle sera rattrapée par la clause catch de MyException. Il est donc impossible que la clause catch pour MyException3 s'exécute.
     * Elle devient donc une déclaration inaccessible.
     * @param args
     */
    public static void main(String[] args) {
        ExceptionTest et = new ExceptionTest();
        try {
            et.myMethod();
        } catch (MyException me) {
            System.out.println("MyException thrown");
        }
        /*catch(MyException3 me3){
            System.out.println("MyException3 thrown");
        }*/ finally {
            System.out.println(" Done");
        }
    }
}

class MyException extends Throwable{}
class MyException1 extends MyException{}
class MyException2 extends MyException{}
class MyException3 extends MyException2{}
