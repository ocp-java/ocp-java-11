package com.gnilapon.exception.handling;

import java.io.*;

public class FileCopier {

    /**
     * Les variables auto-fermables définies dans l'instruction try-with-resources sont implicitement finales.
     * Elles ne peuvent donc pas être réaffectées.
     * @param records1
     * @param records2
     */
    public static void copy(String records1, String records2) {
        try (InputStream is = new FileInputStream(records1);
             OutputStream os = new FileOutputStream(records2);) {  //1
            //if(os == null) os = new FileOutputStream("c:\\default.txt");  //2
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = is.read(buffer)) != -1) {  //3
                os.write(buffer, 0, bytesRead);
                System.out.println("Read and written bytes " + bytesRead);
            }
        } catch (IOException e) { //4
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        copy("c:\\temp\\test1.txt", "c:\\temp\\test2.txt");
    }
}
