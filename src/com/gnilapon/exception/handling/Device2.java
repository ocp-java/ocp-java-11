package com.gnilapon.exception.handling;

import java.io.IOException;

public class Device2 implements AutoCloseable {
    boolean open = false;

    public Device2() {
        open = true;
    }

    public String read() throws IOException {
        throw new IOException("Can't read!");
    }

    public boolean isOpened() {
        return open;
    }

    public void close() {
        open = false;
        System.out.println("Device closed");
    }

    /**
     * L'intérêt d'une instruction try-with-resource est que la ressource spécifiée dans le bloc try with resources doit être fermée dès que le bloc try se termine (c'est-à-dire avant que le contrôle ne passe au bloc catch ou finally), indépendamment de ce qui se passe dans le bloc try.
     * Ainsi, la méthode close() sera invoquée sur toutes les ressources spécifiées dans le bloc try with resource même si l'appel à d2.read() lance une exception.
     * Dans ce cas, trois ressources ont été spécifiées - d1, d2 et d3 (d1 n'est pas instancié dans le bloc try with resource, mais ce n'est pas grave) et donc, la méthode close() sera invoquée sur d3, d2 et d1 (dans cet ordre).
     * @param args
     */
    public static void main(String[] args) {
        Device2 d1 = new Device2();
        try (d1;
             Device2 d2 = new Device2();
             Device2 d3 = new Device2()) {
            d2.read();
        } catch (Exception e) {
            System.out.println("Got Exception " + e.getMessage());
        }
    }
}

