package com.gnilapon.exception.handling;

import java.io.IOException;

public class Device1 implements AutoCloseable {
    String header = null;

    public Device1(String name) throws IOException {
        header = name;
        if ("D2".equals(name)) throw new IOException("Unknown");
        System.out.println(header + " Opened");
    }

    public String read() throws IOException {
        return "";
    }

    public void close() {
        System.out.println("Closing device " + header);
        throw new RuntimeException("RTE while closing " + header);
    }

    /**
     * La sortie suivante, obtenue après l'exécution du programme, explique ce qui se passe :
     * D1 Opened Closing device D1 Exception in thread "main" java.io.IOException :
     * Unknown at
     * trywithresources.Device.<init>(Device.java:9) at
     * trywithresources.Device.main(Device.java:24) Supprimé : java.lang.RuntimeException : RTE while closing D1 at
     * trywithresources.Device.close(Device.java:19) at
     * trywithresources.Device.main(Device.java:26) Résultat Java :
     * 1 Le dispositif D1 est créé avec succès mais une IOException est levée lors de la création du dispositif D2.
     * Ainsi, le contrôle n'entre jamais dans le bloc try et throw new Exception("test") n'est jamais exécuté.
     * Puisqu'une ressource a été créée, sa méthode de fermeture sera appelée (ce qui imprime Fermeture du périphérique D1).
     * Toute exception levée lors de la fermeture d'une ressource est ajoutée à la liste des exceptions supprimées de l'exception levée lors de l'ouverture d'une ressource (ou levée à partir du bloc d'essai).
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        try (Device1 d1 = new Device1("D1");
             Device1 d2 = new Device1("D2")) {
            throw new Exception("test");
        }
    }
}
