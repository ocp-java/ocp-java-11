package com.gnilapon.oop.mains;

import com.gnilapon.oop.classes.*;
import com.gnilapon.oop.interfaces.House;

public class TestClass {

    void objectOfC1(){
        /**
         * C1 héritera de m1() de B1 qui, à son tour, hérite de m1() de A.
         */
        C1 c = new C1();
        c.m1();
    }

    static Wrapper changeWrapper(Wrapper w){
        w = new Wrapper();//This instruction reinitialise le parametre w
        w.w += 9;
        return w;
    }


    public static void main0(String[] args){
        var w = new Wrapper();
        w.w = 20;
        changeWrapper(w); // cet appel n'inpacte pas w puisque la methode reinitialise w
        w.w += 30;
        System.out.println(w.w);
        w = changeWrapper(w);
        System.out.println(w.w);
    }

    long l1;
    public void TestClass(long pLong) { l1 = pLong ; }  //(1)

    /**
     * La déclaration en (1) déclare une méthode, et non un constructeur, parce qu'elle a une valeur de retour.
     * Il se trouve que la méthode porte le même nom que la classe, mais ce n'est pas grave.
     * La classe a un constructeur implicite par défaut puisqu'elle ne contient aucune déclaration de constructeur.
     * Cela permet à l'instanciation en (2) de fonctionner.
     * @param args
     */
    public static void main1(String args[]){
        TestClass a, b ;
        a = new TestClass();  //(2)
        //b = new TestClass(5);  //(3) Because (1) is a method and not a constructor. So there is no constructor that take a parameter.
    }

    /**
     * Le code ne pose aucun problème.
     * Une sous-interface peut parfaitement remplacer une méthode par défaut de l'interface de base.
     * Une classe qui implémente une interface peut également surcharger une méthode par défaut.
     * MyHouse peut dire qu'elle implémente Bungalow ainsi que House, même si House est redondant puisque Bungalow est de toute façon une House.
     * @param args
     */
    public static void main2(String[] args) {
        House ci = new MyHouse();  //1
        System.out.println(ci.getAddress()); //2
    }

    /**
     * Rappelez-vous la règle selon laquelle, dans le cas des méthodes d'instance, c'est toujours la classe de l'objet réel qui détermine quelle version d'une méthode surchargée est invoquée.
     * Ici, observez que la méthode max de la classe A est surchargée par la classe B, puis par la classe C.
     * Ainsi, lorsque main appelle c.max(10, 20), la méthode max de la classe C sera invoquée avec les paramètres 10 et 20 car, même si le type déclaré de c est B, l'objet réel référencé par c est de la classe C.
     * La méthode max de C appelle la méthode max définie dans B avec les paramètres 2*10 et 2*20, c'est-à-dire 20 et 40.
     * La méthode max de B appelle à son tour la méthode max de A avec les paramètres 20 et 40.
     * La méthode max de A renverra 40 à la méthode max() de B.
     * La méthode max() de B renverra 80 à la méthode max() de C.
     * Enfin, la méthode max de C renverra 80 dans main, qui sera imprimé.
     * @param args
     */
    public static void main3(String args[]){
        B1 c = new C1();
        System.out.println(c.max(10, 20));
    }

    /**
     * Observez que la méthode startUp(String s) de CorbaComponent est surchargée par la sous-classe OrderManager.
     * Lorsqu'un objet de la classe OrderManager est construit, le constructeur no args par défaut de CorbaComponent est appelé. Ce constructeur appelle la méthode startUp(String s) avec "IOR" comme paramètre.
     * Maintenant, il y a deux méthodes éligibles qui peuvent être appelées - CorbaComponent's startUp et OrderManager's startUp.  La sélection de la méthode est faite sur la base de la classe de l'objet (qui est ici OrderManager).
     * Ainsi, le startUp de OrderManager est appelé, ce qui définit la variable ior à URL://IOR.
     * Contrairement à la sélection des méthodes d'instance, la sélection des variables (et des méthodes statiques) se fait sur la base de la classe déclarée de la variable et non sur la classe réelle de l'objet auquel elle se réfère.
     * @param args
     */
    public static void main4(String args[]){
        start(new OrderManager());
    }
    static void start(CorbaComponent cc){
        cc.print();
    }

    static int a = 0;
    int b = 5;
    public void foo(){
        while(b>0){
            b--;
            a++;
            System.out.println("value of a : "+a);
        }
    }

    /**
     * Le champ a est statique et il n'y aura qu'une seule copie de a, quel que soit le nombre d'instances de Test que vous créez.
     * Les modifications apportées à ce champ par une instance seront également répercutées dans l'autre instance.
     * Mais le champ b est un champ d'instance.
     * Chaque instance de Test aura sa propre copie de b.
     * Par conséquent, lorsque vous appelez p1.foo() puis p2.foo(), le même champ a est incrémenté 5 fois deux fois et il s'imprimera donc 10 10.
     * @param args
     */
    public static void main(String[] args) {
        var p1 = new TestClass();
        p1.foo();
        var p2 = new TestClass();
        p2.foo();
        System.out.println(p1.a+" "+p2.a);
    }

}
