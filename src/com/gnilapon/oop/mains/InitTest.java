package com.gnilapon.oop.mains;

/**
 * Tout d'abord, les instructions/blocs statiques sont appelés DANS L'ORDRE où ils sont définis.
 * Ensuite, les instructions/blocs d'initialisation d'instance sont appelés dans l'ordre de leur définition.
 * Enfin, le constructeur est appelé.
 * Il imprime donc a b c 2 3 4 1.
 */
public class InitTest {
    public InitTest(){
        s1 = sM1("1");
    }
    static String s1 = sM1("a");
    String s3 = sM1("2");

    {
        s1 = sM1("3");
    }
    static{
        s1 = sM1("b");
    }
    static String s2 = sM1("c");
    String s4 = sM1("4");
    public static void main(String args[]){
        InitTest it = new InitTest();
    }
    private static String sM1(String s){
        System.out.println(s);  return s;
    }
}
