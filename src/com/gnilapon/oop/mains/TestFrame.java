package com.gnilapon.oop.mains;

import java.awt.*;
import java.awt.event.*;

/**
 * Il ne sera pas compilé parce que la variable membre "s" n'est pas accessible à partir de la classe interne.
 * En effet, la classe interne est créée par une méthode statique et n'a donc pas de référence à l'objet TestFrame.
 * Le message exact est le suivant :
 * TestFrame.java:22 : la variable non statique s ne peut pas être référencée à partir d'un contexte statique.
 * System.out.println("Message is " +s) ;
 *                                   ^
 * 1 erreur
 */
class TestFrame extends Frame {
    String s="Message";
    public static void main(String args[]){
        var t = new TestFrame();
        var b = new Button("press me");
        b.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //System.out.println("Message is " +s);
            }
        });
        t.add(b);
    }
}
