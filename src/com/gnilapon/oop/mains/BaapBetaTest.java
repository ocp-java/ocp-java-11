package com.gnilapon.oop.mains;
import com.gnilapon.oop.classes.Baap;
import com.gnilapon.oop.classes.Beta;


/**
 * N'oubliez jamais que les méthodes d'instance sont surchargées et que les variables (et les méthodes statiques) sont cachées.
 * La méthode d'instance invoquée dépend de la classe de l'objet réel, tandis que le champ (et la méthode statique) auquel on accède dépend de la classe de la variable.
 * Ici, b fait référence à un objet de la classe Beta, de sorte que b.getH() appellera toujours la méthode surchargée (méthode de la sous-classe).
 * Cependant, le type de référence de b est Baap.
 * donc b.h fera toujours référence au h de Baap.
 * De plus, à l'intérieur de getH() de Beta, le h de Beta sera accédé au lieu du h de Baap parce que vous accédez à this.h ('this' est implicite) et le type de this est Beta.
 */
public class BaapBetaTest {
    public static void main(String[] args) {
        Baap b = new Beta();
        System.out.println(b.h + " " + b.getH());
        Beta bb = (Beta) b;
        System.out.println(bb.h + " " + bb.getH());
    }
}
