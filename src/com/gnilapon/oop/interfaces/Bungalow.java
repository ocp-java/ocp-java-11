package com.gnilapon.oop.interfaces;

public interface Bungalow extends House{
    default String getAddress(){
        return "101 Smart Str";
    }
}
