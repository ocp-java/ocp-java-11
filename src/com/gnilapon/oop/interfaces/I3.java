package com.gnilapon.oop.interfaces;

/**
 * Les interfaces ne peuvent pas implémenter une autre interface.
 * Elles peuvent étendre plusieurs interfaces.
 */
/**
 * interface I3 implements I1{
    void perform_work();
}*/
