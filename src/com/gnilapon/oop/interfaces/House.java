package com.gnilapon.oop.interfaces;

public interface House{
    default String getAddress(){
        return "101 Main Str";
    }
}
