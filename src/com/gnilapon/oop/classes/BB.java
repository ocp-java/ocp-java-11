package com.gnilapon.oop.classes;


/**
 * Notez que la méthode print() est surchargée dans la classe B.
 * En raison du polymorphisme, la méthode à exécuter est sélectionnée en fonction de la classe de l'objet réel.
 * Ici, lorsqu'un objet de la classe B est créé, le constructeur par défaut de B (qui n'est pas visible dans le code mais qui est automatiquement fourni par le compilateur parce que B ne définit pas de constructeur explicitement) est d'abord appelé.
 * La première ligne de ce constructeur est un appel à super(), qui invoque le constructeur de A.
 * Le constructeur de A appelle à son tour super().
 * Le constructeur de A appelle à son tour print().
 * Or, print est une méthode d'instance non privée et est donc polymorphe, ce qui signifie que le choix de la méthode à exécuter dépend de la classe de l'objet réel sur lequel elle est invoquée.
 * Ici, puisque la classe de l'objet réel est B, la méthode print de B est sélectionnée au lieu de la méthode print de A.
 * À ce stade, la variable i n'a pas été initialisée (parce que nous sommes encore en train d'initialiser A), et c'est donc sa valeur par défaut, c'est-à-dire 0, qui est imprimée.
 * Enfin, 4 est imprimé.
 * NOTE : Essayez ce code après avoir déclaré i dans la classe B comme final et observez la sortie.
 */
public class BB extends AA{
    int i =   4;
    //final int i =   4;
    public static void main(String[] args){
        AA a = new BB();
        a.print();
    }
    void print() {
        System.out.print(i+" ");
    }
}
