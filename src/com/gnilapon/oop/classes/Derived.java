package com.gnilapon.oop.classes;

import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;

/**
 * Les règles applicables aux paramètres de type multiple sont les mêmes que celles applicables à un paramètre de type unique.
 * Vous devez appliquer les mêmes règles pour les deux paramètres de type séparément.
 * Par exemple, nous savons que A<S> est un sous-type valide de A< ? extends T> (où S est un sous-type de T).
 * Par conséquent, Map<Integer, Integer> est un sous-type valide de Map<T extends Number, Z extends Number>.
 * Les limites définies par <T extends Number> et <T> sont différentes.
 * Par conséquent, la liste des paramètres de //1, c'est-à-dire getMap(T t, Z z), est différente de la liste des paramètres de la méthode de la classe Base.
 * Il s'agit donc d'une surcharge valide.
 */
public class Derived extends Base{
    public  <T, Z> TreeMap<T, Z> getMap(T t, Z z) {
        return new TreeMap<T, Z>();
    } //1
    /*public Map<Number, Number> getMap(Number t, Number z) {
        return new TreeMap<Number, Number>();
    } *///2
    public  Map<Integer, Integer> getMap(Number t, Number z) {
        return new HashMap<Integer, Integer>();
    };   //3
}
