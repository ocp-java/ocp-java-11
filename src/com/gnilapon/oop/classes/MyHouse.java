package com.gnilapon.oop.classes;

import com.gnilapon.oop.interfaces.Bungalow;
import com.gnilapon.oop.interfaces.House;

public class MyHouse implements Bungalow, House {
    public String getAddress(){
        return "MyHouse Smart Str";
    }
}
