package com.gnilapon.oop.classes;

/**
 * d est un membre d'instance de Outer et est donc accessible à tous les autres membres d'instance de la classe, y compris une classe interne non statique.
 * Vous pouvez accéder à d à partir d'une classe interne statique en utilisant une référence à une instance de Outer.
 */
public class Outer {

    private double d = 10.0;

    //put inner class here.

    /**
     * Il ne sera pas compilé à cause de 'this.d'.
     * Pour accéder à d, vous devez utiliser soit d, soit Outer.this.d.
     */
    class Inner {
        public void m1() {
            //this.d = 20.0;
            Outer.this.d = 20.0;
        }
    }

    abstract class Inner1 {
        public void m1() {
            d = 20.0;
        }
    }

    final class Inner2 {
        public void m1() {
            d = 20.0;
        }
    }

    private class Inner3 {
        public void m1() {
            d = 20.0;
        }
    }

    static class Inner4 {
        public void m1() {
            Outer o =new Outer();
            o.d = 20.0;
        }
    }

}
