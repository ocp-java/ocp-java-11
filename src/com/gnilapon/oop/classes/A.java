package com.gnilapon.oop.classes;

/**
 * Les classes n'étendent pas les interfaces, elles les implémentent.
 * L'utilisation de @Override est cependant valable.
 */
/**class A extends I1 {
    String s;
    @Override
    void setValue(String val) {
        s = val;
    }
    String getValue() {
        return s;
    }
}*/
