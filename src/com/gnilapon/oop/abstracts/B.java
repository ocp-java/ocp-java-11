package com.gnilapon.oop.abstracts;

import com.gnilapon.oop.interfaces.I1;

/**
 * Observez la signature de la méthode getValue.
 * Elle n'est pas identique à la méthode getValue de I1.
 * Cette méthode getValue ne surcharge pas la méthode getValue de I1.
 * Par conséquent, @Override n'est pas valide ici.
 */
abstract class B implements I1 {
    /**
     * get Value
     * @param i
     * @return int
     */
    /*@Override
    public int getValue(int i) {
        return 0;
    }*/
}

