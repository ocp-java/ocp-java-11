package com.gnilapon.controlling.program.flow;

public class TestClass {


    /**
     * 1. Définir i comme char ne signifie pas qu'il ne peut contenir que des caractères (a, b, c, etc.).
     * Il s'agit d'un type de données intégral qui peut prendre n'importe quelle valeur entière +ive comprise entre 0 et 2^16 -1.
     * 2. L'entier 0 ou 1, 2 etc. n'est pas identique au caractère '0', '1' ou '2' etc.
     * Ainsi, lorsque i est égal à 0, rien n'est imprimé et i est incrémenté à 1 (en raison de i++ dans le commutateur).
     *  i est ensuite à nouveau incrémenté par la boucle for pour l'itération suivante. i devient donc 2.
     * Lorsque i = 2, "C" est imprimé et i est incrémenté à 3 (en raison de i++ dans le commutateur), puis i est incrémenté à 4 par la boucle for, de sorte que i devient 4.
     * Lorsque i = 4, "E" est imprimé et comme il n'y a pas de break, on passe au cas "E" et "F" est imprimé.
     * i est incrémenté à 5 (en raison de i++ dans le commutateur), puis il est à nouveau incrémenté à 6 par la boucle for. Comme i < 5 est maintenant faux, la boucle for se termine.
     * @param args
     */
    public static void main0(String args[]){
        char i;
        LOOP: for (i=0;i<5;i++){
            System.out.println(i);
            switch(i++){
                case '0': System.out.println("A");
                case 1: System.out.println("B"); break LOOP;
                case 2: System.out.println("C"); break;
                case 3: System.out.println("D"); break;
                case 4: System.out.println("E");
                case 'E' : System.out.println("F");
            }
        }
    }

    /**
     * Dans ce type de questions, vous devrez calculer les valeurs des variables de la boucle pour chaque itération (à moins que vous ne puissiez reconnaître le modèle) sur votre feuille de calcul.
     * Début i=0, j=10, count = 0
     * Itération 1 : i < j est vrai, i devient 1, j devient 9, count devient 1
     * Itération 2 : i < j est vrai, i devient 2, j devient 8, count devient 2
     * Itération 3 : i < j est vrai, i devient 3, j devient 7, count devient 3
     * Itération 4 : i < j est vrai, i devient 4, j devient 6, count devient 4
     * Itération 5 : i < j est vrai, i devient 5, j devient 5, count devient 5
     * Itération 6 : i < j est faux donc la boucle while n'est pas entrée.
     * Imprimer 5 5 5.
     * @param args
     */
    public static void main1(String[] args) {
        int i=0, j=10;
        var count = 0;
        while (i<j) {
            i++;
            j--;
            count++;
        }
        System.out.println(i+" "+j+" "+count);
    }

    /**
     * Puisqu'il existe une condition de cas qui correspond à la chaîne d'entrée "c", cette instruction de cas sera exécutée directement.
     * Ceci imprime "cat".
     * Puisqu'il n'y a pas de pause après cette instruction case et l'instruction case suivante, le contrôle passera par l'instruction case suivante (qui est par défaut : ) et donc "none" sera également imprimé.
     * @param input
     */
    public void switchString(String input){
        switch(input){
            case "a" : System.out.println( "apple" );
            case "b" : System.out.println( "bat" );break;
            case "c" : System.out.println( "cat" );
            default : System.out.println( "none" );
        }
    }
    public static void main2(String[] args) throws Exception {
        var tc = new TestClass();
        tc.switchString("c");
    }

    /**
     * c'|'d' produit une valeur de caractère qui est le résultat de l'opération OU bit à bit sur 'c' et 'd'.
     * Cela ne signifie pas que le cas sera exécuté lorsque la valeur de c est 'c' ou 'd'.
     * Ce cas sera exécuté lorsque la valeur de c correspond à la valeur du OU bit à bit de 'c' et 'd', qui est en fait la même que la valeur du caractère 'g'.
     * Voici comment 'c'|'d' est calculé : 'c' = 99 = 1100011 'd' = 100 = 1100100 'c'|'d' = 1100111 <== 103, comme 'g'.
     * Compte tenu des informations ci-dessus, il est facile de voir que i sera incrémenté trois fois lorsque c est 'a' (parce qu'il n'y a pas d'instruction break dans les blocs case), deux fois lorsque c est 'b' (parce qu'il n'y a pas d'instruction break dans le bloc case) et zéro fois lorsque c est 'c' et 'd'.
     * Par conséquent, la valeur finale de i sera 5.
     * Notez qu'il n'y a pas de différence entre ++i et i++ dans la situation donnée.
     * @param args
     */
    public static void main3(String[] args){
        var ca = new char[]{'a', 'b', 'c', 'd'};
        var i = 0;
        for(var c : ca){
            switch(c){
                case 'a' : i++;
                case 'b' : ++i;
                case 'c'|'d' : i++;
            }
        }
        System.out.println("i = "+i);
    }


    /**
     * ++i < 5 signifie qu'il faut incrémenter la valeur de i et la comparer à 5. Essayez maintenant de calculer les valeurs de i et de j à chaque itération.
     * Pour commencer, i=0 et j=11.
     * Au moment de l'évaluation de la condition while, i et j sont les suivants :
     * j = 10 et i=1 (la boucle continuera parce que i<5) (Rappelez-vous que la comparaison aura lieu APRÈS l'incrémentation de i parce que c'est ++i et non i++.
     * j = 9 et i=2 (la boucle continuera parce que i<5).
     * j = 8 et i=3 (la boucle continue car i<5).
     * j = 7 et i=4 (la boucle continuera car i<5).
     * j = 6 et i=5 (la boucle ne continuera PAS parce que i n'est pas <5).
     * Donc, il imprimera 5 6. (Il imprime d'abord i et ensuite j).
     * @param args
     */
    public static void main4(String[] args){
        int i=0, j=11;
        do{
            if(i > j) { break; }
            j--;
        }while( ++i < 5);
        System.out.println(i+"  "+j);
    }

    /**
     * Le point à noter ici est qu'une rupture sans étiquette rompt la boucle extérieure la plus interne.
     * Ainsi, dans ce cas, chaque fois que k>j, la boucle C est interrompue.
     * Vous devriez exécuter le programme et le suivre pas à pas pour comprendre comment il progresse.
     * @param args
     */
    public static void main5(String args[]){
        int c = 0;
        A: for(var i = 0; i < 2; i++){
            System.out.println("loop i "+i);
            System.out.println("********loop i ********");
            System.out.println();
            B: for(var j = 0; j < 2; j++){
                System.out.println("loop j "+j);
                System.out.println("********loop j ********");
                C: for(var k = 0; k < 3; k++){
                    c++;
                    System.out.println("value c = "+c);
                    if(k>j) break;
                }
                System.out.println();
            }
            System.out.println();
            System.out.println();
            System.out.println();
        }
        System.out.println(c);
    }


    /**
     * 1. La condition while utilise l'opérateur post incrément, ce qui signifie que le nombre est d'abord comparé à 11 (et sur la base de cette comparaison, une décision est prise pour savoir si la boucle doit être exécutée à nouveau ou non) et ensuite incrémenté.
     * Ainsi, lorsque le compte est 10, la condition 10<11 est vraie (ce qui signifie que la boucle doit être exécutée à nouveau) et le compte est incrémenté à 11.
     * 2. Lorsque l'effectif est complètement divisible par 3 (c'est-à-dire lorsque l'effectif est 0, 3, 6, 9), sum+=count ; n'est pas exécuté.
     * Ainsi, le résultat est la somme de : 1 2 4 5 7 8 10 11
     * @param args
     */
    public static void main6(String[] args) {
        int count = 0, sum = 0;
        do{
            if(count % 3 == 0) continue;
            sum+=count;
        }
        while(count++ < 11);
        System.out.println(sum);
    }

    /**
     * Contrairement à la boucle "while(){}", la boucle "do {} while()" s'exécute au moins une fois car la condition est vérifiée après l'itération.
     * @param args
     */
    public static void main7(String args[]){
        var b = false;
        var i = 1;
        do{
            i++ ;
        } while (b = !b);
        System.out.println( i );
    }

    /**
     * Les valeurs de i et j dans la boucle for la plus interne changent comme suit :
     * i = 0 j = 5
     * i = 0 j = 4
     * i = 0 j = 3
     * i = 0 j = 2
     * i = 0 j = 1
     * i = 0 j = 0
     * i = 0 j = -1
     * Par conséquent, le println final imprime i = 0, j = -1.
     * @param args
     */
    public static void main8(String[] args){
        int i = 0, j = 5;
        lab1 : for( ; ; i++){
            for( ; ; --j)  if( i >j ) break lab1;
        }
        System.out.println(" i = "+i+", j = "+j);
    }

    /**
     * Observez que la ligne if (value > 4) { et le reste du code dans la boucle for ne s'exécutent en aucun cas.
     * Il s'agit donc d'un code inaccessible et le compilateur s'en plaindra.
     * @param args
     */
    public static void main9(String[] args){
        int[] arr = { 1, 2, 3, 4, 5, 6 };
        int counter = 0;
        for (var value : arr) {
            if (counter >= 5) {
                break;
            } else {
                continue;
            }

            /**
             if (value > 4) {
             arr[counter] = value + 1;
             }
             counter++;
             */
        }
        System.out.println(arr[counter]);
    }

    static String[] days = {"monday", "tuesday", "wednesday", "thursday","friday", "saturday", "sunday" };

    /**
     * Remarquez la déclaration : if(index == 3){ break ; }else { continue ; } Le contrôle ne peut en aucun cas aller au-delà de cette instruction dans la boucle for.
     * Par conséquent, les autres instructions de la boucle for sont inaccessibles et le code ne sera pas compilé.
     * @param args
     */
    public static void main10(String[] args) {
        var index = 0;
        for(var day : days){
            if(index == 3){
                break;
            }else {
                continue;
            }
            /**index++;
            if(days[index].length()>3){
                days[index] = day.substring(0,3);
            }*/
        }
        System.out.println(days[index]);
    }


    /**
     * Dans ce type de questions, vous devrez calculer les valeurs des variables de la boucle pour chaque itération (à moins que vous ne puissiez reconnaître le modèle) sur votre feuille de calcul.
     * Début i=0, j=10
     * Itération 1 : i<=j est vrai, i devient 1 et j devient 9
     * Itération 2 : i<=j est vrai, i devient 2 et j devient 8
     * Itération 3 : i<=j est vrai, i devient 3 et j devient 7
     * Itération 4 : i<=j est vrai, i devient 4 et j devient 6
     * Itération 5 : i<=j est vrai, i devient 5 et j devient 5
     * Itération 6 : i<=j est vrai, i devient 6 et j devient 4
     * Itération 7 : i<=j est faux, la boucle while n'est donc pas activée.
     * Imprimer 6 et 4.
     * @param args
     */
    public static void main11(String[] args) {
        int i=0, j=10;
        while (i<=j) {
            i++;
            j--;
        }
        System.out.println(i+" "+j);
    }


    /**
     * L'énoncé : if(i < j) continue X1 ; else break X2 ; permet seulement de s'assurer que la boucle interne n'itère pas plus d'une fois, c'est-à-dire qu'à chaque itération de i, j ne prend que la valeur 3, puis la boucle j se termine, soit à cause de continue X1 ; soit à cause de break X2 ;. Ce qu'il faut retenir ici, c'est que lorsque la boucle for(i = 0 ; i < 3 ; i++) se termine, la valeur de i est 3 et non 2.
     * De même, s'il n'y avait pas d'instruction à l'intérieur de la boucle interne, la valeur de j après la fin de la boucle aurait été 0 et non 1.
     * @param args
     */
    public static void main12(String args[]){
        int i=0, j=0;
        X1: for(i = 0; i < 3; i++){
            X2: for(j = 3; j > 0; j--){
                if(i < j) continue X1;
                else break X2;
            }
        }
        System.out.println(i+" "+j);
    }


    /**
     * Une boucle do-while est toujours exécutée au moins une fois.
     * Ainsi, lors de la première itération, x est décrémenté et devient 9.
     * La condition "while" est maintenant testée, ce qui renvoie la réponse "true" car 9 est inférieur à 10. La boucle est donc exécutée à nouveau avec x = 9.
     * Dans la boucle, x est décrémenté à 8 et la condition est à nouveau testée, ce qui renvoie à nouveau un résultat positif car 8 est inférieur à 10.
     * Comme vous pouvez le voir, x continue à diminuer d'une unité à chaque itération et à chaque fois, la condition x<10 devient vraie.
     * Cependant, une fois que x a atteint -2147483648, qui est sa MIN_VALUE, il ne peut plus diminuer et, à ce moment-là, lorsque x-- est exécuté, la valeur passe à 2147483647, qui est Integer.MAX_VALUE.
     * À ce moment, la condition x<10 échoue et la boucle se termine.
     * @param args
     */
    public static void main13(String[] args) {
        var x = 10;
        do{
            x--;
            System.out.println(x);  // 1
        }while(x<10);
    }

    int x = 5;
    int getX(){ return x; }

    public static void main14(String args[]) throws Exception{
        TestClass tc = new TestClass();
        tc.looper();
        System.out.println(tc.x);
    }

    /**
     * Notez que looper() déclare une variable automatique x, qui fait de l'ombre à la variable d'instance x.
     * Ainsi, lorsque x = m ; est exécuté, c'est la variable locale x qui est modifiée et non le champ d'instance x.
     * Par conséquent, getX() ne renvoie jamais 0.
     * Si vous supprimez var x = 0 ; de looper(), il imprimera 0 et s'arrêtera.
     */
    public void looper(){
        var x = 0;
        while( (x = getX()) != 0 ){
            for(int m = 10; m>=0; m--){
                x = m;
            }
        }

    }

    /**
     * Voici les règles à suivre :
     *
     * Le type de l'expression du commutateur doit être String, char, byte, short ou int (et leurs classes d'enveloppe), ou un enum, sinon une erreur de compilation se produit.
     *
     * Tous les éléments suivants doivent être vrais, sinon une erreur de compilation se produit :
     * 1. Chaque expression de constante de cas associée à une instruction switch doit être assignable (5.2) au type de l'expression switch.
     * 2. Aucune des expressions de constante de cas associées à une instruction de commutation ne peut avoir la même valeur.
     * 3. Au plus une étiquette par défaut peut être associée à la même instruction de commutation.
     *
     * En principe, il recherche un cas correspondant ou, si aucune correspondance n'est trouvée, il passe à la valeur par défaut. (Si la valeur par défaut n'est pas non plus trouvée, il ne fait rien).
     * Il exécute ensuite les instructions jusqu'à ce qu'il atteigne une pause ou la fin de l'instruction de commutation.
     * Ici, il passe à la valeur par défaut et exécute jusqu'à ce qu'il atteigne le premier break. Il imprime donc 1 0 2.
     *
     *
     * Notez que l'instruction switch compare l'objet String dans son expression avec les expressions associées à chaque étiquette de cas comme si elle utilisait la méthode String.equals ; par conséquent, la comparaison des objets String dans les instructions switch est sensible à la casse. Le compilateur Java génère généralement un bytecode plus efficace à partir des instructions switch qui utilisent des objets String qu'à partir des instructions if-then-else enchaînées.
     * @param i
     */
    public void switchMethod(int i){
        switch (i){
            default:
            case 1:
                System.out.println(1);
            case 0:
                System.out.println(0);
            case 2:
                System.out.println(2);break;
            case 3:
                System.out.println(3);
        }
    }

    public static void main(String[] args) {
        new TestClass().switchMethod(5);
    }
}

