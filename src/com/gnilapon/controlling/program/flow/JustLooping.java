package com.gnilapon.controlling.program.flow;

public class JustLooping {
    private int j;

    /**
     * Le point à noter ici est que le j de la boucle for est différent du membre d'instance j.
     * Par conséquent, l'apparition de j++ dans la boucle for n'affecte pas la boucle while.
     * La boucle for affiche 1 2 3 4 5.
     * La boucle while s'exécute pour les valeurs 0 à 5, soit 6 itérations.
     * Ainsi, 1 2 3 4 5 est imprimé 6 fois.
     * Notez qu'à la fin de la boucle while, la valeur de j est 6.
     */
    void showJ() {
        while (j <= 5) {
            for (int i = 1; i <= 5; ) {
                System.out.print(i + " ");
                i++;
            }
            j++;
        }
    }
    public static void main(String[] args) {
        new JustLooping().showJ();
    }
}
