package com.library.api;

import java.util.List;

public interface BookService {
    List<String> getAllBooks();
    void addBook(String book);
}
