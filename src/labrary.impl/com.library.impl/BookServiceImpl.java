package com.library.impl;

import com.library.api.BookService;

import java.util.ArrayList;
import java.util.List;

public class BookServiceImpl implements BookService {
    private final List<String> books = new ArrayList<>();

    @Override
    public List<String> getAllBooks() {
        return new ArrayList<>(books);
    }

    @Override
    public void addBook(String book) {
        books.add(book);
    }
}
