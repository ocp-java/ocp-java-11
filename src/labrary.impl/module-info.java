module library.impl {
    requires library.api;
    provides com.library.api.BookService with com.library.impl.BookServiceImpl;
}