package com.library.app;

import com.library.api.BookService;

import java.util.ServiceLoader;

public class LibraryApp {
    public static void main(String[] args) {
        ServiceLoader<BookService> serviceLoader = ServiceLoader.load(BookService.class);
        BookService bookService = serviceLoader.findFirst().orElseThrow(() -> new RuntimeException("No BookService found"));

        bookService.addBook("The Great Gatsby");
        bookService.addBook("1984");

        System.out.println("Books in library:");
        for (String book : bookService.getAllBooks()) {
            System.out.println(book);
        }
    }
}
