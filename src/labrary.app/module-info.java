module library.app {
    requires library.api;
    requires library.impl;
    uses com.library.api.BookService;
}